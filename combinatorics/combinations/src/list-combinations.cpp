#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <fstream>

#include "combinatorics.h"

int main(int argc, char* argv[]) {

    if (argc < 3) return __LINE__;

    std::ifstream file(argv[1]);
    if (!file)return __LINE__;

    size_t id;
    if (argc == 4) {
        id = strtol(argv[3],0,10);
        if (id == 0)id = 1;
    }
    else {
        id = 1;
    }

    int choose = strtol(argv[2], 0, 10);
    if (!choose || choose < 0)return __LINE__;

    std::string line;
    std::vector<std::string> list(100);
    while (std::getline(file, line)) {
        int idPos = line.find(';');
        if (idPos == std::string::npos || idPos == line.size())continue;
        list.clear();
        auto idFile = line.substr(0,idPos);
        char* str = strtok((char*)line.c_str() + idPos+1,",");
        while (str) {
            list.push_back(str);
            str = strtok(0, ",");
        }
        nb_PrintFileCombinationsWithParent(idFile, list, choose,id);
    }
    file.close();
}