#include "combinatorics.h"

bool NextCombination(std::vector<int>& combs, int maxN) {
    int m = combs.size() - 1;
    int pos = m;
    while (++combs[pos] > maxN - (m - pos)) {
        pos--;
        if (pos < 0)break;
    }
    if (pos < 0) return false;

    while (pos < m) {
        combs[pos + 1] = combs[pos] + 1;
        pos++;
    }
    return true;
}

bool mt_NextCombination(std::vector<int>& combs, int maxN, int idt) {
    int m = combs.size() - 1;
    int pos = m;
    while ((combs[m]+idt) > maxN) {
        idt -= (maxN - combs[m]+1);
        pos--;
        while (++combs[pos] > maxN - (m - pos)) {
            pos--;
            if (pos < 0) return false;
        }
        while (pos < m) {
            combs[pos + 1] = combs[pos] + 1;
            pos++;
        }
    }
    combs[m] += idt;
    return true;
}

void PrintFileCombinations(std::string idFile, std::vector<std::string>& list, int m, size_t &id) {
    if (m> list.size())return;
    int n = list.size();

    std::string out;
    out.reserve(1024000);
    std::vector<int> combs(m);
    std::string strChoose = std::to_string(m);
    out += std::to_string(id) + ';' + strChoose + ';' + idFile + ';' + list[0] + ';';
    for (int i = 1; i < m; i++) {
        combs[i] = i;
        if(i<m-1)out += list[i] + ',';
    }
    out += list[m-1] + '\n';
    id++;
    while (NextCombination(combs, n-1)) {
        id++;
        out += std::to_string(id) + ';' + strChoose + ';' + idFile + ';' + list[combs[0]] + ';';
        for (int i = 1; i < m-1; i++) {
            out += list[combs[i]] + ',';
        }
        out += list[combs[m-1]]+ '\n';
    }
    std::cout << out;
}

void nb_PrintFileCombinations(std::string idFile, std::vector<std::string>& list, int m, size_t& id) {
    if (m > list.size())return;
    int n = list.size();

    std::vector<int> combs(m);
    std::string strChoose = std::to_string(m);
    std::cout << std::to_string(id) + ';' + strChoose + ';' + idFile + ';' + list[0] + ';';
    for (int i = 1; i < m; i++) {
        combs[i] = i;
        if (i < m - 1)std::cout << list[i] + ',';
    }
    std::cout << list[m - 1] + '\n';
    id++;
    while (NextCombination(combs, n - 1)) {
        id++;
        std::cout << std::to_string(id) + ';' + strChoose + ';' + idFile + ';' + list[combs[0]] + ';';
        for (int i = 1; i < m - 1; i++) {
            std::cout << list[combs[i]] + ',';
        }
        std::cout << list[combs[m - 1]] + '\n';
    }
    
}

void PrintFileCombinationsWithParent(std::string idFile, std::vector<std::string>& list, int m, size_t& id) {
    if (m > list.size())return;
    int n = list.size();

    std::string out;
    out.reserve(1024000);
    std::vector<int> combs(m);
    std::string strChoose = std::to_string(m);

    for (int p = 0; p < m; p++) {
        out += std::to_string(id) + ';' + strChoose + ';' + idFile + ';' + list[p] + ';';
        for (int i = 1; i < m; i++) {
            combs[i] = i;
            if (i < m - 1)out += list[(p+i)%m] + ',';
        }
        out += list[(p+m - 1)%m] + '\n';
        id++;
    }
   
    while (NextCombination(combs, n - 1)) {
        for (int p = 0; p < m; p++) {
            out += std::to_string(id) + ';' + strChoose + ';' + idFile + ';' + list[combs[p]] + ';';
            for (int i = 1; i < m; i++) {
                if (i < m - 1)out += list[combs[(p + i) % m]] + ',';
            }
            out += list[combs[(p + m - 1) % m]] + '\n';
            id++;
        }
    }
    std::cout << out;
}

void nb_PrintFileCombinationsWithParent(std::string idFile, std::vector<std::string>& list, int m, size_t& id) {
    if (m > list.size())return;
    int n = list.size();


    std::vector<int> combs(m);
    std::string strChoose = std::to_string(m);

    for (int p = 0; p < m; p++) {
        std::cout << std::to_string(id) + ';' + strChoose + ';' + idFile + ';' + list[p] + ';';
        for (int i = 1; i < m; i++) {
            combs[i] = i;
            if (i < m - 1)std::cout << list[(p + i) % m] + ',';
        }
        std::cout << list[(p + m - 1) % m] + '\n';
        id++;
    }

    while (NextCombination(combs, n - 1)) {
        for (int p = 0; p < m; p++) {
            std::cout << std::to_string(id) + ';' + strChoose + ';' + idFile + ';' + list[combs[p]] + ';';
            for (int i = 1; i < m; i++) {
                if (i < m - 1)std::cout << list[combs[(p + i) % m]] + ',';
            }
            std::cout << list[combs[(p + m - 1) % m]] + '\n';
            id++;
        }
    }

}