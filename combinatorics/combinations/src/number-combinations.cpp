#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <omp.h>
#include <fstream>

#include "combinatorics.h"

std::string to_string(std::vector<int>& v, char sep) {
    std::string out;
    for (int i = 0; i < v.size() - 1; i++) {
        out += std::to_string(v[i]) + sep;
    }
    out += std::to_string(v[v.size() - 1]);
    return out;
}


int main(int argc, char* argv[]) {
    
    if (argc < 4) {
        std::cerr << "Inconrrect number of arguments passed" << std::endl;
        return __LINE__;
    }
    
    int n = strtol(argv[1],0,10);
    int m = strtol(argv[2], 0, 10);
	int maxLines =1000000;
	if(argc>=5){
		maxLines = strtol(argv[4],0,10);
		if(maxLines ==0) maxLines =1000000;
	}
	

    if (!m && n) {
        std::cerr << "Invalid input(s)" << std::endl;
        return __LINE__;
    }
	
	if(n==1) 
	{
		return __LINE__;
	}
#pragma omp parallel
    {
        
        std::vector<int> combs(m);
        for (int i = 0; i < m; i++) {
            combs[i] = i;
        }
        int id = omp_get_thread_num();
        int stride = omp_get_num_threads();

        std::string path = argv[3];
        std::string out;
        if (id != 0) {
            mt_NextCombination(combs, n, id);
        }
        
        std::ofstream output(path + "/" + argv[1] + '-' + argv[2] + '-' + to_string(combs, '-') + ".txt");
        for (int p = 0; p < m; p++) {
            out += std::to_string(combs[p]) + ';';
            for (int i = 1; i < m-1; i++) {
                out += std::to_string(combs[(i + p) % m]) + ',';
            }
            out += std::to_string(combs[(m - 1 + p) % m]) + '\n';
        }
        output << out;
        int nl = 1;
        while (mt_NextCombination(combs, n, stride)) {
            out.clear();
            for (int p = 0; p < m; p++) {
                out += std::to_string(combs[p]) + ';';
                for (int i = 1; i < m-1; i++) {
                    out += std::to_string(combs[(i + p) % m]) + ',';
                }
				nl++;
                out += std::to_string(combs[(m - 1 + p) % m]) + '\n';
				if (nl >= maxLines) {
					nl = 1;
					output.close();
					output.open(path + "/" + argv[1] + '-' + argv[2] + '-' + to_string(combs, '-') + ".txt");
				}
            }
            output << out;
            nl++;
            if (nl >= maxLines) {
                nl = 1;
                output.close();
                output.open(path + "/" + argv[1] + '-' + argv[2] + '-' + to_string(combs, '-') + ".txt");
            }
        }
        output.close();
    }
}
