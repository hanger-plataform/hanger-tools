#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <fstream>

bool NextCombination(std::vector<int>& combs, int maxN);


bool mt_NextCombination(std::vector<int>& combs, int maxN, int idt);


void PrintFileCombinations(std::string idFile, std::vector<std::string>& list, int m, size_t &id);

void nb_PrintFileCombinations(std::string idFile, std::vector<std::string>& list, int m, size_t& id);

void PrintFileCombinationsWithParent(std::string idFile, std::vector<std::string>& list, int m, size_t& id);

void nb_PrintFileCombinationsWithParent(std::string idFile, std::vector<std::string>& list, int m, size_t& id);

