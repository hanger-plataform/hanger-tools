#!/bin/bash 
IFS=$'\n'
error_check()
{
    if [ $? -gt 0 ] 
    then
      echo "An error has occurred :_( CHECK"
      exit 1
    fi
}
gunset()
{
  local var_re="${1:-.*}"
  eval $( set | \
         awk -v FS='='        \
             -v VAR="$var_re" \
            '$1 ~ VAR { print "unset", $1 }' )
}
string_escape(){
 IFS=$'\n' 
 echo ${1}|sed -e  's/[\ \^`~!@%&:;,<>\/"?}{)(+=#$]/\\&/g'
}
run_on_athena()
{
IFS=$'\n' 
    QUERY=$1
    set_s3_location
    STORAGE_STAGING_QUEUE_PATH=${S3_LOCATION_STG} 
   # Cria o database.
    ATHENA_QUERY_ID=$(aws athena start-query-execution \
            --query-string "${QUERY}" \
            --output text \
	    --result-configuration OutputLocation=${STORAGE_STAGING_QUEUE_PATH})
    error_check
    # Identifica o status de execução.
    while true
    do
        ATHENA_QUERY_STATUS=$(aws athena get-query-execution --query-execution-id ${ATHENA_QUERY_ID} | jq '.QueryExecution.Status.State' | sed 's/\"//g')
        error_check

        if [[ "${ATHENA_QUERY_STATUS}" == "SUCCEEDED" ]]; then
            break
        elif [[ "${ATHENA_QUERY_STATUS}" == "FAILED" || "$A{THENA_QUERY_STATUS}" == "CANCELLED" ]]; then
            echo "Fail running ${QUERY} :/"
            exit 1
        else
            # Verifica o status da instrução enquanto diferente de SUCCEEDED, FAILED ou CANCELLED.
            sleep 1
        fi
    done
    error_check
    echo "Removing staging files of ${STORAGE_STAGING_QUEUE_PATH}/${ATHENA_QUERY_ID}*"
    aws s3 rm ${STORAGE_STAGING_QUEUE_PATH} --recursive --exclude "*" --include "*${ATHENA_QUERY_ID}*" --only-show-errors
    error_check
}
remove_table_version(){
IFS=$'\n' 	
 TABLE_VERSION=$(aws glue get-table-versions --max-items=100 --database-name ${1} --table-name ${2} | jq --compact-output "[.TableVersions[].VersionId]" | sed -e 's/\,/ /g;s/\[/ /g;s/\]/ /g;s/\"//g')
 error_check
 aws glue batch-delete-table-version --database-name ${1} --table-name ${2} --version-ids $TABLE_VERSION
 error_check
}

has_data_to_process(){
IFS=$'\n' 
#QTD=$(ls ${1}/ |wc -l )
QTD=$(find "${1}" -maxdepth 1 ! -type d  -regextype posix-egrep -iregex '.*\.csv\b' -exec bash -c 'printf "%q\n" "$@"' sh {} +|wc -l)
echo "
######################################################
### CHECK IF HAVE FILES TO PROCESSING
### STEP NAME == ${2} 
### QTD_FILES     = ${QTD}
### RULE TO ABORT JOB = QTD_FILES < 2
######################################################
"
if [ ! -z "${QTD}" ]
then 
  if [ ${QTD} -lt 2 ]
  then
   echo "Not Files To Process"
   exit 0 
  fi 
fi
}

custom_date(){
if [ ! -z "${FROM_DATE2}" ]
then 
FROM_DATE="${FROM_DATE2}"
fi

if [ ! -z "${FROM_TIME2}" ]
then 
FROM_TIME="${FROM_TIME2}"
fi

if [ ! -z "${TO_DATE2}" ]
then 
TO_DATE="${TO_DATE2}"
fi

if [ ! -z "${TO_TIME2}" ]
then
TO_TIME="${TO_TIME2}"
fi
}
set_s3_location(){	
 S3_LOCATION=$(echo s3://${STORAGE_BUCKET_DATA}/ingestion/${1}/${PROCESS_PATH}/${PROCESS_NAME}/${YMD_PARTITION_FOLDER}/)
 S3_LOCATION_STG=$(echo s3://${STORAGE_BUCKET_DATA_STG}/ingestion/${1}/${PROCESS_PATH}/${PROCESS_NAME}/${PROCESS_UUID}/)
}
set_paths(){
  ############################################
  #BASE PATH 
  ############################################
  TMP_FILES_PATH=${HANGER_TMP_FILES}/${PROCESS_UUID}
  mkdir -p ${BTMP_FILES}
  HANGER_FILES_PATH=${TMP_FILES_PATH}/hanger
  mkdir -p ${HANGER_FILES_PATH}
  KEYVALUE_FILES_PATH=${TMP_FILES_PATH}/keyvalue
  mkdir -p ${KEYVALUE_FILES_PATH}
  LOG_FILES_PATH=${TMP_FILES_PATH}/logs
  mkdir -p ${LOG_FILES_PATH}
  ALITA_FILES_PATH=${TMP_FILES_PATH}/alita
  mkdir -p ${ALITA_FILES_PATH} 
echo "######################################################
## CREATING PATHS
##
##  TMP_FILES_PATH      == ${HANGER_TMP_FILES}/${PROCESS_UUID}
##  HANGER_FILES_PATH   == ${TMP_FILES_PATH}/hanger
##  KEYVALUE_FILES_PATH == ${TMP_FILES_PATH}/keyvalue
##  LOG_FILES_PATH      == ${TMP_FILES_PATH}/logs
##  ALITA_FILES_PATH    == ${TMP_FILES_PATH}/alita
######################################################
"
}	

send_to_datalake(){
IFS=$'\n'
FILE_INPUT_PATH=${1}
FILE_PROCESS_TYPE=${2}
TABLE_SCHEMA=${3}
TABLE_NAME=$4}
FILE_LIST=${5}
echo "INICIOU O FOR"
echo "
FILE_INPUT_PATH=${1}
FILE_PROCESS_TYPE=${2}
TABLE_SCHEMA=${3}
TABLE_NAME=${4}
FILE_LIST=${5}

"
for k in $(zcat ${FILE_LIST}|sed 1d|cut -d ';' -f 2|cut -d '/' -f1-3|sort|uniq)
do
 echo "Extract PMS and CLIENT_UUID - ${k}"	
 PMS=$(echo ${k}|cut -d '/' -f 3)
 CLIENT_UUID=$(echo ${k}|cut -d '/' -f 1)
 set_s3_location "${FILE_PROCESS_TYPE}"

  for p in $(find "${1}" -maxdepth 1 ! -type d  -regextype posix-egrep -iregex '.*\.csv\b'|grep -iP "(${CLIENT_UUID}_).*(_${PMS}_)(.*\.csv\b)")
  do
   #FILE COMPRESS 
   echo "FILE COMPRESS - ${p}"
   #if [ -f "${p}" ]
   #then
   pigz -f --fast ${p}
   error_check
   #else
   #  echo "File ${p} not exists"
   #fi 
  done
#  --only-show-errors
#  error_check
#run_on_athena "ALTER TABLE ${TABLE_SCHEMA}.${TABLE_NAME} ADD IF NOT EXISTS PARTITION(year = '${YEAR}',month = '${MONTH}',day = '${DAY}',pms = '${PMS}',client_uuid = '${CLIENT_UUID}')LOCATION '${S3_LOCATION}';"
#error_check
done
error_check
 ##############################################################
 ##### TO FUTURE WHEN WE PARTITIONED FILES WITH CLIENT AND UUID
 ##### --include "((.*${CLIENT_UUID}.*)(.*${PMS}.*))(.*\.csv.gz)\b"
 ##############################################################
  aws s3 cp ${FILE_INPUT_PATH} \
  ${S3_LOCATION} \
  --recursive \
  --exclude "*" \
  --include "*.csv.gz" \
  --only-show-errors
  error_check
  
#REMOVE TABLE VERSION
#remove_table_version ${TABLE_SCHEMA} ${TABLE_NAME}
#error_check
}

csv_quality_check(){
IFS=$'\n'
PNAME="${1}-csv-check"
FILENAME=${2}
FILESPATH=${3}
mkdir -p ${LOG_FILES_PATH}/${PNAME}

   for f in $(find ${FILESPATH} -maxdepth 1 ! -type d  -regextype posix-egrep -iregex '.*\.csv\b'|grep -v ".csv.gz")
   do
     #for z in $(${HANGER_TOOLS_OTHER}/csvlint -delimiter ";" $(string_escope ${f}))
     for z in $(/efs/apps/libs/others/csvlint -delimiter ";" "${f}")
     do
      echo "${PROCESS_UUID};${f};${z}" >> "${LOG_FILES_PATH}/${PNAME}/${FILENAME}"
     done
   done
   error_check

   csv_logs "${PNAME}" "${FILENAME}" "${LOG_FILES_PATH}/${PNAME}"
   error_check
}
csv_logs(){
IFS=$'\n'
PNAME=${1}
FILENAME=${2}
FILESPATH=${3}
mkdir -p ${LOG_FILES_PATH}/${PNAME}

echo "
##############################################################
##  COMPRESSION THE HANGER LOG FILE AND SEND TO DATALAKE
##  ORIGINAL_FILE = ${LOG_FILES_PATH}/${PNAME}/${FILENAME}.csv
##  COMPRESSED_FILE = ${LOG_FILES_PATH}/${PNAME}/${FILENAME}.gz
##############################################################
"
#COMPRESS HANGER LOG FILE
pigz -f --fast ${LOG_FILES_PATH}/${PNAME}/${FILENAME}
error_check

#SEND HANGER LOG FILE TO DATA LAKE
set_s3_location ${PNAME}
aws s3 cp ${LOG_FILES_PATH}/${PNAME}/${FILENAME}.gz ${S3_LOCATION}
error_check
}

hanger_extractor(){
#####################################
## LOG BASE PATH HANGER 
#####################################
PNAME=hanger
mkdir -p ${LOG_FILES_PATH}/${PNAME}-csv-log
error_check

java -jar ${GLOVE_LIBS}/s3.jar \
--output="${HANGER_FILES_PATH}" \
--bucket="${INGESTION_S3_BUCKET}" \
--prefix="${S3_PREFIX}" \
--filter="${S3_FILTER}" \
--region="${S3_REGION}" \
--start_date="${FROM_DATE}" \
--end_date="${TO_DATE}" \
--start_time="${FROM_TIME}" \
--end_time="${TO_TIME}" \
--delimiter=";" \
--properties="{\"skip\":\"0\"}" \
--partition="::filename()" \
--key="::farmfingerprint([[::filename(),::RowNumber()]])" \
--field="1+2+3+4+5+6+7+8+9+10+11+12+13+14+15+16+17+18+19+20+21+22+23+24+25+26+27+28+29+30+31+32+33+34+35+36+37+38+39+40+41+42+43+44+45+46+47+48+49+50+51+52+53+54+55+56+57+58+59+60+61+62+63+64+65+66+67+68+69+70+71+72+73+74+75+76+77+78+79+80+81+82+83+84+85+86+87+88+89+90+91+92+93+94+95+96+97+98+99+100+101+102+103+104+105+106+107+108+109+110+111+112+113+114+115+116+117+118+119+120+121+122+123+124+125+126+127+128+129+130+131+132+133+134+135+136+137+138+139+140+141+142+143+144+145+146+147+148+149+150+151+152+153+154+155+156+157+158+159+160+161+162+163+164+165+166+167+168+169+170+171+172+173+174+175+176+177+178+179+180+181+182+183+184+185+186+187+188+189+190+191+192+193+194+195+196+197+198+199+200+201+202+203+204+205+206+207+208+209+210+211+212+213+214+215+216+217+218+219+220+221+222+223+224+225+226+227+228+229+230+231+232+233+234+235+236+237+238+239+240+241+242+243+244+245+246+247+248+249+250+251+252+253+254+255+256+257+258+259+260+261+262+263+264+265+266+267+268+269+270+271+272+273+274+275+276+277+278+279+280+281+282+283+284+285+286+287+288+289+290+291+292+293+294+295+296+297+298+299+300+301+302+303+304+305+306+307+308+309+310+311+312+313+314+315+316+317+318+319+320+321+322+323+324+325+326+327+328+329+330+331+332+333+334+335+336+337+338+339+340+341+342+343+344+345+346+347+348+349+350+351+352+353+354+355+356+357+358+359+360+361+362+363+364+365+366+367+368+369+370+371+372+373+374+375+376+377+378+379+380+381+382+383+384+385+386+387+388+389+390+391+392+393+394+395+396+397+398+399+400+401+402+403+404+405+406+407+408+409+410+411+412+413+414+415+416+417+418+419+420+421+422+423+424+425+426+427+428+429+430+431+432+433+434+435+436+437+438+439+440+441+442+443+444+445+446+447+448+449+450+451+452+453+454+455+456+457+458+459+460+461+462+463+464+465+466+467+468+469+470+471+472+473+474+475+476+477+478+479+480+481+482+483+484+485+486+487+488+489+490+491+492+493+494+495+496+497+498+499+500+idx_row::RowNumber()+id_file::md5([[::filename()]])+process_uuid::fixed(${PROCESS_UUID})" \
--no_header \
--inventory="${LOG_FILES_PATH}/${PNAME}-csv-log/${PROCESS_UUID}.csv"
error_check

#CHECK IF EXISTS FILE TO PROCESS
has_data_to_process "${HANGER_FILES_PATH}" "${PNAME}"
error_check

csv_logs "${PNAME}-csv-log" "${PROCESS_UUID}.csv" "${HANGER_FILES_PATH}"
error_check

csv_quality_check "${PNAME}" "${PROCESS_UUID}.csv" "${HANGER_FILES_PATH}"
error_check

}

keyvalue(){
echo "
##############################################################
## START A CREATE KEYVALUE FILE
##############################################################
"

PNAME=keyvalue-csv
mkdir -p ${LOG_FILES_PATH}/${PNAME}-log
error_check

${HANGER_TOOLS}/key-value-extractor \
"${HANGER_FILES_PATH}/" \
"${LOG_FILES_PATH}/${PNAME}-log/${PROCESS_UUID}.csv" \
"${PROCESS_UUID}" \
";" \
"${KEYVALUE_FILES_PATH}/" \
""
error_check

has_data_to_process "${KEYVALUE_FILES_PATH}"
error_check

csv_logs "${PNAME}-log" "${PROCESS_UUID}.csv" "${KEYVALUE_FILES_PATH}"
error_check

csv_quality_check "${PNAME}" "${PROCESS_UUID}.csv" "${HANGER_FILES_PATH}"
error_check

}

alita_tree(){
IFS=$'\n'
PNAME=alita-tree-csv
FILESPATH=${1}
PNAME_FILESPATH=${2} 
ALITA_TOLERANCE=${3}
mkdir -p ${ALITA_FILES_PATH}/${PNAME}/${PNAME_FILESPATH}
mkdir -p ${LOG_FILES_PATH}/${PNAME}-log

echo "
##############################################################
##  START ALITA TREE 
##  copy files from ${FILESPATH}
##  To  ${ALITA_FILES_PATH}/${PNAME}/${PNAME_FILESPATH}/
##############################################################
"
find ${FILESPATH} -maxdepth 1 ! -type d  -regextype posix-egrep -iregex '.*12.*month.*state.*\.csv\b' -exec cp {} ${ALITA_FILES_PATH}/${PNAME}/${PNAME_FILESPATH}/ \;
error_check

has_data_to_process "${ALITA_FILES_PATH}/${PNAME}/${PNAME_FILESPATH}"
error_check

${HANGER_TOOLS}/alita-tree \
${ALITA_FILES_PATH}/${PNAME}/${PNAME_FILESPATH}/ \
${ALITA_FILES_PATH}/${PNAME}/ \
${ALITA_TOLERANCE} 
error_check

#csv_logs "${PNAME}-log" "${PROCESS_UUID}.csv" "${KEYVALUE_FILES_PATH}"
#error_check

csv_quality_check "${PNAME}" "${PROCESS_UUID}.csv" "${ALITA_FILES_PATH}/${PNAME}"
error_check

}

BRANCH="${1}"
PROCESS_PATH="${2}"
PROCESS_NAME="${3}"
PROCESS_TYPE="${4}"
TARGET="${5}"
STORAGE_BUCKET_PREFIX="${6}"
DELTA_DELAY="${7}"
ENCODE="${8}"
IS_RECREATE="${9}"
IS_SCHEMA_EVOLUTION="${10}"
IS_RELOAD="${11}"
ALLOW_RECREATE="${12}"
SAMPLE="${13}"
LOAD_DELTA_TYPE="${14}"
NAMED_QUERY_INCLUDE_STEP="${15}"
GENERIC_PARAMETER="${16}"
PROCESS_UUID="${17}"
S3_PREFIX="${18}"
S3_FILTER="${19}"
FROM_DATE2="${20}"
FROM_TIME2="${21}"
TO_DATE2="${22}"
TO_TIME2="${23}"
ALITA_TREE_TOLERANCE="${24}"
S3_REGION="${25}"
echo "
##########################################################
## SET ENVIRONMENT VARIABLES
##########################################################
"
source ${HANGER_SCRIPTS}/${DEPENV}.${PROCESS_TYPE}.env
error_check
env | grep -i hanger
env | grep -i glove
cat ${HANGER_SCRIPTS}/${DEPENV}.${PROCESS_TYPE}.env

echo "
##########################################################
## SET ENVIRONMENT FROM EXECUTION LOAD ENV
##########################################################
"
source $(bash ${HANGER_SCRIPTS}/nq_environment.sh "${DELTA_DELAY}" "${PROCESS_PATH}" "${PROCESS_NAME}" "${BRANCH}" "${PROCESS_TYPE}" "${LOAD_DELTA_TYPE}" "${PROCESS_UUID}")
error_check

bash ${HANGER_SCRIPTS}/nq_environment_show_vars.sh
error_check


echo "
###########################################################
## START LOAD HANGER INGESTION v0.1
##
###########################################################
"
custom_date
error_check

set_paths
error_check

hanger_extractor
error_check

keyvalue
error_check

alita_tree "${KEYVALUE_FILES_PATH}" "keyvalue-csv" "0"
error_check

echo "
############################################################
##   SEND DATALAKE FILES TO ATHENA TABLES
############################################################
"
send_to_datalake \
"${HANGER_FILES_PATH}" \
"hanger-csv" \
"spc_hanger_ingestion" \
"hanger-csv" \
"${LOG_FILES_PATH}/hanger-csv-log/${PROCESS_UUID}.csv.gz"
error_check

send_to_datalake \
"${KEYVALUE_FILES_PATH}" \
"keyvalue-csv" \
"spc_hanger_ingestion" \
"keyvalue-csv" \
"${LOG_FILES_PATH}/hanger-csv-log/${PROCESS_UUID}.csv.gz"
error_check
