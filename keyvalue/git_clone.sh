#!/bin/bash
#v0.1 set NQ environment multibranch platform
#CHECK ERRORS LAST INSTRUCTIONS
error_check()
{
  if [ $? -gt 0 ]
  then
    echo "An error has occurred :_( - ${1}"
    exit 1
  fi
}
###################################
#1 == NAMED_QUERY_DIRECTORY
#2 == SSH_KEY
#3 == PROCESS_PATH/PROCESS_NAME
#4 == BRANCH
#5 == GIT_REPO_URL
###################################
#CREATE REPO FOLDER
cd ${1}
GIT_SSH_COMMAND="ssh -i ${2} -o IdentitiesOnly=yes" git init
error_check "init"
GIT_SSH_COMMAND="ssh -i ${2} -o IdentitiesOnly=yes" git remote add -f ${4} "${5}"
error_check "remote add"
GIT_SSH_COMMAND="ssh -i ${2} -o IdentitiesOnly=yes" git config core.sparseCheckout true
error_check "core.sparseCheckout.true"
echo "${3}" >> ${1}/.git/info/sparse-checkout
error_check "/.git/info/sparse-checkout"
GIT_SSH_COMMAND="ssh -i ${2} -o IdentitiesOnly=yes" git pull ${4} ${4}
error_check "git pull  ${4} ${4}"
