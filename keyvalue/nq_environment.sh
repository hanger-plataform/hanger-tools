#!/bin/bash
#v0.1 set NQ environment multibranch platform
# bash nq_environment.sh "15 minute ago" "delinquency" "aged_receivables" "EI-60" "ingest" "0" "19000101000000"
#DELIMITER OF ROW
IFS=$'\n'
#######################################################
#                   SET VARIABLES
#######################################################
APROCESS_UUID=$(date +'%Y%m%d%H%M%S%N')
CPROCESS_UUID=$(echo "${7}")
DEPENV=$(echo ${DEPENV}|sed 's/"//g'|tr '[:upper:]' '[:lower:]')
PROCESS_TYPE=$(echo "${5}"|tr '[:upper:]' '[:lower:]')
source ${HANGER_SCRIPTS}/${DEPENV}.${PROCESS_TYPE}.env
DELTA_DELAY=$(echo "${1}")
PROCESS_PATH=$(echo "${2}"|tr '[:upper:]' '[:lower:]')
PROCESS_NAME=$(echo "${3}"|tr '[:upper:]' '[:lower:]')
BRANCH=$(echo "${4}")
RBRANCH=$(echo "${BRANCH}"|cut -d '-' -f 1|tr '[:upper:]' '[:lower:]')
LOAD_DELTA_TYPE=$(echo "${6}")
#######################################################
#CHECK ERRORS LAST INSTRUCTIONS
error_check()
{
  if [ $? -gt 0 ]
  then
    echo "An error has occurred :_( - ${1}"
    cat ${CUSTOM_METADATA_PATH}/delta.csv.ctl > ${CUSTOM_METADATA_PATH}/delta.csv
    exit 1
  fi
}
custom_process_uuid(){
if [ -z "${CPROCESS_UUID}" ]
then
 PROCESS_UUID=${APROCESS_UUID}  
else
 PROCESS_UUID=${CPROCESS_UUID}
fi
} 
extra_vars(){
#SPLITED_ACTUAL_DATE
YEAR=$(echo $PROCESS_UUID|cut -c1-4)
MONTH=$(echo $PROCESS_UUID|cut -c5-6)
DAY=$(echo $PROCESS_UUID|cut -c7-8)
HOUR=$(echo $PROCESS_UUID|cut -c9-10)
MINUTE=$(echo $PROCESS_UUID|cut -c11-12)
SECOND=$(echo $PROCESS_UUID|cut -c13-14)
YMDHM_PARTITION_FOLDER=${YEAR}/${MONTH}/${DAY}/${HOUR}/${MINUTE}
YMDH_PARTITION_FOLDER=${YEAR}/${MONTH}/${DAY}/${HOUR}
YMD_PARTITION_FOLDER=${YEAR}/${MONTH}/${DAY}
YM_PARTITION_FOLDER=${YEAR}/${MONTH}
Y_PARTITION_FOLDER=${YEAR}
}
#CHECK VARIABLES
check_vars(){
  if [ -z "${DEPENV}" ]
  then 
    echo "ERROR - THE ENVIRONMENT VARIABLE DEPENV IS EMPTY"
    exit 1
  fi
  #NAMED_QUERY NAME
  if [ -z "${PROCESS_NAME}" ]
   then 
   echo "ERROR - PROCESS_NAME IS EMPTY"
   exit 1
  fi
  if [ -z "${PROCESS_PATH}" ]
   then
   echo "ERROR - PROCESS_PATH IS EMPTY"
   exit 1
  fi
  #DELTA DELAY VARIABLE 
  if [ -z "${DELTA_DELAY}" ]
  then
    DELTA_DELAY='15 minute ago'
  fi
   #CHECK SSH_KEY_PATH VARIABLE
   if [ ! -f "${SSH_KEY}" ]
   then 
      echo "ERROR - ${SSH_KEY} NOT EXISTS"
      exit 1 
   fi 
   #CHECK GIT HUB REPO
   if [ -z "${GIT_REPO_URL}" ]
   then 
     echo "ERROR - GIT_REPO_URL IS EMPTY"
     exit 1
   fi
 #CHECK BRANCH VARIABLE
 if [ -z "${RBRANCH}" ] 
 then 
   RBRANCH="${DEPENV}"
 fi
 if [ -z "${BRANCH}" ]
 then 
   BRANCH="${DEPENV}"
 fi
  if [ "${RBRANCH}" != "${DEPENV}" ]
   then 
    echo "YOUR BRANCH RUNNING IN WRONG ENVIRONMENT, PLEASE CHECK YOUR BRANCH IS CORRECT END TRY AGAIN"
    echo "BRANCH == ${BRANCH}"
    echo "DEPENV == ${DEPENV}"
    exit 1
  fi
   #CHECK DELTA
   if [ -z "${LOAD_DATE}" ]
   then 
     echo "ERROR - LOAD_DATE IS EMPTY"
     exit 1
   fi
   if [ -z "${TODAY}" ]
   then 
     echo "ERROR - TODAY IS EMPTY"
     exit 1
   fi
    if [ -z "${DELTA_DATE}" ]
   then 
     echo "ERROR - DELTA_DATE IS EMPTY"
     exit 1
   fi
    if [ -z "${FROM_DATE}" ]
   then 
     echo "ERROR - FROM_DATE IS EMPTY"
     exit 1
   fi 
    if [ -z "${FROM_TIME}" ]
   then 
     echo "ERROR - FROM_TIME IS EMPTY"
     exit 1
   fi
   if [ -z "${TO_DATE}" ]
   then 
     echo "ERROR - TO_DATE IS EMPTY"
     exit 1
   fi
   if [ -z "${TO_TIME}" ]
   then 
     echo "ERROR - TO_TIME IS EMPTY"
     exit 1
   fi
}
load_full(){
    DELTA_DATE="1900-01-01 00:00:00"
    DELTA_DATE_TO="2999-01-01 23:59:59"
    FROM_DATE="1900-01-01"
    FROM_TIME="23:59:59"
    TO_DATE="2999-12-31"
    TO_TIME="23:59:59"
    mkdir -p ${CUSTOM_METADATA_PATH}/
    echo "1900-01-01 00:00:00" > ${CUSTOM_METADATA_PATH}/delta.csv.ctl
    echo ${LOAD_DATE} > ${CUSTOM_METADATA_PATH}/delta.csv
}
load_delta(){ 	
    DELTA_DATE=$(cat ${CUSTOM_METADATA_PATH}/delta.csv)
    if [ "${DELTA_DATE}" == "1900-01-01 00:00:00" ]
    then 
      DELTA_DELAY=""
    fi      
    DELTA_DATE_DELAY=$(date -d "${DELTA_DATE} ${DELTA_DELAY}" +'%Y-%m-%d %H:%M:00')
    DELTA_DELAY_TO=$(echo ${DELTA_DELAY}|cut -d ' ' -f1-2)
    DELTA_DATE_DELAY_TO=$(date -d "${LOAD_DATE} ${DELTA_DELAY_TO}" +'%Y-%m-%d %H:%M:00')
    FROM_DATE=$(date -d "${DELTA_DATE_DELAY}" +'%Y-%m-%d')
    FROM_TIME=$(date -d "${DELTA_DATE_DELAY}" +'%H:%M:00')
    TO_DATE=$(date -d "${DELTA_DATE_DELAY_TO}" +'%Y-%m-%d')
    TO_TIME=$(date -d "${DELTA_DATE_DELAY_TO}" +'%H:%M:59')
    cp ${CUSTOM_METADATA_PATH}/delta.csv ${CUSTOM_METADATA_PATH}/delta.csv.ctl
    echo ${LOAD_DATE} > ${CUSTOM_METADATA_PATH}/delta.csv
}
get_delta(){
 LOAD_DATE=$(date +'%Y-%m-%d %H:%M:00')
 TODAY=$(date -d "${LOAD_DATE}" +'%Y/%m/%d/%H/%M')
  if [ ${LOAD_DELTA_TYPE} -eq 1 ]
  then 
    if [ -f ${CUSTOM_METADATA_PATH}/delta.csv ]
    then
       load_delta
    else 
       load_full
    fi
  else
    load_full
  fi
  error_check
}
create_paths(){
  #HANGER_TMP_FILES_BASE_PATH
  BHANGER_TMP_FILES=${HANGER_TMP_FILES}/${PROCESS_UUID}
  mkdir -p ${BHANGER_TMP_FILES}
  
  #NAMED_QUERY_DIRECTORY_PATH
  NAMED_QUERY_DIRECTORY=${BHANGER_TMP_FILES}/named_query
  mkdir -p ${NAMED_QUERY_DIRECTORY}
  
  #CUSTOM_METADATA_PATH
  CUSTOM_METADATA_PATH=${CUSTOM_METADATA_PATH}/${PROCESS_PATH}/${PROCESS_NAME}
  mkdir -p ${CUSTOM_METADATA_PATH}
  echo ${PROCESS_UUID} > ${CUSTOM_METADATA_PATH}/process_uuid
}
git_clone(){
 #echo bash ${HANGER_SCRIPTS}/git_clone.sh "${NAMED_QUERY_DIRECTORY}" "${SSH_KEY}" "${PROCESS_PATH}\/${PROCESS_NAME}" "${BRANCH}" "${GIT_REPO_URL}" 
 if [ -d "${NAMED_QUERY_DIRECTORY}" ]
 then 
    rm -rf ${NAMED_QUERY_DIRECTORY}
    mkdir -p ${NAMED_QUERY_DIRECTORY} 
 fi 
 bash ${HANGER_SCRIPTS}/git_clone.sh "${NAMED_QUERY_DIRECTORY}" "${SSH_KEY}" "${PROCESS_PATH}\/${PROCESS_NAME}" "${BRANCH}" "${GIT_REPO_URL}"  >/dev/null 2>&1
}
make_env_file(){
#GENERATE ENV VARIABLES RUNTIME EXECUTION
echo "export NAMED_QUERY_DIRECTORY="\"${NAMED_QUERY_DIRECTORY}/${PROCESS_PATH}\" >  ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export CUSTOM_METADATA_PATH="\"${CUSTOM_METADATA_PATH}\"                         >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export TODAY="\"${TODAY}\"                                                       >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export LOAD_DATE="\"${LOAD_DATE}\"                                               >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export FROM_DATE="\"${FROM_DATE}\"                                               >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export FROM_TIME="\"${FROM_TIME}\"                                               >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export TO_DATE="\"${TO_DATE}\"                                                   >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export TO_TIME="\"${TO_TIME}\"                                                   >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export DEPENV="\"${DEPENV}\"                                                     >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export PROCESS_UUID="\"${PROCESS_UUID}\"                                         >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export AWS_ACCOUNT_NUMBER="\"${AWS_ACCOUNT_NUMBER}\"                             >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export BHANGER_TMP_FILES="\"${BHANGER_TMP_FILES}\"                               >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export YEAR="\"${YEAR}\"                                                         >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export MONTH="\"${MONTH}\"                                                       >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export DAY="\"${DAY}\"                                                           >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export HOUR="\"${HOUR}\"                                                         >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export MINUTE="\"${MINUTE}\"                                                     >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export SECOND="\"${SECOND}\"                                                     >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export YMDHM_PARTITION_FOLDER="\"${YMDHM_PARTITION_FOLDER}\"                     >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export YMDH_PARTITION_FOLDER="\"${YMDH_PARTITION_FOLDER}\"                       >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export YMD_PARTITION_FOLDER="\"${YMD_PARTITION_FOLDER}\"                         >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export YM_PARTITION_FOLDER="\"${YM_PARTITION_FOLDER}\"                           >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
echo "export Y_PARTITION_FOLDER="\"${Y_PARTITION_FOLDER}\"                             >> ${CUSTOM_METADATA_PATH}/execution.load.env
error_check
}
check_envfile(){
#NAMED_QUERY_DIRECTORY CHECKS
if [ -z "${NAMED_QUERY_DIRECTORY}" ] || [ "${NAMED_QUERY_DIRECTORY}" != "/" ]
then 
  if [ ! -d ${NAMED_QUERY_DIRECTORY} ] 
  then 
    echo "ERROR - NAMED_QUERY_DIRECTORY NOT EXISTS"
	exit 1
  fi
fi
#CUSTOM_METADATA_PATH CHECKS
if [ -z "${CUSTOM_METADATA_PATH}" ] || [ "${CUSTOM_METADATA_PATH}" != "/" ]
then 
  if [ ! -d ${CUSTOM_METADATA_PATH} ] 
  then 
    echo "ERROR - CUSTOM_METADATA_PATH NOT EXISTS"
	exit 1
  fi
  if [ ! -f "${CUSTOM_METADATA_PATH}/execution.load.env" ] 
    then 
    echo "ERROR - execution.load.env NOT EXISTS"
	exit 1
  fi
  if [ ! -f "${CUSTOM_METADATA_PATH}/delta.csv.ctl" ] 
    then 
    echo "ERROR - delta.csv.ctl NOT EXISTS"
	exit 1
  fi
  if [ ! -f "${CUSTOM_METADATA_PATH}/delta.csv" ] 
    then 
    echo "ERROR - delta.csv NOT EXISTS"
	exit 1
  fi
fi
}
################################################################################################################################
#DEFINE PROCESS_UUID
#echo "custom_process_uuid"
custom_process_uuid

#CREATE PATHS
#echo "create_paths"
create_paths
error_check

#GET DELTA
#echo "get_delta"
get_delta
error_check

#EXTRA VARS
#echo "extra_vars"
extra_vars
error_check

#VALIDATE VARIABLES
#echo "check_vars"
check_vars
error_check

#CLONE REPO
#echo "git_clone"
git_clone
error_check

#SET ENV SYSTEM
#echo "make_env_file"
make_env_file
error_check
#CHECK ENV FILE
#echo "source ${CUSTOM_METADATA_PATH}/execution.load.env"
source "${CUSTOM_METADATA_PATH}/execution.load.env"
error_check
#echo "check_envfile"
check_envfile
error_check
#SEND FILE TO BASH
echo "${CUSTOM_METADATA_PATH}/execution.load.env"
