#!/bin/bash
export LCC_ALL=C
export DEPENV="<ENVIRONMENT>"
export DATASET_NAME="<DATABASE_NAME>"
export AWS_ACCOUNT_NUMBER="<ACCOUNT_NUMBER_OR_NAME_ENVIRONMENT>"
export ACCOUNT_NUMBER=${AWS_ACCOUNT_NUMBER}
export CUSTOM_METADATA_PATH=/efs/metadata/custom
export STORAGE_BUCKET_PREFIX="<S3_BUCKET_PREFIX_NAME>"
export STORAGE_BUCKET_DATA="${STORAGE_BUCKET_PREFIX}${AWS_ACCOUNT_NUMBER}"
export STORAGE_BUCKET_DATA_STG="${STORAGE_BUCKET_PREFIX}stg-${AWS_ACCOUNT_NUMBER}"
export GLOVE_HOME=/home/app/glove
export GLOVE_LIBS="${GLOVE_HOME}/extractor/lib"
export PDI_HOME=/efs/apps/data-integration
export HANGER_TOOLS=/efs/apps/hanger-platform/hanger-tools/stable/bin
export HANGER_TOOLS_OTHER=/efs/apps/libs/others
export HANGER_SCRIPTS=/efs/apps/scripts/env
export HANGER_TMP_FILES=/home/app/tmp_files
export PATH=${GLOVE_HOME}:${GLOVE_LIBS}:${PDI_HOME}:${PATH}
