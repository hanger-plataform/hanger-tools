﻿#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

#include <aws/core/Aws.h>
#include <aws/s3/S3Client.h>
#include <aws/s3/model/GetObjectRequest.h>
#include <aws/s3/model/ListObjectsV2Request.h>

#include <sstream>
#include <iostream>
#include <fstream>


int main(int argc)
{   
    std::string regex = "[0-9a-z]{10}\\/raw-zone\\/yardi\\/[A-Z]{2}.*.xlsx";
    std::string bucketName = "lobbycre-prod";
    std::string prefix = "sdkbf12mqm/raw-zone/yardi";
    std::string outputPath = "test/";
    boost::filesystem::path dir("./test");

    if(!boost::filesystem::exists(dir)){
        if(!boost::filesystem::create_directory(dir)){
            std::cout << "failed to create output directory " << dir.c_str() << std::endl;
            return -1;
        }
    }

    Aws::SDKOptions options;
    Aws::InitAPI(options);
    {
       Aws::Client::ClientConfiguration config;
       Aws::S3::S3Client s3_client(config);

        Aws::S3::Model::ListObjectsV2Request fileList;
        fileList.WithBucket(bucketName);
        fileList.WithPrefix(prefix);
       
        auto outcome = s3_client.ListObjectsV2(fileList);
        if (outcome.IsSuccess())
        {
            std::cout << "Objects in bucket '" << bucketName << "':" 
                << std::endl << std::endl;

            auto fileNames = outcome.GetResult().GetContents();

            int n = fileNames.size();
            int i;
#pragma omp parallel shared(fileNames) shared(config)
            {   
                boost::regex expression(regex);
                boost::cmatch what;
                Aws::S3::Model::GetObjectRequest file;
                file.WithBucket(bucketName);
                Aws::S3::S3Client s3_downloader(config);

                #pragma omp for
                for(i=0;i<n;i++){
                    if(boost::regex_match(fileNames[i].GetKey().c_str(),what,expression)){
                        std::cout << "Downloading " << fileNames[i].GetKey() <<std::endl;
                        file.SetKey(fileNames[i].GetKey());
                        auto outcome = s3_downloader.GetObject(file);
                        if(outcome.IsSuccess()){
                            auto& retrieved_file = outcome.GetResultWithOwnership().GetBody();
                            std::string outPath = outputPath + fileNames[i].GetKey().c_str();
                            for(auto &v : outPath)if(v=='/')v='_';
                            std::stringstream outBuffer;
                            outBuffer << retrieved_file.rdbuf();
                            FILE *outFile = fopen(outPath.c_str(),"w");
                            fwrite(outBuffer.str().c_str(),1,outBuffer.str().size(),outFile);
                            fclose(outFile);
                        }
                    }
                        
                }
            }
            
            
        }else{
            std::cout << "Error: ListObjects: " <<
                    outcome.GetError().GetMessage() << std::endl;
        }
    }
    Aws::ShutdownAPI(options);

    return 0;
}