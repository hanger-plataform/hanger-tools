/**
 * @file mapper.cpp
 * @author your name (eriksef2@gmail.com)
 * @brief Program that read t12 data and creates an account tree. 
 * @version 0.1
 * @date 2021-11-26
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "mapper.h"

 


int main(int argc, char* argv[]){

    if(argc <3)return -1;

    std::string timestamp;

    time_t t;
    tm *timeinfo;
    time(&t);
    timeinfo = localtime(&t);
    timestamp = std::to_string(timeinfo->tm_year+1900) + "-"+std::to_string(timeinfo->tm_mon+1)+
                 "-" + std::to_string(timeinfo->tm_mday) + " " + std::to_string(timeinfo->tm_hour) 
                 + ":" + std::to_string(timeinfo->tm_min) + ":" + std::to_string(timeinfo->tm_sec);
    

    std::string inputDir = argv[1];
    std::string outputDir = argv[2];
    double tol = 0;
    if(argc>=4){
        tol = strtod(argv[3],0);
    }

    std::ofstream errorLog;
    if(argc>=6){
        errorLog.open(argv[5]);
    }else{
        errorLog.open("Alita-Errors.log");
    }

    //Key-value function
    int (*kvf) (csv_workbook &,double **,char***,int**,int &,int &,int &,int &);
    if(argc >=5){
        if(!strcmp(argv[4],"hanger")){
            kvf = hanger::ReadKeyValueDataHanger;
            
        }else{
            kvf = hanger::ReadKeyValueData;
        }
    }else{
        kvf = hanger::ReadKeyValueData;
    }
    
    auto fileList = hanger::GetFileList(inputDir);

    if(fileList.empty()){
        std::cout << "Invalid input directory " + inputDir<<std::endl;
        return -1;
    }

    if(boost::filesystem::is_regular_file(outputDir)){
        std::cout << "Invalid output directory " + outputDir<<std::endl;
        return -1;
    }else{
        if(!boost::filesystem::exists(outputDir)){
            boost::filesystem::create_directory(outputDir);
        }
    }

    int i;
    #pragma omp parallel shared(fileList) shared(inputDir) shared(outputDir)
    {
        double* matrix=0;
        char** labels=0;
        int* tree=0;
        int rows=0,cols=0,size=0,rr=0;

        //Allocates space for the parents map
        std::map<int,int> parents; //Vector that holds the maps for each iteration
        //parents.reserve(1024);

        //std::vector<int> subtotals; //Vector to validate the subtotals
        //subtotals.reserve(1024);

        //Allocates space for the level vector
        std::vector<int> levels;
        levels.reserve(1024);

        //Allocates space for the orphans vector
        std::vector<std::pair<int,double>> orphans; //Vector to mark orphan accounts
        orphans.reserve(1024);

        //Allocates space for the headers
        std::vector<int> headers; //Vector to mark orphan accounts
        orphans.reserve(1024);

        //Output strings
        std::string id_row;
        id_row.reserve(1024);

        std::string id_account;
        id_account.reserve(1024);

        std::string format = "";
        boost::regex reg ("[^[:alnum:]]");
        
        std::string out;
        out.reserve(1048576);
        std::string parent;
        parent.reserve(1024);
        std::string child;
        child.reserve(1024);
        std::string temp;
        temp.reserve(1024);

    #pragma omp for 
        for(i=0;i< fileList.size();i++){
            csv_workbook wb;
            
            int status = read_csv_workbook(&wb,fileList[i].c_str(),';','\n');

            if(status==0){
                

                std::string filename = fileList[i].filename().c_str();
                std::string id_file;
                char md5str[128];
                pg_md5_hash((const uint8*)filename.c_str(), filename.size(),(char*) md5str);
                id_file = md5str;
                std::ofstream output(outputDir + "/" + filename);
                //Creates matrix to receive the date in matrix format (text set to 0); labels to receive the labels and tree to receive the tree
                try{

                    kvf(wb,&matrix,&labels,&tree,rows,cols,size,rr);
                    

                    //Finds the column with the labels
                    double meanSize =0;
                    int labelCol =0;
                    for(int j = 0;j< cols;j++){
                        double tempMean=0;
                        for(int i=0;i< rows;i++){
                            if(labels[i + j*rows])
                                tempMean+= strlen(labels[i + j*rows]);
                        }
                        if(tempMean > meanSize){
                            meanSize=tempMean;
                            labelCol = j;
                        }
                    }

                    

                    //Counts the number of non-zero entries on each row
                    for(int i=0;i< rows;i++)
                    {
                        matrix[i+ labelCol*rows] =0;
                        for(int j=labelCol+1;j< cols;j++)
                        {
                            if(matrix[i + j*rows]!=0)
                            {
                                matrix[i+ labelCol*rows] +=1;
                            }
                        }
                    }
                    
                    cols = cols>(labelCol+14)?(labelCol+14):cols;
                    #pragma omp critical
                    {
                        std::cout << "Alita - Mapping (tolerance "<< tol << ") nrows: " << rows << " ncols: " << cols << " labels in column " << labelCol+1 << " " << fileList[i].c_str() <<std::endl;
                    }
                    //Creates the account tree
                    for(int j=labelCol+1;j< cols;j++){
                        //Maps the accounts
                        hanger::MapT12Details(matrix + j*rows,parents,rows,10,tol);
                        //Each child votes for its own parent
                        for(auto &child :parents)
                        {
                            int row = child.second;
                            int col = child.first;
                            tree[row + col* rows] +=1;
                        }
                    }
                    
                    
                    //Tallies the most voted parents and sets the winner as the real parent
                    for(int j=0;j< rows;j++){
                        int max = 0;
                        int mp = 0;
                        if(matrix[j + labelCol*rows] > 1)
                        {
                            for(int i = 0;i< rows;i++){
                                if(tree[i + j* rows]>max){
                                    max = tree[i + j* rows];
                                    mp=i;
                                }
                            }
                            if(max > 0)
                            {
                                if(max > 1)
                                {
                                    matrix[j + labelCol*rows] = max /  matrix[j + labelCol*rows];
                                    for(int i = 0;i< rows;i++){
                                        if( mp != i){
                                            tree[i + j*rows]=0;
                                        }else
                                        {
                                            tree[i + j*rows]=1;
                                        }
                                    }
                                }
                                else
                                {
                                    for(int i = 0;i< rows;i++){
                                        tree[i + j*rows]=0;
                                    }
                                }
                            }
                        }
                         else
                        {
                            for(int i = 0;i< rows;i++)
                            {
                                tree[i + j*rows]=0;
                            }
                        }
                    }

                  

                    //Checks if the account has no parent
                    orphans.clear();
                    headers.clear();
                    for(int j =0; j< rows;j++){
                        bool isOrphan = true;
                        for(int i = j+1;i< rows;i++){
                            if(tree[i + j*rows]){
                                isOrphan = false;
                                //break;
                            }
                        }
                        if(isOrphan || j == (rows-1)){
                            double* value = matrix + j;
                            double sum = 0;
                            for(int i = labelCol+1;i<cols;i++){
                                sum +=matrix[j + i*rows];
                            }
                            if(sum==0.0){
                                headers.push_back(j);
                            }else{
                                orphans.push_back({j,sum});
                            }
                            
                        }
                    }

                    int noi = -1;
                    int expense=-1;
                    int income =-1;
                    if(!orphans.empty()){
                        for(int i = orphans.size()-1;i>=2;i--){
                            for(int j=i-1;j>=1;j--){
                                for(int k = j-1;k>=0;k--){
                                    double testVal = orphans[i].second + orphans[j].second-orphans[k].second;
                                    if(fabs(testVal) < tol){
                                        noi = orphans[i].first;
                                        expense = orphans[j].first;
                                        income = orphans[k].first;
                                    }
                                }
                            }
                        }
                    }

                    
                    levels.resize(rows);
                    std::fill(levels.begin(),levels.end(),1);
                    out.clear();
                    if(noi>0){
                        if(labels[income + labelCol*rows]){
                            hanger::MapT12Levels(tree,rows,levels,income,expense);
                            if(cols - labelCol > 0)
                            {
                                matrix[income + labelCol*rows] /= cols - labelCol;
                                matrix[expense + labelCol*rows] /= cols - labelCol;
                                matrix[noi + labelCol*rows] /= cols - labelCol;
                            }else
                            {
                                matrix[income + labelCol*rows] =0;
                                matrix[expense + labelCol*rows] =0;
                                matrix[noi + labelCol*rows] =0;
                            }
                            hanger::MapT12Levels(tree,rows,levels,income,expense);
                            //Computes id_row
                            id_row = filename + "total_income|"+ std::to_string(income);
                            auto val =util::Fingerprint128 (id_row);
                            auto f1 = val.first;
                            auto f2 = val.second;
                            id_row = std::to_string(f1) + std::to_string(f2);
                            
                            //Computes id_account
                            parent = labels[income + labelCol*rows];
                            temp = boost::regex_replace(parent,reg,format);
                            boost::to_lower(temp);
                            id_account = temp + "|" +  temp;
                            val =util::Fingerprint128 (id_account);
                            f1 = val.first;
                            f2 = val.second;
                            id_account= std::to_string(f1) + std::to_string(f2);

                            std::string str = std::string(labels[income + labelCol*rows]);
                            hanger::EscapeQuotes(str);
                            out.append("\"");
                            out.append(id_file);
                            out.append("\";\"");
                            out.append(filename);
                            out.append("\";\"");
                            out.append(id_row);
                            out.append("\";\"");
                            out.append(id_account);
                            out.append("\";");
                            out.append(std::to_string(income+1));
                            out.append(";\"");
                            out.append(str);
                            out.append("\";");
                            out.append(std::to_string(income+1));
                            out.append(";\"");
                            out.append(str);
                            out.append("\";\"total_income\";");
                            out.append(std::to_string(levels[income]));
                            out.push_back(';');
                            out.append(std::to_string(matrix[income + labelCol*rows]));
                            out.push_back('\n');
                        }
                        if(labels[expense + labelCol*rows]){
                           //Computes id_row
                            id_row = filename + "total_expense|"+ std::to_string(expense);
                            auto val =util::Fingerprint128 (id_row);
                            auto f1 = val.first;
                            auto f2 = val.second;
                            id_row = std::to_string(f1) + std::to_string(f2);
                            
                            //Computes id_account
                            parent = labels[expense + labelCol*rows];
                            temp = boost::regex_replace(parent,reg,format);
                            boost::to_lower(temp);
                            id_account = temp + "|" +  temp;
                            val =util::Fingerprint128 (id_account);
                            f1 = val.first;
                            f2 = val.second;
                            id_account= std::to_string(f1) + std::to_string(f2);

                            std::string str = std::string(labels[expense + labelCol*rows]);
                            hanger::EscapeQuotes(str);
                            out.append("\"");
                            out.append(id_file);
                            out.append("\";\"");
                            out.append(filename);
                            out.append("\";\"");
                            out.append(id_row);
                            out.append("\";\"");
                            out.append(id_account);
                            out.append("\";");
                            out.append(std::to_string(expense+1));
                            out.append(";\"");
                            out.append(str);
                            out.append("\";");
                            out.append(std::to_string(expense+1));
                            out.append(";\"");
                            out.append(str);
                            out.append("\";\"total_expense\";");
                            out.append(std::to_string(levels[expense]));
                            out.push_back(';');
                            out.append(std::to_string(matrix[expense + labelCol*rows]));
                            out.push_back('\n');
                        }
                        if(labels[noi + labelCol*rows]){
                            //Computes id_row
                            id_row = filename + "noi|"+ std::to_string(noi);
                            auto val =util::Fingerprint128 (id_row);
                            auto f1 = val.first;
                            auto f2 = val.second;
                            id_row = std::to_string(f1) + std::to_string(f2);
                            
                            //Computes id_account
                            parent = labels[noi + labelCol*rows];
                            temp = boost::regex_replace(parent,reg,format);
                            boost::to_lower(temp);
                            id_account = temp + "|" +  temp;
                            val =util::Fingerprint128 (id_account);
                            f1 = val.first;
                            f2 = val.second;
                            id_account= std::to_string(f1) + std::to_string(f2);

                            std::string str = std::string(labels[noi + labelCol*rows]);
                            hanger::EscapeQuotes(str);
                            out.append("\"");
                            out.append(id_file);
                            out.append("\";\"");
                            out.append(filename);
                            out.append("\";\"");
                            out.append(id_row);
                            out.append("\";\"");
                            out.append(id_account);
                            out.append("\";");
                            out.append(std::to_string(noi+1));
                            out.append(";\"");
                            out.append(str);
                            out.append("\";");
                            out.append(std::to_string(noi+1));
                            out.append(";\"");
                            out.append(str);
                            out.append("\";\"noi\";");
                            out.append(std::to_string(levels[noi]));
                            out.push_back(';');
                            out.append(std::to_string(matrix[expense + labelCol*rows]));
                            out.push_back('\n');
                        }
                    }
                    else{
                        for(auto & v: orphans){
                            if(v.second!=0){
                                if(income <0){
                                    income = v.first;
                                }else{
                                    expense = v.first;
                                    break;
                                }
                            }
                        }
                        if(income >= rows || expense >= rows)
                        {
                            income = -1;
                            expense = -1;
                        }
                        if(income>0 && expense > 0)
                        {
                            hanger::MapT12Levels(tree,rows,levels,income,expense);
                        }
                        else
                        {
                            std::fill(levels.begin(),levels.end(),1);
                        }
                        if(cols - labelCol > 0)
                        {
                            matrix[income + labelCol*rows] /= cols - labelCol;
                            matrix[expense + labelCol*rows] /= cols - labelCol;
                            //matrix[noi + labelCol*rows] /= cols - labelCol;
                        }else
                        {
                            matrix[income + labelCol*rows] =0;
                            matrix[expense + labelCol*rows] =0;
                            //matrix[noi + labelCol*rows] =0;
                        }
                        if(income >0 && labels[income + labelCol*rows]){
                             //Computes id_row
                            id_row = filename + "total_income|"+ std::to_string(income);
                            auto val =util::Fingerprint128 (id_row);
                            auto f1 = val.first;
                            auto f2 = val.second;
                            id_row = std::to_string(f1) + std::to_string(f2);
                            
                           //Computes id_account
                            parent = labels[income + labelCol*rows];
                            temp = boost::regex_replace(parent,reg,format);
                            boost::to_lower(temp);
                            id_account = temp + "total_expense|" +  temp;
                            val =util::Fingerprint128 (id_account);
                            f1 = val.first;
                            f2 = val.second;
                            id_account= std::to_string(f1) + std::to_string(f2);

                            std::string str = std::string(labels[income + labelCol*rows]);
                            hanger::EscapeQuotes(str);
                            out.append("\"");
                            out.append(id_file);
                            out.append("\";\"");
                            out.append(filename);
                            out.append("\";\"");
                            out.append(id_row);
                            out.append("\";\"");
                            out.append(id_account);
                            out.append("\";");
                            out.append(std::to_string(income+1));
                            out.append(";\"");
                            out.append(str);
                            out.append("\";");
                            out.append(std::to_string(income+1));
                            out.append(";\"");
                            out.append(str);
                            out.append("\";\"total_income\";");
                            out.append(std::to_string(levels[income]));
                            out.push_back(';');
                            out.append(std::to_string(matrix[income + labelCol*rows]));
                            out.push_back('\n');
                        }
                        if(expense > 0 && labels[expense + labelCol*rows]){
                            //Computes id_row
                            id_row = filename + "total_expense|"+ std::to_string(expense);
                            auto val =util::Fingerprint128 (id_row);
                            auto f1 = val.first;
                            auto f2 = val.second;
                            id_row = std::to_string(f1) + std::to_string(f2);
                            
                            //Computes id_account
                            parent = labels[expense + labelCol*rows];
                            temp = boost::regex_replace(parent,reg,format);
                            boost::to_lower(temp);
                            id_account = temp + "|" +  temp;
                            val =util::Fingerprint128 (id_account);
                            f1 = val.first;
                            f2 = val.second;
                            id_account= std::to_string(f1) + std::to_string(f2);

                            std::string str = std::string(labels[expense + labelCol*rows]);
                            hanger::EscapeQuotes(str);
                            out.append("\"");
                            out.append(id_file);
                            out.append("\";\"");
                            out.append(filename);
                            out.append("\";\"");
                            out.append(id_row);
                            out.append("\";\"");
                            out.append(id_account);
                            out.append("\";");
                            out.append(std::to_string(expense+1));
                            out.append(";\"");
                            out.append(str);
                            out.append("\";");
                            out.append(std::to_string(expense+1));
                            out.append(";\"");
                            out.append(str);
                            out.append("\";\"total_expense\";");
                            out.append(std::to_string(levels[expense]));
                            out.push_back(';');
                            out.append(std::to_string(matrix[expense + labelCol*rows]));
                            out.push_back('\n');
                        }
                    }
                    
                    //Prints out the result is csv format
                    std::string systag = "income";
                    if(income <0 && expense < 0){
                        systag = "untagged";
                    }
                    for(int i=0;i< rows;i++){
                        if(income >0 && i==income+1){
                            systag = "expense";
                        }else if(expense > 0 && i==expense+1){
                            systag = "below_the_line";
                        }
                        
                        if( labels[i + labelCol*rows])
                        {
                            parent= labels[i + labelCol*rows];
                            hanger::EscapeQuotes(parent);
                            if(parent[parent.size()-1]=='\"'){
                            parent.resize(parent.size()-1);
                        }
                        }
                        else
                        {
                            parent = std::to_string(i);
                        }
                       
                        for(int j=0;j< rows;j++){
                            if(tree[i + j*rows]){
                                
                                if(labels[j + labelCol*rows]){
                                    child = labels[j + labelCol*rows];
                                    hanger::EscapeQuotes(child);
                                }else{
                                    child = std::to_string(j);
                                }
                                

                                //Computes id_row
                                id_row = filename + "|"+ std::to_string(j);
                                auto val =util::Fingerprint128 (id_row);
                                auto f1 = val.first;
                                auto f2 = val.second;
                                id_row = std::to_string(f1) + std::to_string(f2);
                                
                                //Computes id_account
                                temp = boost::regex_replace(parent + "|" + child,reg,format);
                                boost::to_lower(temp);
                                val =util::Fingerprint128 (temp);
                                f1 = val.first;
                                f2 = val.second;
                                id_account= std::to_string(f1) + std::to_string(f2);
                                out.append("\"");
                                out.append(id_file);
                                out.append("\";\"");
                                out.append(filename);
                                out.append("\";\"");
                                out.append(id_row);
                                out.append("\";\"");
                                out.append(id_account);
                                out.append("\";");
                                out.append(std::to_string(i+1));
                                out.append(";\"");
                                out.append(parent);
                                out.append("\";");
                                out.append(std::to_string(j+1));
                                out.append(";\"");
                                out.append(child);
                                out.append("\";\"");
                                out.append(systag);
                                out.append("\";");
                                out.append(std::to_string(levels[j]));
                                out.push_back(';');
                                out.append(std::to_string(matrix[j + labelCol*rows]));
                                out.push_back('\n');
                            }
                        }
                    }
                    
                if(!out.empty()){
                    output<<out;
                }
                output.close();
                free_csv_workbook(&wb);
                }catch(std::exception &err){
                    #pragma omp critical
                    {
                        errorLog << "\"" +std::string(fileList[i].c_str()) +  "\";\"" + std::string(err.what()) + "\"\n";
                    }
                    output.close();
                    free_csv_workbook(&wb);
                }
                catch(const char *err){
                    #pragma omp critical
                    {
                        errorLog << "\"" +std::string(fileList[i].c_str()) +  "\";\"" + std::string(err) + "\"\n";
                    }
                    output.close();
                    free_csv_workbook(&wb);
                }
                catch(...){
                     #pragma omp critical
                    {
                        errorLog << "\"" +std::string(fileList[i].c_str()) +  "\";\"An unexpected error occured\"\n";
                    }
                    output.close();
                    free_csv_workbook(&wb);
                }
            }else{
                std::cout << "Failure to open file " <<  fileList[i].c_str() <<std::endl;
            }
        }
        
        if(matrix) delete [] matrix;
        if(tree)   delete [] tree;
        if(labels) delete [] labels;
    }

   
    errorLog.close();

    return 0;
    

}

