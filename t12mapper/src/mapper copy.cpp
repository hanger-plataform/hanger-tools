/**
 * @file mapper.cpp
 * @author your name (eriksef2@gmail.com)
 * @brief Program that read t12 data and creates an account tree. 
 * @version 0.1
 * @date 2021-11-26
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "mapper.h"

 


int main(int argc, char* argv[]){

    if(argc <3)return -1;

    std::string timestamp;

    time_t t;
    tm *timeinfo;
    time(&t);
    timeinfo = localtime(&t);
    timestamp = std::to_string(timeinfo->tm_year+1900) + "-"+std::to_string(timeinfo->tm_mon+1)+
                 "-" + std::to_string(timeinfo->tm_mday) + " " + std::to_string(timeinfo->tm_hour) 
                 + ":" + std::to_string(timeinfo->tm_min) + ":" + std::to_string(timeinfo->tm_sec);
    

    std::string inputDir = argv[1];
    std::string outputDir = argv[2];
    double tol = 0;
    if(argc>=4){
        tol = strtod(argv[3],0);
    }

    std::ofstream errorLog;
    if(argc>=6){
        errorLog.open(argv[5]);
    }else{
        errorLog.open("Alita-Errors.log");
    }

    //Key-value function
    int (*kvf) (csv_workbook &,double **,char***,int**,int &,int &,int &,int &);
    if(argc >=5){
        if(!strcmp(argv[4],"hanger")){
            kvf = hanger::ReadKeyValueDataHanger;
            
        }else{
            kvf = hanger::ReadKeyValueData;
        }
    }else{
        kvf = hanger::ReadKeyValueData;
    }
    
    auto fileList = hanger::GetFileList(inputDir);

    if(fileList.empty()){
        std::cout << "Invalid input directory " + inputDir<<std::endl;
        return -1;
    }

    if(boost::filesystem::is_regular_file(outputDir)){
        std::cout << "Invalid output directory " + outputDir<<std::endl;
        return -1;
    }else{
        if(!boost::filesystem::exists(outputDir)){
            boost::filesystem::create_directory(outputDir);
        }
    }

    int i;
    #pragma omp parallel shared(fileList) shared(inputDir) shared(outputDir)
    {
        double* matrix=0;
        char** labels=0;
        int* tree=0;
        int rows=0,cols=0,size=0,rr=0;

        std::map<int,int> parents; //Vector that holds the maps for each iteration
        //parents.reserve(1024);

        std::vector<int> subtotals; //Vector to validate the subtotals
        subtotals.reserve(1024);

        std::vector<std::pair<int,double>> orphans; //Vector to mark orphan accounts
        orphans.reserve(1024);

        std::vector<int> headers; //Vector to mark orphan accounts
        orphans.reserve(1024);

        std::string id_row;
        id_row.reserve(1024);

        std::string id_account;
        id_account.reserve(1024);

        std::string format = "";
        boost::regex reg ("[^[:alnum:]]");
        
        //private(matrix) private(labels) private(tree) private(rows,cols,size,rr) private(parents) private(subtotals) private(orphans)
    #pragma omp for 
        for(i=0;i< fileList.size();i++){
            csv_workbook wb;
            int status = read_csv_workbook(&wb,fileList[i].c_str(),';','\n');
            if(status==0){

                
                
                std::cout << "Alita - Mapping " << fileList[i].c_str() <<std::endl;
                
                
                parents.clear();
                subtotals.clear();
                orphans.clear();

                std::string filename = fileList[i].filename().c_str();
                std::string id_file;
                char md5str[64];
                pg_md5_hash((const uint8*)filename.c_str(), filename.size(),(char*) md5str);
                id_file = md5str;
                std::ofstream output(outputDir + "/" + filename);
                //Creates matrix to receive the date in matrix format (text set to 0); labels to receive the labels and tree to receive the tree
                try{

                    
                    kvf(wb,&matrix,&labels,&tree,rows,cols,size,rr);
                    
                    

                    //Finds the column with the labels
                    //Finds the column with the labels
                    double meanSize =0;
                    int labelCol =0;
                    for(int j = 0;j< cols;j++){
                        double tempMean=0;
                        for(int i=0;i< rows;i++){
                            if(labels[i + j*rows])
                                tempMean+= strlen(labels[i + j*rows]);
                        }
                        if(tempMean > meanSize){
                            meanSize=tempMean;
                            labelCol = j;
                        }
                    }

                    //Counts the number of non-zero entries on each row
                    for(int i=0;i< rows;i++)
                    {
                        matrix[i+ labelCol*rows] =0;
                        for(int j=labelCol+1;j< cols;j++)
                        {
                            if(matrix[i + j*rows]!=0)
                            {
                                matrix[i+ labelCol*rows] +=1;
                            }
                        }
                    }
                    
                    
                    cols = cols>(labelCol+14)?(labelCol+14):cols;
                    //Creates the account tree
                    for(int j=labelCol+1;j< cols;j++){
                        //Maps the accounts
                        hanger::MapT12Details(matrix + j*rows,parents,rows,10,tol);
                        //Each child votes for its own parent
                        for(auto &child :parents)
                        {
                            int row = child.second;
                            int col = child.first;
                            tree[row + col* rows] +=1;
                        }
                    }
                   
                    //Tallies the most voted parents and sets the winner as the real parent
                    for(int j=0;j< rows;j++){
                        int max = 0;
                        int mp = 0;
                        if(matrix[j + labelCol*rows] > 1)
                        {
                            for(int i = 0;i< rows;i++){
                                if(tree[i + j* rows]>max){
                                    max = tree[i + j* rows];
                                    mp=i;
                                }
                            }
                            if(max > 0)
                            {
                                if(max > 1)
                                {
                                    matrix[j + labelCol*rows] = max /  matrix[j + labelCol*rows];
                                    for(int i = 0;i< rows;i++){
                                        if( mp != i){
                                            tree[i + j*rows]=0;
                                        }else
                                        {
                                            tree[i + j*rows]=1;
                                        }
                                    }
                                }
                                else
                                {
                                    for(int i = 0;i< rows;i++){
                                        tree[i + j*rows]=0;
                                    }
                                }
                            }
                        }
                        else
                        {
                            for(int i = 0;i< rows;i++)
                            {
                                //tree[i + j*rows]=0;
                            }
                        }
                    }
                    

                    std::string out;
                    out.reserve(1048576);

                    
                    std::string parent;
                    std::string child;
                    std::string temp;

                    for(int i=0;i<rows;i++)
                    {
                        //for(int j=0;j<rows;j++){
                            if(labels[i +labelCol*rows]){
                                out.append(std::to_string(i));
                                out.append(",");
                                out.append(labels[i +labelCol*rows]);
                                out.append(",");
                                out.append(std::to_string(matrix[i + labelCol*rows]));
                                out.append(",");
                            }else{
                                out.append(std::to_string(i));
                                out.append(",");
                                out.append("null");
                                out.append(",");
                                out.append(std::to_string(matrix[i + labelCol*rows]));
                                out.append(",");
                            }
                        //}
                        for(int j=0;j<rows;j++)
                        {
                            out.append(std::to_string(tree[i + j * rows]));
                            out.append(",");
                        }
                        /*for(int j=labelCol;j<cols;j++)
                        {
                            if(labels[i +j*rows]){
                                if(j==labelCol)
                                {
                                    out.append(labels[i + j * rows]);
                                }else{
                                    out.append(std::to_string(matrix[i + j * rows]));
                                }
                                
                            }
                            out.append(",");*/
                        //}
                        out.append("\n");
                    }
                    output << out;
                    output.close();
                    free_csv_workbook(&wb);
                    
                }
                catch(const char *err){
                    #pragma omp critical
                    {
                        errorLog << "\"" +std::string(fileList[i].c_str()) +  "\";\"" + std::string(err) + "\"\n";
                    }
                    output.close();
                    free_csv_workbook(&wb);
                }
                catch(...){
                     #pragma omp critical
                    {
                        errorLog << "\"" +std::string(fileList[i].c_str()) +  "\";\"An unexpected error occured\"\n";
                    }
                    output.close();
                    free_csv_workbook(&wb);
                }
            }else{
                std::cout << "Failure to open file " <<  fileList[i].c_str() <<std::endl;
            }
        }
        
        if(matrix) delete [] matrix;
        if(tree)   delete [] tree;
        if(labels) delete [] labels;
    }

   
    errorLog.close();

    return 0;
    

}
