#ifndef MAPPER_H
#define MAPPER_H

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include <set>
#include <math.h>
#include <utility>
#include <fstream>
#include <ctime>
#include<map>


#include <boost/regex.hpp>
#include <boost/algorithm/algorithm.hpp>
#include <boost/algorithm/string.hpp>

#include "../xlsx-parser/include/xlsx-parser.h"
#include "../../farmhash/include/farmhash.h"
#include "../../include/hanger-utils.h"

//md5 dependencies
typedef unsigned char uint8;
typedef unsigned int uint32;
bool pg_md5_hash(const uint8 *buff, size_t len, char *hexsum);
//

namespace hanger{
    /**
     * @brief Structure to track the children of a subtotal
     * 
     */
    struct subtotal{
        int row;
        std::vector<int> children;
    };
    
    /**
     * @brief Reads key value data from key-value formarted csv
     * 
     * @param wb 
     * @param matrix 
     * @param labels 
     * @param tree 
     * @param nr 
     * @param nc 
     * @param size 
     * @param rr 
     * @return int 
     */
    int ReadKeyValueData(csv_workbook &wb,double **matrix,char*** labels,int** tree,int &nr,int &nc, int &size, int &rr);

    /**
     * @brief Reads key value data from a hanger extracted key-value format
     * 
     * @param wb 
     * @param matrix 
     * @param labels 
     * @param tree 
     * @param nr 
     * @param nc 
     * @param size 
     * @param rr 
     * @return int 
     */
    int ReadKeyValueDataHanger(csv_workbook &wb,double **matrix,char*** labels,int** tree,int &nr,int &nc, int &size, int &rr);

    /**
     * @brief Maps a t12 account balance
     * 
     * @param t12 
     * @param tree 
     * @param n 
     * @param stride 
     * @param maxIter 
     * @return true 
     * @return false 
     */
    bool MapT12Balance(double *t12, int* tree, int n, int stride, int maxIter);

    /**
     * @brief Maps a t12 account details
     * 
     * @param t12 
     * @param parents 
     * @param n 
     * @param maxIter 
     * @param tol 
     * @return true 
     * @return false 
     */
    bool MapT12Details(double *t12, std::map<int,int> &parents,int n, int maxIter, double tol);

    /**
     * @brief Maps a t12 totals/subtotals
     * 
     * @param t12 
     * @param parents 
     * @param subtotals 
     * @param tol 
     * @return true 
     * @return false 
     */
    bool MapT12Totals(double *t12, std::vector< subtotal >  &parents,std::vector<int> &subtotals,double tol);
    
    /**
     * @brief 
     * 
     */
    void MapT12Levels(int* tree, int nr, int nc, std::vector<int> &levels,int totalIncomeRow,int totalExpenseRow);

    //FUNTION DEFINITIONS
   
    bool MapT12Balance(double *t12, int* tree, int n, int stride, int maxIter)
    {
        std::vector<bool> isSet(n,1);
        std::vector<bool> prevSet(n,1);

        bool done = false;
        int c = 0;
        while(!done && c<maxIter){

            for(int i=1;i<n;i++){
                if(t12[i]!=0){
                    double sum =0;
                    int countChildren =0;
                    bool isEqual=false;
                    int j = i-1;
                    while(j>=0){
                        if(isSet[j]){
                            sum+=t12[j];
                            countChildren++;
                        }
                        if(sum==t12[i]){
                            isEqual = true;
                            break;
                        }
                        j--;
                    }

                    if(isEqual && countChildren){
                        while(j<i){
                            if(isSet[j]){
                                tree[i + j*stride] += 1;
                                isSet[j]=0;    
                            }
                            j++;
                        }
                    }
                    if(std::equal(isSet.begin(),isSet.end(),prevSet.begin())){
                        done = true;
                    }else{
                        prevSet = isSet;
                    }
                }else{
                    isSet[i] =0;
                }
            }
            c++;
        }
        return done;
    }

    bool MapT12Details(double *t12, std::map<int,int> &parents,int n, int maxIter, double tol)
    {
        //Vector to check if the previous setted rows are the same
        std::vector<bool> isSet(n,1);
        parents.clear();
        bool done = false;
        int c = 0;

        //Rip and tear until it is done (unless maxIter is reached first)
        while(!done && c<maxIter){
            std::map<int,int> tempParents;
            done = true;
            for(int i=0;i<n;i++){
                auto val = t12[i];
                if(t12[i]!=0 && isSet[i]){
                    double sum =0;
                    bool isEqual=false;
                    int j = i-1;
                    int countChildren =0;
                    while(j>=0){
                        if(isSet[j]){
                            double xx = t12[j];
                            countChildren++;
                            sum+=t12[j];
                        }
                        if(fabs(sum-t12[i]) <=tol && countChildren){
                            isEqual = true;
                            break;
                        }
                        j--;
                    }

                    if(isEqual){
                        done = false;
                        //Sets the parent of j-th row as the i-th row
                        while(j<i){
                            if(isSet[j])
                            {
                                tempParents[j] = i;
                            }  
                            j++;
                        }
                    }
                }else{
                    isSet[i] =0;
                }
                
            }
            for(auto &t : tempParents)
            {
                int chld = t.first;
                int prnt = t.second;
                auto lookup = parents.find(t.first);
                while(lookup!=parents.end() && lookup->second < prnt)
                {
                    chld = lookup->second;
                    lookup = parents.find(lookup->second);
                }
                if(parents.find(chld)== parents.end())
                {
                    parents[chld] = t.second;
                    isSet[prnt] = 0;
                }
                
                /*if(parents.find(t.first)==parents.end())
                {
                    parents[t.first] = t.second;
                    isSet[t.second] = 0;
                }*/
            }
            c++;
        }
        return done;
    }

    bool MapT12Totals(double *t12, std::map<int,int> &parents,std::vector<int> &subtotals, int maxIter,double tol)
    {
        parents.clear();

        
        int c = 0;
        int n = subtotals.size();
        std::vector<bool> isSet(n,1);
        //Rip and tear until it is done (unless maxIter is reached first)
        bool done =false;
        while(!done && c < maxIter)
        {
            for(int i=0;i<n;i++){
                if(t12[subtotals[i]]!=0){
                    double sum =0;
                    bool isEqual=false;
                    int j = i-1;
                    int countChildren =0;
                    while(j>=0){
                        if(isSet[j]){
                            sum+=t12[subtotals[j]];
                            countChildren++;
                        }
                        if(fabs(sum-t12[subtotals[i]]) <=tol && countChildren){
                            isEqual = true;
                            break;
                        }
                        j--;
                    }

                    if(isEqual){
                        //Builds a set with all its children
                        if(isEqual && isSet[i]){
                            //found a child turn it off
                            std::vector<int> children;
                            //Sets the parent of j-th row as the i-th row
                            while(j<i){
                                parents[subtotals[j]] = i;
                                j++;
                            }
                        }
                    }
                }else{
                    isSet[i]=0;
                }
                
            }
            for(auto &child : parents)
            {

                isSet[child.first] = 0;
            }
            c++;
        }
        return done;
    }

    void MapT12Levels(int* tree, int nr, std::vector<int> &levels,int totalIncomeRow,int totalExpenseRow)
    {
        std::fill(levels.begin(),levels.end(),1);
        //Calculates the level of expenses
        for(int i= totalExpenseRow; i > totalIncomeRow;i--)
        {
            for(int j =0;j< i;j++)
            {
                if(tree[i + j*nr]!=0)
                {
                    levels[j] = levels[i]+1;
                }
            }
        }
        //Calculates the level of incomes
        for(int i= totalIncomeRow; i >=0;i--)
        {
            for(int j =0;j< i;j++)
            {
                if(tree[i + j*nr]!=0)
                {
                    levels[j] = levels[i]+1;
                }
            }
        }
    }
    

    int ReadKeyValueData(csv_workbook &wb,double **matrix,char*** labels,int** tree,int &nr,int &nc, int &size, int &rr)
    {
        int rows =0;
        int cols =0;
        //Finds the number of rows
        for(int i =8;i< wb.nc;i+=12){
            int cr = strtol(wb.cells[i].value,0,10);
            int cc = strtol(wb.cells[i+1].value,0,10);
            if(cr> rows){
                rows = cr;
            }
            if(cc > cols){
                cols = cc;
            }
        }

        //Realocates space if there are more rows than previously allocated
        int newSize = rows*cols;
        if(newSize > 1000000000){
            throw "File to big to read";
        }
        if(newSize > size){
            size = newSize;
            if(*matrix)delete [] *matrix;
            if(*labels)delete [] *labels;
            *matrix = new double[size]{0};
            *labels = new char*[size]{0};
            if(*matrix ==0 && labels==0){
                throw "Error allocating memory for matrix or labels";
            }
        }else{
            memset(*matrix, 0, size*sizeof(double));
            memset(*labels, 0, size*sizeof(char*));
        }

        int ts = rows*rows;
        if(ts> rr){
            rr = ts;
            if(*tree)delete [] *tree;
            *tree = new int[ts]{0};
            if(*tree==0){
                throw "Error allocating memory for tree";
            }
        }else{
            memset(*tree,0, rr*sizeof(int));
        }

        for(int i =8;i< wb.nc;i+=12){
            int cr = strtol(wb.cells[i].value,0,10)-1;
            int cc = strtol(wb.cells[i+1].value,0,10)-1;
            if(cr >=0 && cc >=0 && cc < 496){
                char* value = wb.cells[i+2].value;
                remove_char(value,',');
                if((*labels)[cr + cc*rows]==0) 
                {
                    (*matrix)[cr + cc*rows] = strtod(value,0);
                    (*labels)[cr + cc*rows] = value;
                }
                
            }
            
        }

        nr = rows;
        nc = cols;

        return 0;
    }

    int ReadKeyValueDataHanger(csv_workbook &wb,double **matrix,char*** labels,int** tree,int &nr,int &nc, int &size, int &rr)
    {
        int rows =0;
        int cols =0;
        for(int i =8;i< wb.nc;i+=12){
            char* c1 = wb.cells[i].value;
            char* c2 = wb.cells[i+1].value;
            int cr = strtol(wb.cells[i].value,0,10);
            int cc = strtol(wb.cells[i+1].value,0,10);
            if(cr> rows && cr > 1){
                rows = cr;
            }
            if(cc > cols && cc >3 && cc < 501 && cr >1){
                cols = cc;
            }
        }

        if(rows ==0 || cols ==0){
            return __LINE__;
        }

        rows = rows -1;
        cols = cols - 3;

        if(rows <=0 || cols <=0){
            return __LINE__;
        }

        int newSize = rows*cols;
        if(newSize > 1000000000){
            throw "File to big to read";
        }
        if(newSize > size){
            size = newSize;
            if(*matrix)delete [] *matrix;
            if(*labels)delete [] *labels;
           
            *matrix = new double[size]{0};
            *labels = new char*[size]{0};
            if(*matrix ==0 && labels==0)
            {
                throw "Error allocating memory for matrix or labels";
            }
        }else{
            memset(*matrix, 0, size*sizeof(double));
            memset(*labels, 0, size*sizeof(char*));
        }

        int ts = rows*rows;
        if(ts> rr){
            rr = ts;
            if(*tree)delete [] *tree;
            *tree = new int[ts]{0};
            if(*tree==0){
                throw "Error allocating memory for tree";
            }
        }else{
            memset(*tree,0, rr*sizeof(int));
        }

        for(int i =8;i< wb.nc;i+=12){
            int cr = strtol(wb.cells[i].value,0,10)-2;
            int cc = strtol(wb.cells[i+1].value,0,10)-4;
            if(cr >=0 && cc >=0 && cc < 496){
                char* value = wb.cells[i+2].value;
                remove_char(value,',');
                if((*labels)[cr + cc*rows]==0) 
                {
                    (*matrix)[cr + cc*rows] = strtod(value,0);
                    (*labels)[cr + cc*rows] = value;
                }
                
            }
            
        }

        nr = rows;
        nc = cols;

        return 0;
    }
    
    
}

#endif