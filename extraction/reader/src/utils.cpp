
#include <string.h>
#include <fstream>
#include <filesystem>
#include "utils.h"



using namespace std;




char* get_ext(const std::string& s){
    size_t n= s.size()-1;
    char *result =(char*)s.c_str() + n;
    while(*result !='.'){
        result--;
        if(result == s.c_str()){
            return 0;
        }
    }
    return result+1;
}



std::vector<char*> parse_file(std::string &str){
    std::vector<char*> out;
    char* next;
    char* tok= strtok_r((char*)str.c_str(),".",&next);

    if(tok){
        out.push_back(tok);
        while(tok){
            tok= strtok_r(0,".",&next);
            if(tok)out.push_back(tok);
        }
    }else{
        return  out;
    }
    
    return out;
}

std::string to_string(cell &c){
    if(&c==0)return "";
    if(c.is_string){
        if(c.text)return string(c.text);
        return "";
    }else{
        return to_string(c.value);
    }
}

std::ostream &operator<<(std::ostream &out , cell & cell){
    if(cell.is_string){
        return out << cell.text;
    }else{
        return out << cell.value;
    }
}

std::string to_json(cell& c){
    std::string result;
    result.reserve(256);
    result += "{\"name\":\""; 
    result += c.name;
    result+= "\",\"row\":"+ std::to_string(c.row) +",\"col\":" + std::to_string(c.col);
    if(c.is_string){
        result+= ",\"value\":\"" + to_string(c) + "\"}";
    }else{
        result+= ",\"value\":" + to_string(c) + "}";
    }
    return result;
}

std::string to_json(xlsx_sheet& s){
    std::string result;
    result.reserve(65536);
    result += "{\"sheet\":\""; 
    result += s.name;
    result += "\", data:[";
    return result;
}
