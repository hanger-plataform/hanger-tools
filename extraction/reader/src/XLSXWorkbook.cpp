#include "XLSXWorkbook.h"


XLSXWorkbook::XLSXWorkbook(){};

XLSXWorkbook::XLSXWorkbook(const char* name)
{
    int status = read_xlsx_workbook((xlsx_workbook*)this,name);
    if(!status){
        is_valid = true;
    }
}

XLSXWorkbook::XLSXWorkbook(std::string &name):XLSXWorkbook(name.c_str()){}

XLSXWorkbook::~XLSXWorkbook()
{
    if(is_valid){
        free_xlsx_workbook((xlsx_workbook*)this);
    }
}

void XLSXWorkbook::open(const char* name)
{
    if(is_valid){
        free_xlsx_workbook((xlsx_workbook*)this);
    }
    int status = read_xlsx_workbook((xlsx_workbook*)this,name);
    if(!status){
        is_valid = true;
    }else{
        is_valid = false;
    }
}

bool XLSXWorkbook::status(){return is_valid;};


xlsx_sheet* XLSXWorkbook::get_sheet_id(int i){
    if(is_valid && i < this->ns){
        return this->sheets[i];
    }else{
        return 0;
    }
}

XLSXWorkbook::iterator XLSXWorkbook::begin(){
    if(is_valid){
        return {this->sheets,this->ns};
    }
    else{
        return {0,0};
    }
}

XLSXWorkbook::iterator XLSXWorkbook::end(){
    return {0,0};
}

std::string XLSXWorkbook::to_csv(int sheet,char sep,bool ln){
    if(sheet >= this->ns){
        return std::to_string(__LINE__);
    }else{
        auto sh = this->sheets[sheet];
        int maxcol =0;
        for(int i=0;i<sh->nc;i++){
            if(maxcol < sh->data[i].col){
                maxcol = sh->data[i].col;
            }
        }
        std::string result;
        result.reserve(1048576);
        int current_row = sh->data[0].row;
        int current_col =0;
        for(int i=0;i<sh->nc;i++){
            if(ln)result += std::to_string(current_row) + sep;
            result += std::string(this->name) +sep;
            result += std::string(sh->name) + sep;
            while(sh->data[i].row ==current_row){
                while(current_col<sh->data[i].col){
                    result+=sep;
                    current_col++;
                }
                result += to_string(sh->data[i]);
                i++;
                if(i==sh->nc)break;
            }
            while(current_col <maxcol){
                result+=sep;
                current_col++;
            }
            current_row = sh->data[i].row;
            i--;
            result += '\n';
            current_col =0;
        }
        return result;
    }
}

char** XLSXWorkbook::get_str_table(){
    return this->stringTable;
}

void normalize(XLSXWorkbook &wb){
    char** strtable = wb.get_str_table();
    while(*strtable){
        char* str = *strtable;
        while(*str){
            if(*str==';' || *str =='\n' || *str ==13){
                *str = ' ';
            }
            str++;
        }
        strtable++;
    }
}



std::string extract_all_xlsx(const char* path){
    std::string out;
    out.reserve(1048576);
    for(auto &file: std::filesystem::directory_iterator(path)){
         char *ext = get_ext(file.path().string());
         if(ext){
             if(!strcmp(ext,"xlsx")){
                XLSXWorkbook wb(file.path().string().c_str());
                normalize(wb);
                if(wb.status()){
                    for(int i=0;i<wb.ns;i++){
                        out += wb.to_csv(i,';',true);
                    }
                    
                }
             }
         }
    }
    return out;
}


std::vector<cell*> XLSXWorkbook::get_row(int sheet,int row){
    if(sheet > this->ns) throw "Invalid sheet";
    xlsx_sheet* sh = this->sheets[sheet];
    int nc = sh->nc;
    int i=0;
    while(sh->data[i].row!=row){
        i++;
        if(i==nc)return std::vector<cell*>();
    }
    std::vector<cell*> result;
    while(sh->data[i].row==row){
        result.push_back(&sh->data[i]);
        i++;
    }
    return result;
}

XLSXWorkbook::iterator::iterator(){}

XLSXWorkbook::iterator::iterator(xlsx_sheet** sheets, int ns):sheets(sheets),ns(ns){}

xlsx_sheet &XLSXWorkbook::iterator::operator*(){
    if(cs < ns && cs >=0 && sheets){
        return *sheets[cs];
    }else{
        throw "Invalid iterator";
    }
}

bool XLSXWorkbook::iterator::operator!=(const iterator & it){
    if(sheets == it.sheets && cs == it.cs){
        return false;
    }
    return true;
}
void XLSXWorkbook::iterator::operator++(){
        cs++;
        if(cs>=ns){
            sheets =0;
            cs=0;
        }
    }
void XLSXWorkbook::iterator::operator++(int){
    cs++;
    if(cs>=ns){
        sheets =0;
        cs=0;
    }
}

std::vector<cell*> XLSXWorkbook::get_column(int sheet,int col){
    if(sheet > this->ns) throw "Invalid sheet";
    xlsx_sheet* sh = this->sheets[sheet];
    std::vector<cell*> result;
    for(int i=0;i<sh->nc;i++){
        if(sh->data[i].col == col){
            result.push_back(&sh->data[i]);
        }
    }
    return result;
}

xlsx_sheet* XLSXWorkbook::operator[](unsigned int i){
    if(i<ns){
        return sheets[i];
    }
    return 0;
}

void XLSXWorkbook::free(){
    memset(this,0,sizeof(XLSXWorkbook));
    free_xlsx_workbook(this);
}