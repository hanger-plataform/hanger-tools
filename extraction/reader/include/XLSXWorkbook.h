#ifndef XLSXWORKBOOK_H
#define XLSXWORKBOOK_H

#include <xlsx-parser/xlsx_op.h>
#include "utils.h"
#include <string>
#include <vector>
#include <iostream>
#include "utils.h"
#include <string>


std::string extract_all_xlsx(const char* path);


class XLSXWorkbook:public xlsx_workbook{
    bool is_valid = false;
public:
    
    XLSXWorkbook();

    XLSXWorkbook(const char* name);
    XLSXWorkbook(std::string &name);

    ~XLSXWorkbook();

    void open(const char* name);

    bool status();

    int nstr();

    xlsx_sheet* get_sheet_id(int i);

    char** get_str_table();

    class iterator{
    private:
        xlsx_sheet** sheets=0;
        int ns=0;
        int cs=0;
    public:
        iterator();
        iterator(xlsx_sheet** sheets, int ns);
        xlsx_sheet &operator*();
        bool operator!=(const iterator & it);
        void operator++();
        void operator++(int);
    };

    iterator begin();
    iterator end();

    std::string to_csv(int sheet,char sep=',',bool ln = false);

    std::string to_json();

    std::vector<cell*> get_column(int sheet,int col);

    std::vector<cell*> get_row(int sheet,int row);

    xlsx_sheet* operator[](unsigned int i);

    void free();

};

#endif