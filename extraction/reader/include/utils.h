#ifndef UTILS_H
#define UTILS_H

#include <xlsx-parser/xlsx_op.h>
#include <string>
#include <vector>
#include <filesystem>


std::string to_string(cell &c);

char* get_ext(const std::string& s);

std::vector<char*> parse_file(std::string &str);

std::string to_string(cell &c);

std::ostream &operator<<(std::ostream &out , cell & cell);

std::string to_json(cell& c);

std::string to_json(xlsx_sheet& s);

#endif