#ifndef XLSXWORKBOOK_H
#define XLSXWORKBOOK_H

#include <xlsx-parser/xlsx_op.h>
#include "utils.h"
#include <string>
#include <vector>
#include <iostream>

std::ostream &operator<<(std::ostream &out , cell & cell){
    if(cell.is_string){
        return out << cell.value.str;
    }else{
        return out << cell.value.x;
    }
}

class XLSXWorkbook:public xlsx_workbook{
private:
    xlsx_workbook wb;
public:
    bool is_valid = false;
    inline XLSXWorkbook();

    inline XLSXWorkbook(const char* name);

    inline ~XLSXWorkbook();

    inline void open(const char* name);

    inline bool status();

    inline int size();

    inline int nstr();

    inline xlsx_sheet* get_sheet_id(int i);

    inline char** get_str_table();

    class iterator{
    private:
        xlsx_sheet** sheets=0;
        int ns=0;
        int cs=0;
    public:
        iterator(){}
        iterator(xlsx_sheet** sheets, int ns):sheets(sheets),ns(ns){}
        xlsx_sheet &operator*(){
            if(cs < ns && cs >=0 && sheets){
                return *sheets[cs];
            }else{
                throw "Invalid iterator";
            }
        }
        bool operator!=(const iterator & it){
            if(sheets == it.sheets && cs == it.cs){
                return false;
            }
            return true;
        }
        void operator++(){
            cs++;
            if(cs>=ns){
                sheets =0;
                cs=0;
            }
        }
        void operator++(int){
            cs++;
            if(cs>=ns){
                sheets =0;
                cs=0;
            }
        }
    };

    iterator begin();
    iterator end();

    std::string to_csv(int sheet,char sep=',',bool ln = false);

    std::string to_json();

    std::vector<cell*> get_column(int sheet,int col){
        if(sheet > wb.ns) throw "Invalid sheet";
        xlsx_sheet* sh = wb.sheets[sheet];
        std::vector<cell*> result;
        for(int i=0;i<sh->nc;i++){
            if(sh->data[i].col == col){
                result.push_back(&sh->data[i]);
            }
        }
        return result;
    }

    std::vector<cell*> get_row(int sheet,int row){
        if(sheet > wb.ns) throw "Invalid sheet";
        xlsx_sheet* sh = wb.sheets[sheet];
        int nc = sh->nc;
        int i=0;
        while(sh->data[i].row!=row){
            i++;
            if(i==nc)return std::vector<cell*>();
        }
        std::vector<cell*> result({&sh->data[i]});
        while(sh->data[i].row==row){
           
        }
        return (&sh->data+i,&sh->data+k+1);
    }

};

XLSXWorkbook::XLSXWorkbook(){};

XLSXWorkbook::XLSXWorkbook(const char* name)
{
    int status = read_xlsx_workbook(&wb,name);
    if(!status){
        is_valid = true;
    }
}

XLSXWorkbook::~XLSXWorkbook()
{
    if(is_valid){
        free_xlsx_workbook(&wb);
    }
}

void XLSXWorkbook::open(const char* name)
{
    if(is_valid){
        free_xlsx_workbook(&wb);
    }
    int status = read_xlsx_workbook(&wb,name);
    if(!status){
        is_valid = true;
    }else{
        is_valid = false;
    }
}

bool XLSXWorkbook::status(){return is_valid;};

int XLSXWorkbook::size(){
    if(is_valid)return wb.ns;
    return 0;
};

int XLSXWorkbook::nstr(){
    if(is_valid)return wb.nstr;
    return 0;
}
xlsx_sheet* XLSXWorkbook::get_sheet_id(int i){
    if(is_valid && i < wb.ns){
        return wb.sheets[i];
    }else{
        return 0;
    }
}

XLSXWorkbook::iterator XLSXWorkbook::begin(){
    if(is_valid){
        return {wb.sheets,wb.ns};
    }
    else{
        return {0,0};
    }
}

XLSXWorkbook::iterator XLSXWorkbook::end(){
    return {0,0};
}

std::string XLSXWorkbook::to_csv(int sheet,char sep,bool ln){
    if(sheet >= wb.ns){
        return std::to_string(__LINE__);
    }else{
        auto sh = wb.sheets[sheet];
        int maxcol =0;
        for(int i=0;i<sh->nc;i++){
            if(maxcol < sh->data[i].col){
                maxcol = sh->data[i].col;
            }
        }
        std::string result;
        result.reserve(1048576);
        int current_row = sh->data[0].row;
        int current_col =0;
        for(int i=0;i<sh->nc;i++){
            if(ln)result += std::to_string(current_row) + sep;
            result += std::string(wb.name) +sep;
            result += std::string(sh->name) + sep;
            while(sh->data[i].row ==current_row){
                while(current_col<sh->data[i].col){
                    result+=sep;
                    current_col++;
                }
                result += to_string(sh->data[i]);
                i++;
                if(i==sh->nc)break;
            }
            while(current_col <maxcol){
                result+=sep;
                current_col++;
            }
            current_row = sh->data[i].row;
            i--;
            result += '\n';
            current_col =0;
        }
        return result;
    }
}

char** XLSXWorkbook::get_str_table(){
    return wb.stringTable;
}

#endif