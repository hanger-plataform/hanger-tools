#include <xlsx-parser/csv_op.h>
#include "utils.h"


class CSVWorkbook: public csv_workbook{
private:
    bool is_valid;
public:
    CSVWorkbook();

    CSVWorkbook(const char* name,char sep, char lb);

    ~CSVWorkbook();

    void open(const char* name);

    bool status();

    std::string to_json();

    std::vector<cell*> get_column(int sheet,int col);

    std::vector<cell*> get_row(int sheet,int row);

};

CSVWorkbook::CSVWorkbook(){}

CSVWorkbook::CSVWorkbook(const char* name,char sep, char lb){
    is_valid = (read_csv_workbook((csv_workbook*)this,name,sep,lb) ==0);
}

CSVWorkbook::~CSVWorkbook(){
    if(is_valid){
        free_csv_workbook((csv_workbook*)this);
    }
}

void CSVWorkbook::open(const char* name){
    if(is_valid){
        free_csv_workbook((csv_workbook*)this);
    }
}

bool CSVWorkbook::status(){
    return is_valid;
}

std::string to_json(){
    return "";
}