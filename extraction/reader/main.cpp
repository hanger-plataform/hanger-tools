#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>
#include <algorithm>
#include <time.h>
#include "XLSXWorkbook.h"

std::vector<std::string> FileList(const char* fileList){
    std::ifstream files(fileList);
    if(files){
        std::vector<std::string> list;
        list.reserve(500);
        std::string line;
        while(std::getline(files,line)){
            list.push_back(line);
        }
        return list;
    }
    else{
       return std::vector<std::string>(); 
    }
}

int main(int argc, char* argv[]){
    //std::ifstream list("list_of_files.txt");

    if(argc>1){
        auto fileList = FileList(argv[1]);
    
        if(!fileList.empty()){
            int i;
            std::ofstream output("output.csv");
            time_t now = time(0);
            std::string date = ctime(&now);
            date.resize(date.size()-1);
            #pragma omp parallel for shared(output) shared(date)
            for(int i=0;i<fileList.size();i++){
                std::string wb_name = fileList[i];
                XLSXWorkbook wb(wb_name);
                std::string data("");
                data.reserve(1024000);
                if(wb.status()){
                    auto sh = wb.sheets[0];
                    auto isData = sh->nc;
                    for(size_t j=0;j< sh->nc;j++){
                        auto text = to_string(sh->data[j]);
                        if(text.find('\n'))std::replace(text.begin(),text.end(),'\n',',');
                        data+= date +";"+ fileList[i].substr(2) + ";"+ sh->name + ';' + std::string(sh->data[j].name) +';' +
                        std::to_string(sh->data[j].row)+ ';'+ std::to_string(sh->data[j].col)+ ";\"" + text + "\"\n";
                    }

                    #pragma omp critical
                    {
                        if(isData)output << data;
                    }

                }
            }
            
        }
    }
    
    return 0;
}