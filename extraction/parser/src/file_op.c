#include "file_op.h"
#include <stdlib.h>

size_t read_file_buffered(char** buffer, const char* name) {
    FILE* input = fopen(name, "r");
    if (input) {
        fseek(input, 0, SEEK_END);
        size_t nBytes = ftell(input);
        fseek(input, 0, SEEK_SET);
        *buffer = (char*)malloc(nBytes + 1);
        (*buffer)[nBytes] = 0;
        nBytes = fread(*buffer, 1, nBytes, input);
        fclose(input);
        return nBytes;
    }
    return 0;
}