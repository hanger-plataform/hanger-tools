#include "csv_op.h"

#include <stdio.h>

#include "string_op.h"

int read_csv_workbook(csv_workbook* wb,const char* name, char sep,char lb){
   
    FILE* input = fopen(name,"r");
    if(!input){
        memset(wb,0,sizeof(csv_workbook));
        return __LINE__;
    }

    fseek(input,0,SEEK_END);
    size_t size = ftell(input);
    fseek(input,0,SEEK_SET);

    char* buffer = (char*)malloc(size+1);
    wb->dataBuffer=buffer;
    wb->dataBufferSize = size;
    buffer[size]=0;

    if(fread(buffer,1,size,input)!=size){
        free(buffer);
        fclose(input);
        memset(wb,0,sizeof(csv_workbook));
        return __LINE__;
    }
    fclose(input);

    //Read number of cells
    int nc=0;
    int is_cell = 0;
    while(*buffer){
        if(*buffer == sep){
            if(is_cell)nc++;
            is_cell=0;
            *buffer=0;
        }else if(*buffer==lb){
            if(is_cell)nc++;
            is_cell=0;
        }else if(*buffer =='\"'){
            buffer++;
            while((*buffer!='\"')&(*buffer!=0))buffer++;
            is_cell=1;
            nc++;
        }
        else{
            is_cell=1;
        }
        buffer++;
    }

    wb->nc = nc;
    wb->data = (cell*)malloc(nc*sizeof(cell));

    char* end = buffer;
    buffer -= size;

    int row =0;
    int col =0;
    for(size_t i=0;i<nc;i++){
        while(!*buffer){
            buffer++;
            col++;
            if(buffer==end) return 0;
        }
        if(*buffer==lb){
            int col =0;
            *buffer=0;
            buffer++;
            if(buffer==end) return 0;
            row++;
        }else{
            wb->data[i].col=col;
            wb->data[i].row=row;
            wb->data[i].is_string=1;
            wb->data[i].text=buffer;
            //name_from_position(wb->data[i].name,col,row);
            while(*buffer){
                buffer++;
                if(*buffer==lb){
                    *buffer =0;
                    buffer++;
                    if(buffer==end) return 0;
                    int col =0;
                    row++;
                    break;
                }
            }
        }
    }

    
    return 0;
}

int free_csv_workbook(csv_workbook* wb){
    if(wb->data){
        free(wb->data);
    }
    if(wb->dataBuffer){
        free(wb->dataBuffer);
    }
}