#include "string_op.h"
#include <stdlib.h>
#include<stdio.h>




int name_from_position(char* name,unsigned int i, unsigned int j) {
    char temp[10];
    if(j<26){
        sprintf(name,"%c%d",j+65,i+1);
        return 0;
    }else{
        int pos=9;
        temp[9] = j%26 + 65;
        j/=26;
        while(j){
            pos--;
            if(j==26){
                temp[pos] = 90;
                break;
            }else if(j%26 ==0){
                temp[pos] = 90;
                j/=26;
                j--;
            }else{
                temp[pos] = j%26 + 64;
                j/=26;
            }
            
        }

        int k;
        for(k=0;pos<10;k++,pos++){
            name[k]= temp[pos];
        }
        sprintf(name+k,"%d",i+1);
    }
    return 0;
}

int get_array_pos(char* name, int* col, int* row) {
    if (!name) {
        return __LINE__;
    }
    char* bg = name;
    while((*name & 0b11011111)>=65 && (*name & 0b11011111) <= 90){
        name++;
    }
    
    char* nm = name -1;
    int tc =((*nm&0b11011111)-65);
    int pwr = 26;
    while(nm!=bg){
        nm--;
        tc += pwr*((*nm&0b11011111) - 64);
        pwr*=26;
        
    }

    *col = tc;
    *row = strtol(name,0,10) - 1;
    return 0;
}


void trim(char* str){
    if(!str){
        return;
    }
    char* ptr1 = str;
    while(*ptr1==' '){
        ptr1++;
    }
    if(ptr1!=str){
        while(*ptr1){
            *str = *ptr1;
            str++;
            ptr1++;
        }
        *str=0;
        str--;
        while(*str==' '){
            *str=0;
            str--;
        }
    }
}

void remove_tag(char* src,char* tag,char* endtag, size_t tgl){
    if (!src) {
        return;
    }
    while(*src==' ')src++;
    char* tg = strstr(src,tag);
    if(tg!=src)return;

    char* endtg = strstr(src, endtag);
    if(!endtg){ 
        return;
    };
    *endtg = 0;
    while(*src){
        *(src-tgl)=*src;
        src++;
    }
    *(src-tgl)=*src;
    return;
}
