#include "xlsx_op.h"
#include "sheet_info.h"

#ifndef NDEBUG
#include <mcheck.h>
#endif




char* get_next_cell(cell *tgt,char* buffer, char** stringTable) {
    char* tag = strstr(buffer, "<c r");
    if (!tag) return 0;
    char* name=0;
    char *r = 0,*t=0,*s=0,*str =0;

    int pos = 3;

    while (tag && tag[pos] != '>'){
        pos++;
        if(tag[pos]=='/'){
            pos = 3;
            tag = strstr(tag+pos, "<c r");
            t=0;
        }else if(tag[pos]=='t'&&tag[pos+1]=='='){
            t = tag + pos+3;
        }
    }


    if (!tag) return 0;
    tgt->inl =0;
    r = tag + 6;
    int pr = 2;
    while (r[pr] != '\"')pr++;
    char* vasd= r+pr;
    sprintf(tgt->name, "%.*s", pr, r);
    get_array_pos(tgt->name, &tgt->col, &tgt->row);
    if (!t) {
        char* v = strstr(tag, "<v>") + 3;
        tgt->value= strtod(v,0);
        tgt->text =0;
        tgt->is_string = 0;
    }
    else {
        if(!strncmp(t,"n\"",2)){
            char* v = strstr(tag, "<v>") + 3;
            tgt->value = strtod(v,0);
            tgt->text =0;
            tgt->is_string = 0;
        }else if(!strncmp(t,"s\"",2)){
            if (stringTable) {
                char* v = strstr(tag, "<v>") + 3;
                int val = strtol(v,0,10);
                tgt->text = stringTable[val];
                tgt->value =val; 
                tgt->is_string = 1;
            }
        }else if(!strncmp(t,"str\"",2)){
            tgt->value = 0;
            char* v = strstr(tag, "<v>") + 3;
            char* beg = v;
            while(*v!='<')v++;
            tgt->text = (char*)calloc(v-beg+1,1);
            memcpy(tgt->text,beg,v-beg);
            tgt->inl =1;
            tgt->is_string = 1;
        }else{
            tgt->value = 0;
            tgt->is_string = 0;
        }
        
    }
    

    return tag + pos;
}

xlsx_sheet * read_xlsx_sheet(zip_t* xlsx_file,int ids, char** stringTable, zip_stat_t *info){

    int status;
    status = zip_stat(xlsx_file,sheet_names[ids],0,info);
    
    if(status){
        return 0;
    }
    
    zip_file_t* input = zip_fopen(xlsx_file,sheet_names[ids],ZIP_FL_UNCHANGED);

    if(input){
        char* buffer;
        xlsx_sheet* dst = (xlsx_sheet *) malloc(sizeof(xlsx_sheet));

        if(!dst){
            zip_fclose(input);
            return 0;
        }

        buffer = (char*)malloc(info->size+1);
        buffer[info->size]=0;
        zip_fread(input,buffer,info->size);
        
        dst->data= get_table_data(buffer,stringTable,&dst->nc);
        dst->name = sheet_names2[ids];
        zip_fclose(input);
        free(buffer);
        return dst;
    }else{
        return 0;
    }
    
}

int read_xlsx_workbook(xlsx_workbook* wb,const char* name){


    int status;
    zip_file_t* input;
    zip_t* xlsx_file = zip_open(name,ZIP_RDONLY,&status);
    
    if(!xlsx_file){
        memset(wb,0,sizeof(xlsx_workbook));
        return  __LINE__;
    }
    
    memset(wb,0,sizeof(xlsx_workbook));
    
    zip_stat_init(&wb->info);
    status = zip_stat(xlsx_file,"[Content_Types].xml",0,&wb->info);
    int nsc =0;
    if(!status){
        status = zip_stat(xlsx_file,sheet_names[nsc],0,&wb->info);
        while(!status){
            status = zip_stat(xlsx_file,sheet_names[nsc],0,&wb->info);
            nsc++;
            if(nsc >=MAX_SHEETS) break;
        }
        if(nsc>0){
            wb->ns = nsc;
        }else{
            memset(wb,0,sizeof(xlsx_workbook));
            return __LINE__;
        }
        
    }else{
        return __LINE__;
    }
    
    wb->sheets = (xlsx_sheet**)calloc(nsc,sizeof(cell*));
    status = zip_stat(xlsx_file,"xl/sharedStrings.xml",0,&wb->info);
    if(!status){
        wb->strTableData = (char*)malloc(wb->info.size+1);
        if(!wb->strTableData){
            zip_discard(xlsx_file);
            memset(wb,0,sizeof(xlsx_workbook));
            return __LINE__;
        }
        
        input = zip_fopen(xlsx_file,"xl/sharedStrings.xml",ZIP_FL_UNCHANGED);
        
        if(!input){
            free( wb->strTableData);
            memset(wb,0,sizeof(xlsx_workbook));
            return __LINE__;
        }
        
        wb->strTableData[wb->info.size]=0;
        if(!zip_fread(input,wb->strTableData,wb->info.size)){
            zip_fclose(input);
            zip_discard(xlsx_file);
            wb->strTableData;
            memset(wb,0,sizeof(xlsx_workbook));
            return __LINE__;
        }
        
        wb->stringTable=get_string_table(wb->strTableData,&wb->nstr);
        zip_fclose(input);
        

    }else{
        memset(wb,0,sizeof(xlsx_workbook));
    }

    int ns =0;
    xlsx_sheet * dst = read_xlsx_sheet(xlsx_file,ns,wb->stringTable, &wb->info);
    
    while(dst && ns < nsc){
        wb->sheets[ns] = dst;
        wb->sheets[ns]->nstr = wb->nstr;
        ns++;
        dst = read_xlsx_sheet(xlsx_file,ns,wb->stringTable, &wb->info);
    }
    wb->ns = ns;
    zip_discard(xlsx_file);


  
    int n = strlen(name);
    wb->name = (char*)malloc(n+1);
    memcpy(wb->name,name,n+1);
    
    return 0;
    
}

void free_xlsx_workbook(xlsx_workbook* wb){
    
    if(wb->stringTable) free(wb->stringTable);
    for(int i=0;i< wb->ns;i++){
        if(wb->sheets[i]){
            if(wb->sheets[i]->data){
                for(int c=0;c<wb->sheets[i]->nc;c++){
                    if(wb->sheets[i]->data[c].inl)free(wb->sheets[i]->data[c].text);
                }
                free(wb->sheets[i]->data);
            }
        }
    }
    if(wb->sheets) free(wb->sheets);
    if(wb->name)free(wb->name);
}

size_t cell_from_name(const char* name) {
    size_t x = 0x0;
    int* lx = (int*)&x;
    int* ux = (int*)&x +1 ;
    int pos = 0;
    int temp = 0;
    while ((name[pos] & 0b1101111) >= 65 && (name[pos] & 0b1101111) <= 90) {
        temp = 27 * temp + (name[pos] & 0b1101111)-65;
        pos++;
    }
    *lx = temp;
    *ux = strtol(name + pos,0,10);
    return x;
}


cell* get_table_data(char* buffer,char** stringTable, size_t *nCells) {
    
    int n = count_cells(buffer);
    if(n==0)*nCells=0;
    if (n > 0) {
        cell* tableData = (cell*)calloc(n, sizeof(cell));
        size_t pos = 0;
        char* tag = get_next_cell(&tableData[0], buffer, stringTable);
        while (tag) {
            pos++;
            tag = get_next_cell(&tableData[pos], tag, stringTable);
        }
        *nCells = pos;
        return tableData;
    }
    return 0;
}

int count_cells(char* buffer) {
    int result = 0;
    while ((buffer = strstr(buffer+1, "<c r"))){

        while(*buffer!='>'){
            buffer++;
            if(*buffer=='/')break;
        }
        if(*buffer!='/'){
            result++;
        }
    }    
    return result;
}

size_t count_string_table(char* buffer){
    
    char* type =  strstr(buffer, "<x:si");
    size_t count =0;
    if(!type){
        char* next = strstr(buffer, "</si>");
        while(next){
            count++;
            next = strstr(next+1, "</si>");
        }
    }else{
        char* next = strstr(buffer, "</x:si>");
        while(next){
            count++;
            next = strstr(next+1, "</x:si>");
        }

    }

    return count;
}


char NA[] = "NA";
char** get_string_table(char* buffer, size_t* nStr) {
    
    if(!buffer) return 0;
    
    char* unq = strstr(buffer, "uniqueCount");
    size_t n;
    if(unq) 
    {   
        unq += 13;
        n= strtol(unq,0,10);
    }else{
        n = count_string_table(buffer);
    }
    if(!n) return 0;

    *nStr = n;
    char* type =  strstr(buffer, "<x:si");
    if(!type){
        if (n > 0) {
            char** table = (char**)calloc((n+1) , sizeof(char*));
            table[n]=0;

            char* begin_tag = strstr(buffer, "<si");
            char* end_tag ; 
            begin_tag = strstr(begin_tag, "<t");
            if(begin_tag[2]!='/'){
                while(*begin_tag!='>')begin_tag++;
                begin_tag++;
                end_tag = strstr(begin_tag, "</t>");

                if(end_tag){
                    *end_tag =0;
                }else{
                    free(table);
                    return 0;
                }
                table[0] =begin_tag;
            }else{
                end_tag = begin_tag;
                table[0] =NA;
            }
            
            size_t i;
            for (i = 1; i < n; i++){
                begin_tag = strstr(end_tag+1, "<si");
                begin_tag = strstr(begin_tag, "<t");
                if(begin_tag[2]!='/'){
                    if(!begin_tag){
                        table[i] =0;
                        break;
                    }
                    while(*begin_tag!='>')begin_tag++;
                    begin_tag++;
                    end_tag = strstr(begin_tag, "</t>");
                    if(end_tag){
                        *end_tag =0;
                    }else{
                        free(table);
                        return 0;
                    }
                    table[i] =begin_tag;
                }else{
                    end_tag = begin_tag;
                    table[i] =NA;
                }
            }
            table[i]=0;
            return table;
        }
        return 0;
    }else{
         if (n > 0) {
            char** table = (char**)calloc((n+1) , sizeof(char*));
            table[n]=0;

            char* begin_tag = strstr(buffer, "<x:si");
            char* end_tag ; 
            begin_tag = strstr(begin_tag, "<x:t");
            if(begin_tag[2]!='/'){
                while(*begin_tag!='>')begin_tag++;
                begin_tag++;
                end_tag = strstr(begin_tag, "</x:t>");

                if(end_tag){
                    *end_tag =0;
                }else{
                    free(table);
                    return 0;
                }
                table[0] =begin_tag;
            }else{
                end_tag = begin_tag;
                table[0] =NA;
            }
            
            size_t i;
            for (i = 1; i < n; i++){
                begin_tag = strstr(end_tag+1, "<x:si");
                begin_tag = strstr(begin_tag, "<x:t");
                if(begin_tag[2]!='/'){
                    if(!begin_tag){
                        table[i] =0;
                        break;
                    }
                    while(*begin_tag!='>')begin_tag++;
                    begin_tag++;
                    end_tag = strstr(begin_tag, "</x:t>");
                    if(end_tag){
                        *end_tag =0;
                    }else{
                        free(table);
                        return 0;
                    }
                    table[i] =begin_tag;
                }else{
                    end_tag = begin_tag;
                    table[i] =NA;
                }
            }
            table[i]=0;
            return table;
        }
        return 0;
        return 0;
    }
    
}

cell* get_cell(xlsx_sheet* sh, int i, int j){
    if(!sh)return 0;
    int a = 0, b = (sh->nc)/2,c = sh->nc -1;
    while(1){

        if(sh->data[a].row == i && sh->data[a].col == j){
            return &sh->data[a];
        }else if(sh->data[a].row > i){
            return 0;
        }else if(sh->data[a].row == i && sh->data[a].col > j){
            return 0;
        }
        
        cell v = sh->data[c];
        if(sh->data[c].row == i && sh->data[c].col == j){
            return &sh->data[c];
        }else if(sh->data[c].row < i){
            return 0;
        }else if(sh->data[c].row == i && sh->data[c].col < j){
            return 0;
        }

        
        if(sh->data[b].row == i && sh->data[b].col == j){
            return &sh->data[b];
        }else if(sh->data[b].row > i){
            c = b-1;
            a++;
            if(c<=a)return 0;
            b = (a+b)/2;
        }else if(sh->data[b].row == i){
            if(sh->data[b].col >j){
                c = b-1;
                a++;
                if(c<=a)return 0;
                b = (a+b)/2;
            }else{
                a = b+1;
                c = c-1;
                if(a>=c)return 0;
                b = (a+c)/2;
            }
        }else{
            a = b+1;
            c = c-1;
            if(a>=c)return 0;
            b = (a+c)/2;
        }
    }
}

cell* get_named_cell(xlsx_sheet* sh, char* name){
    int i,j;
    int status = get_array_pos(name,&j,&i);
    if(!status){
        return get_cell(sh,i,j);
    }
    return 0;
}

char* cell_to_json(cell *c){

    if(!c)return 0;
    
    char template1s[] ="{\"name\":\"%s\", \"value\":\"%s\"}";
    char template1f[] ="{\"name\":\"%s\", \"value\":%f}";
    char* out;
    if(c->is_string){
        size_t size = strlen(c->text);
        out = (char*)malloc(size + 50);
        sprintf(out,template1s,c->name,c->text);
    }else{
        out = (char*)malloc(57);
        sprintf(out,template1f,c->name,c->value);
    }
    return out;
}

const char XLSX_OP_DEBUG_INFO[] = {
    "Failure to open xlsx workbook"
};

