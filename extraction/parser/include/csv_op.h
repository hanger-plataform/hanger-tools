#ifndef CSV_OP_H
#define CSV_OP_H

#include "xlsx_op.h"


#ifdef __cplusplus
extern "C" {
#endif

typedef struct csv_workbook{
    cell* data;
    size_t nc;
    const char* name;
    char* dataBuffer;
    size_t dataBufferSize;
}csv_workbook;

int read_csv_workbook(csv_workbook* wb,const char* name, char sep,char lb);

int free_csv_workbook(csv_workbook* wb);

#ifdef __cplusplus
}
#endif

#endif