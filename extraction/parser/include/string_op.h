#ifndef STRING_OP_H
#define STRING_OP_H

#include <string.h>



#ifdef __cplusplus
extern "C" {
#endif


void remove_tag(char* src,char* tag,char* endtag, size_t tgl);

void trim(char* str);

int get_array_pos(char* name, int* col, int* row);

int name_from_position(char* name,unsigned int i, unsigned int j) ;

#ifdef __cplusplus
}
#endif

#endif