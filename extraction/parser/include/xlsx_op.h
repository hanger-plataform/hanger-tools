#ifndef XLSX_OP_H
#define XLSX_OP_H

#include <stdlib.h>
#include <zip.h>

#include "string_op.h"

#define LOWER(x) x & 0x00000000FFFFFFFF
#define UPPER(x) x >> 32
#define MAX_COLUMN 16384
#define MAX_ROW 1048576

#define MAX_SHEETS 40

#ifdef __cplusplus
extern "C" {
#endif

typedef struct cell {
    char name[12];
    double value;
    char* text;
    char is_string;
    char inl;
    int row;
    int col;
}cell;

typedef struct xlsx_sheet{
    cell* data;
    char** stringTable;
    size_t nstr;
    size_t nc;
    const char* name;
    char* strTableData;
}xlsx_sheet;

typedef struct xlsx_workbook{
    xlsx_sheet** sheets;
    char** stringTable;
    size_t nstr;
    int ns;
    char* name;
    char* strTableData;
    zip_stat_t info;
} xlsx_workbook;



int read_xlsx_sheet2(xlsx_sheet *dst,const char *name,int *err, char** stringTable);

void sheet_to_json(const char* name, xlsx_sheet *sheet);

int read_xlsx_workbook(xlsx_workbook* wb,const char* name);

void free_xlsx_workbook(xlsx_workbook* wb);

void print_sheet(xlsx_workbook *wb,size_t ids);

char* get_next_cell(cell *tgt,char* buffer, char** stringTable);

size_t cell_from_name(const char* name);

cell* get_table_data(char* buffer,char** stringTable, size_t *nCells);

cell* get_cell(xlsx_sheet* sh, int i, int j);

cell* get_named_cell(xlsx_sheet* sh, char* name);

int count_cells(char* buffer);

char** get_string_table(char* buffer, size_t* nStr);

#ifdef __cplusplus
}
#endif

#endif