# XLSX and CSV extraction tool

## Intructions

1. Install libzip from https://libzip.org/. Make sure that the .so(.a) file is in /usr/local/lib and the headers in /usr/local/include.
2. Make sure you have gcc installed
3. navigate to the extractor directory and run make (**$ make**)
4. After that is done run make install

To use the library add to the include directories /usr/local/include/xlsx-parser (**-I /usr/local/include/xlsx-parser**) and to the libs usr/local/lib (**-L/usr/local/lib**)

Additionaly you can use the file in and skip building lib.

