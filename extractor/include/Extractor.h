/**
 * @file Extractor.h
 * @author your name (you@domain.com)
 * @brief Extractor functions
 * @version 0.1
 * @date 2021-11-20
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef EXTRACTOR_H
#define EXTRACTOR_H

#include <string>
#include<iostream>
#include <vector>
#include <fstream>
#include <utility>
#include <set>
#include <omp.h>
#include <ctime>
#include <limits.h>

#include<boost/filesystem.hpp>
#include<boost/program_options.hpp>
#include<boost/algorithm/string.hpp>
#include<boost/regex.hpp>

#include "../../include/hanger-options.h"
#include "../../xlsx-parser/include/xlsx-parser.h"
#include "../../farmhash/include/farmhash.h"
#include "../../include/hanger-utils.h"

extern "C"{
    #include"xls.h"
}


//md5 dependencies
typedef unsigned char uint8;
typedef unsigned int uint32;
bool pg_md5_hash(const uint8 *buff, size_t len, char *hexsum);
//

namespace hanger{

    /**
     * @brief Converts file with name 'file' to key-value format
     * 
     * @param file 
     * @return std::string 
     */
    std::string ExtractKeyValue(const std::string &fileName,std::string &timestamp,char csv_sep);

    /**
     * @brief Converts file with name 'file' to key-value given a format  'fields'
     * 
     * @param file 
     * @param fields 
     * @param fnp 
     * @param shn 
     * @return std::string 
     */
    std::string ExtractKeyValue(const std::string &file,std::vector< std::pair<std::string,std::string> > &fields,int fnp, int shn,std::string & vp);

    /**
     * @brief Creates a key-value roll from a cell
     * 
     * @param src 
     * @param fields 
     * @return std::string 
     */
    std::string CreateRow(cell &src, std::vector< std::pair<std::string,std::string> > &fields);

    /**
     * @brief Creates a key-value roll from a cell given a format in 'fields'
     * 
     * @param cell 
     * @param fields 
     * @return std::string 
     */
    std::string CreateRow(xls::xlsCell *cell, std::vector< std::pair<std::string,std::string> > &fields);

    

    /**
     * @brief Extracts key value and inserts the numeric value as the last column before valuesa appendded at the end
     * 
     * @param fileName 
     * @param timestamp 
     * @param process_uuid 
     * @param csv_sep 
     * @return std::string 
     */
    std::string ExtractKeyValueNumeric(const std::string &fileName,std::string &timestamp,std::string &process_uuid, char csv_sep,std::string & vp);

    std::string ExtractKeyValueNumericCSVParquet(const std::string &fileName,
                                              std::string &timestamp,
                                              std::string &process_uuid,
                                              char csv_sep,std::string &vp, 
                                              std::vector< std::vector<std::string> >&parquetColumns
                                              );
    
    std::string ExtractKeyValue(const std::string &fileName,std::string &timestamp, char csv_sep,std::string & vp)
    {
        auto ext = boost::filesystem::extension(fileName);
        auto path = boost::filesystem::path(fileName);
        if(ext == ".xlsx" || ext == ".xlsm" || ext == ".XLSX" ){
            xlsx_workbook wb;
            int status = read_xlsx_workbook(&wb,fileName.c_str());
            if(status!=0){
                std::string errorStr;
                if(status>=100000){
                    errorStr = xlsx_get_error_info(status);
                }else{
                    errorStr = "Error reading sheet " + std::to_string(status);
                }
                free_xlsx_workbook(&wb);
                throw "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"" +errorStr + "\"";
            }else{
                std::string out;
                out.reserve(1048576);
                std::string value;
                value.reserve(1024);
                for(int sh =0; sh< wb.ns;sh++){
                    if(wb.sheets[sh])
                    {
                        std::string sheetName = wb.sheets[sh]->name;
                        sheetName = sheetName.substr(0,sheetName.find('.'));
                        for(size_t c =0;c< wb.sheets[sh]->nc;c++){
                            trim_cell(&wb.sheets[sh]->cells[c]);
                            value = wb.sheets[sh]->cells[c].value;
                            EscapeQuotes(value);
                            out.append("\"");
                            out.append(timestamp);
                            out.append( "\";\"");
                            out.append(path.filename().c_str());
                            out.append("\";\"");
                            out.append(sheetName);
                            out.append("\";");
                            out.append(std::to_string(c));
                            out.push_back(';');
                            out.append(std::to_string(wb.sheets[sh]->cells[c].row+1));
                            out.push_back(';');
                            out.append(std::to_string(wb.sheets[sh]->cells[c].col+1));
                            out.append(";\"");
                            out.append(value);
                            out.append(vp);
                        }
                    }
                }
                free_xlsx_workbook(&wb);
                return out;
            }
        }else if(ext ==".csv"){
            csv_workbook wb;
            int status = read_csv_workbook(&wb,fileName.c_str(),csv_sep,'\n');
            if(status!=0){
                free_csv_workbook(&wb);
                throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"csv read error\"";
            }else{
                std::string out;
                out.reserve(1048576);
                std::string value;
                value.reserve(1024);
                std::string sheetName = "sheet1";
                for(size_t c =0;c< wb.nc;c++){
                    trim_cell(&wb.cells[c]);
                    value = wb.cells[c].value;
                    while(value[value.size()-1]=='\"'&& !value.empty()){
                        value.resize(value.size()-1);
                    }
                    if(value.size()!=0){
                        EscapeQuotes(value);
                        out.append("\"");
                        out.append(timestamp);
                        out.append( "\";\"");
                        out.append(path.filename().c_str());
                        out.append("\";\"");
                        out.append(sheetName);
                        out.append("\";");
                        out.append(std::to_string(c));
                        out.push_back(';');
                        out.append(std::to_string(wb.cells[c].row+1));
                        out.push_back(';');
                        out.append(std::to_string(wb.cells[c].col+1));
                        out.append(";\"");
                        out.append(value);
                        out.append(vp);
                    }
                }
                free_csv_workbook(&wb);
                return out;
            }
        }else if(ext ==".xls" || ext ==".XLS"){
            xls::xls_error_t error = xls::LIBXLS_OK;
            xls::xlsWorkBook *wb = xls_open_file(fileName.c_str(), "UTF-8", &error);
            if (wb == NULL) {
                xls::xls_close_WB(wb);
                throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str())+ "\";\"failure to read xls workbook\"";
            }
            std::string out;
            out.reserve(1048576);
            std::string value;
            value.reserve(1024);
            for (int sh=0; sh<wb->sheets.count; sh++){
                xls::xlsWorkSheet *work_sheet = xls::xls_getWorkSheet(wb, sh);
                std::string sheetName = "sheet"+std::to_string(sh+1);
                if(!work_sheet){
                    xls::xls_close_WB(wb);
                    throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str())+ "\";\"failure to read xls worksheet\"";
                }
                error = xls::xls_parseWorkSheet(work_sheet);
                for (int j=0; j<work_sheet->rows.lastrow; j++){
                    xls::xlsRow *row = xls::xls_row(work_sheet, j);
                    if(!row){
                        xls::xls_close_WB(wb);
                        throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str())+ "\";\"failure to read xls row\"";
                    }
                    int c =0;
                    for (int k=0; k<=work_sheet->rows.lastcol; k++){
                        xls::xlsCell *cell = &row->cells.cell[k];
                        if(!cell) 
                        {
                            xls::xls_close_WB(wb);
                            throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str())+ "\";\"failure to read xls cell\"";
                        }
                        if (cell->id == XLS_RECORD_NUMBER){
                            value = std::to_string(cell->d);
                            out.append("\"");
                            out.append(timestamp);
                            out.append( "\";\"");
                            out.append(path.filename().c_str());
                            out.append("\";\"");
                            out.append(sheetName);
                            out.append("\";");
                            out.append(std::to_string(c));
                            out.push_back(';');
                            out.append(std::to_string(cell->row+1));
                            out.push_back(';');
                            out.append(std::to_string(cell->col+1));
                            out.append(";;");
                            out.append(value);
                            out.append(vp);
                        }else if(cell->str!=0 && strcmp(cell->str,"error")!=0){
                            value = std::string(cell->str);
                            EscapeQuotes(value);
                            out.append("\"");
                            out.append(timestamp);
                            out.append( "\";\"");
                            out.append(path.filename().c_str());
                            out.append("\";\"");
                            out.append(sheetName);
                            out.append("\";");
                            out.append(std::to_string(c));
                            out.push_back(';');
                            out.append(std::to_string(cell->row+1));
                            out.push_back(';');
                            out.append(std::to_string(cell->col+1));
                            out.append(";\"");
                            out.append(value);
                            out.append("\";");
                            out.append(vp);
                        }
                        c++;
                    }
                }
            }
            xls::xls_close_WB(wb);
            return out;
        }
        throw "\""+timestamp +"\";\"" + std::string(path.filename().c_str())+ "\";\"file format not supported\"";
    }

    std::string ExtractKeyValueNumeric(const std::string &fileName,std::string &timestamp,std::string &process_uuid, char csv_sep,std::string &vp)
    {
        auto ext = boost::filesystem::extension(fileName);
        auto path = boost::filesystem::path(fileName);
        std::string full_file_name = path.filename().c_str();
        std::string id_file;
        char md5str[64];
        pg_md5_hash((const uint8*)full_file_name.c_str(), full_file_name.size(),(char*) md5str);
        id_file = md5str;

        if(ext == ".xlsx" || ext == ".xlsm" || ext == ".XLSX"){
            xlsx_workbook wb;
            int status = read_xlsx_workbook(&wb,fileName.c_str());
            if(status!=0){
                std::string errorStr;
                if(status>=100000){
                    errorStr = xlsx_get_error_info(status);
                }else{
                    errorStr = "Error reading sheet " + std::to_string(status);
                }
                free_xlsx_workbook(&wb);
                throw "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"" +errorStr + "\"";
            }else{
                std::string out;
                out.reserve(1048576);
                //out.append("\"partition_field\";\"custom_primary_key\";\"etl_load_date\";\"id_file\";\"process_uuid\";\"full_file_name\";\"sheet_name\";\"cell_number\";\"idx_row\";\"idx_column\";\"key_value\";\"numeric_value\"\n");
                std::string value;
                value.reserve(1024);
                std::string unified;
                unified.reserve(1024);
                std::string fingerprint;
                fingerprint.reserve(1024);
                for(int sh =0; sh< wb.ns;sh++){
                    if(wb.sheets[sh])
                    {
                        std::string sheetName = wb.sheets[sh]->name;
                        sheetName = sheetName.substr(0,sheetName.find('.'));
                        for(size_t c =0;c< wb.sheets[sh]->nc;c++){
                            fingerprint = std::string(path.filename().c_str()) + "|" + sheetName + "|"+ std::to_string(c);
                            auto val =util::Fingerprint128 (fingerprint);
                            auto f1 = val.first;
                            auto f2 = val.second;
                            fingerprint = std::to_string(f1) + std::to_string(f2);
                            trim_cell(&wb.sheets[sh]->cells[c]);
                            value = wb.sheets[sh]->cells[c].value;
                            unified = value;
                            if(IsNumber(value)){
                                remove_char(wb.sheets[sh]->cells[c].value,',');
                                double numericValue = strtod(wb.sheets[sh]->cells[c].value,0);
                                value = ";;"+std::to_string(numericValue)  + ";";
                            }else{
                                value = ";\"" +value + "\";;";
                            }
                            out.append("\"");
                            out.append(full_file_name);
                            out.append( "\";\"");
                            out.append(fingerprint);
                            out.append( "\";\"");
                            out.append(timestamp);
                            out.append( "\";\"");
                            out.append(id_file);
                            out.append("\";\"");
                            out.append(process_uuid);
                            out.append("\";\"");
                            out.append(full_file_name);
                            out.append("\";\"");
                            out.append(sheetName);
                            out.append("\";");
                            out.append(std::to_string(c));
                            out.push_back(';');
                            out.append(std::to_string(wb.sheets[sh]->cells[c].row+1));
                            out.push_back(';');
                            out.append(std::to_string(wb.sheets[sh]->cells[c].col+1));
                            out.append(value);
                            out.append("\"");
                            out.append(unified);
                            out.push_back('\"');
                            out.append(vp);
                        }
                    }
                }
                free_xlsx_workbook(&wb);
                return out;
            }
        }else if(ext ==".csv" || ext ==".CSV"){
            csv_workbook wb;
            int status = read_csv_workbook(&wb,fileName.c_str(),csv_sep,'\n');
            if(status!=0){
                free_csv_workbook(&wb);
                throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"csv read error\"";
            }else{
                std::string out;
                out.reserve(1048576);
                //out.append("\"partition_field\";\"custom_primary_key\";\"etl_load_date\";\"id_file\";\"process_uuid\";\"full_file_name\";\"sheet_name\";\"cell_number\";\"idx_row\";\"idx_column\";\"key_value\";\"numeric_value\"\n");
                std::string value;
                value.reserve(1024);
                std::string unified;
                unified.reserve(1024);
                std::string fingerprint;
                fingerprint.reserve(1024);
                std::string sheetName = "sheet1";
                for(size_t c =0;c< wb.nc;c++){
                    fingerprint = std::string(path.filename().c_str()) + "|" + sheetName + "|"+ std::to_string(c);
                    auto val =util::Fingerprint128 (fingerprint);
                    auto f1 = val.first;
                    auto f2 = val.second;
                    fingerprint = std::to_string(f1) + std::to_string(f2);
                    trim_cell(&wb.cells[c]);
                    value = wb.cells[c].value;
                    unified = value;
                    if(IsNumber(value)){
                        remove_char(wb.cells[c].value,',');
                        double numericValue = strtod(wb.cells[c].value,0);
                        value = ";;"+std::to_string(numericValue) + ";" ;
                    }else{
                        value = ";\"" +value + "\";;";
                    }
                    out.append("\"");
                    out.append(full_file_name);
                    out.append( "\";\"");
                    out.append(fingerprint);
                    out.append( "\";\"");
                    out.append(timestamp);
                    out.append( "\";\"");
                    out.append(id_file);
                    out.append("\";\"");
                    out.append(process_uuid);
                    out.append("\";\"");
                    out.append(full_file_name);
                    out.append("\";\"");
                    out.append(sheetName);
                    out.append("\";");
                    out.append(std::to_string(c));
                    out.push_back(';');
                    out.append(std::to_string(wb.cells[c].row+1));
                    out.push_back(';');
                    out.append(std::to_string(wb.cells[c].col+1));
                    out.append(value);
                    out.append("\"");
                    out.append(unified);
                    out.push_back('\"');
                    out.append(vp);
                }
                free_csv_workbook(&wb);
                return out;
            }
        }else if(ext ==".xls" || ext ==".XLS" ){
            xls::xls_error_t error = xls::LIBXLS_OK;
            xls::xlsWorkBook *wb = xls_open_file(fileName.c_str(), "UTF-8", &error);
            if (wb == NULL) {
                xls::xls_close_WB(wb);
                throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str())+ "\";\"failure to read xls workbook\"";
            }
            std::string out;
            out.reserve(1048576);
            //out.append("\"partition_field\";\"custom_primary_key\";\"etl_load_date\";\"id_file\";\"process_uuid\";\"full_file_name\";\"sheet_name\";\"cell_number\";\"idx_row\";\"idx_column\";\"key_value\";\"numeric_value\"\n");
            std::string value;
            value.reserve(1024);
            std::string fingerprint;
            fingerprint.reserve(1024);
            for (int sh=0; sh<wb->sheets.count; sh++){
                xls::xlsWorkSheet *work_sheet = xls::xls_getWorkSheet(wb, sh);
                std::string sheetName = "sheet"+std::to_string(sh+1);
                if(!work_sheet){
                    xls::xls_close_WB(wb);
                    throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str())+ "\";\"failure to read xls worksheet\"";
                }
                error = xls::xls_parseWorkSheet(work_sheet);
                for (int j=0; j<work_sheet->rows.lastrow; j++){
                    xls::xlsRow *row = xls::xls_row(work_sheet, j);
                    if(!row){
                        xls::xls_close_WB(wb);
                        throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str())+ "\";\"failure to read xls row\"";
                    }
                    int c =0;
                    for (int k=0; k<=work_sheet->rows.lastcol; k++){
                        xls::xlsCell *cell = &row->cells.cell[k];
                        fingerprint = std::string(path.filename().c_str()) + "|" + sheetName + "|"+ std::to_string(k);
                        auto val =util::Fingerprint128 (fingerprint);
                        auto f1 = val.first;
                        auto f2 = val.second;
                        fingerprint = std::to_string(f1) + std::to_string(f2);
                        if(!cell) 
                        {
                            xls::xls_close_WB(wb);
                            throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str())+ "\";\"failure to read xls cell\"";
                        }
                        if (cell->id == XLS_RECORD_NUMBER){
                            value = std::to_string(cell->d);
                            out.append("\"");
                            out.append(full_file_name);
                            out.append( "\";\"");
                            out.append(fingerprint);
                            out.append( "\";\"");
                            out.append(timestamp);
                            out.append( "\";\"");
                            out.append(id_file);
                            out.append("\";\"");
                            out.append(process_uuid);
                            out.append("\";\"");
                            out.append(full_file_name);
                            out.append("\";\"");
                            out.append(sheetName);
                            out.append("\";");
                            out.append(std::to_string(c));
                            out.push_back(';');
                            out.append(std::to_string(cell->row+1));
                            out.push_back(';');
                            out.append(std::to_string(cell->col+1));
                            out.append(";");
                            out.append(";;");
                            out.append(value);
                            out.append(";\"");
                            out.append(value);
                            out.push_back('\"');
                            out.append(vp);
                        }else if(cell->str!=0 && strcmp(cell->str,"error")!=0){
                            value = std::string(cell->str);
                            EscapeQuotes(value);
                            if(!value.empty()){
                                if(IsNumber(value)){
                                    remove_char(cell->str,',');
                                    double numericValue = strtod(cell->str,0);
                                    value = ";;"+std::to_string(numericValue);
                                }else{
                                    value = ";\"" +value + "\"";
                                }
                                out.append("\"");
                                out.append(full_file_name);
                                out.append( "\";\"");
                                out.append(fingerprint);
                                out.append( "\";\"");
                                out.append(timestamp);
                                out.append( "\";\"");
                                out.append(id_file);
                                out.append("\";\"");
                                out.append(process_uuid);
                                out.append("\";\"");
                                out.append(full_file_name);
                                out.append("\";\"");
                                out.append(sheetName);
                                out.append("\";");
                                out.append(std::to_string(c));
                                out.push_back(';');
                                out.append(std::to_string(cell->row+1));
                                out.push_back(';');
                                out.append(std::to_string(cell->col+1));
                                out.append(value);
                                out.append(";");
                                out.append(value);
                                out.append(vp);
                            }
                        }
                        c++;
                    }
                }
            }
            xls::xls_close_WB(wb);
            return out;
        }
        throw "\""+timestamp +"\";\"" + std::string(path.filename().c_str())+ "\";\"file format not supported\"";
    }


    struct ParquetColumns
    {
        std::vector<std::string> full_file_name;
        std::vector<std::string> fingerprint;
        std::vector<std::string> timestamp;
        std::vector<std::string> id_file;
        std::vector<std::string> process_uuid;
        std::vector<std::string> sheetName;
        std::vector<uint32_t> cells;
        std::vector<uint32_t> rows;
        std::vector<uint32_t> columns;
        std::vector<std::string> key_value;
        std::vector<double> numeric_value;
        std::vector<std::string> unified;

        void Clear()
        {
            full_file_name.clear();
            fingerprint.clear();
            timestamp.clear();
            id_file.clear();
            process_uuid.clear();
            sheetName.clear();
            cells.clear();
            rows.clear();
            columns.clear();
            key_value.clear();
            numeric_value.clear();
            unified.clear();
        }
    };

    std::string ExtractKeyValueNumericCSVParquet(const std::string &fileName,
                                              std::string &timestamp,
                                              std::string &process_uuid,
                                              char csv_sep,std::string &vp, 
                                              ParquetColumns &parquetColumns)
    {
        auto ext = boost::filesystem::extension(fileName);
        auto path = boost::filesystem::path(fileName);
        std::string full_file_name = path.filename().c_str();
        std::string id_file;
        char md5str[64];
        pg_md5_hash((const uint8*)full_file_name.c_str(), full_file_name.size(),(char*) md5str);
        id_file = md5str;

        if(ext == ".xlsx" || ext == ".xlsm" || ext == ".XLSX"){
            xlsx_workbook wb;
            int status = read_xlsx_workbook(&wb,fileName.c_str());
            if(status!=0){
                std::string errorStr;
                if(status>=100000){
                    errorStr = xlsx_get_error_info(status);
                }else{
                    errorStr = "Error reading sheet " + std::to_string(status);
                }
                free_xlsx_workbook(&wb);
                throw "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"" +errorStr + "\"";
            }else{
                std::string out;
                out.reserve(1048576);
                //out.append("\"partition_field\";\"custom_primary_key\";\"etl_load_date\";\"id_file\";\"process_uuid\";\"full_file_name\";\"sheet_name\";\"cell_number\";\"idx_row\";\"idx_column\";\"key_value\";\"numeric_value\"\n");
                std::string value;
                value.reserve(1024);
                std::string unified;
                unified.reserve(1024);
                std::string fingerprint;
                fingerprint.reserve(1024);
                for(int sh =0; sh< wb.ns;sh++){
                    if(wb.sheets[sh])
                    {
                        std::string sheetName = wb.sheets[sh]->name;
                        sheetName = sheetName.substr(0,sheetName.find('.'));
                        for(size_t c =0;c< wb.sheets[sh]->nc;c++){
                            fingerprint = std::string(path.filename().c_str()) + "|" + sheetName + "|"+ std::to_string(c);
                            auto val =util::Fingerprint128 (fingerprint);
                            auto f1 = val.first;
                            auto f2 = val.second;
                            fingerprint = std::to_string(f1) + std::to_string(f2);
                            trim_cell(&wb.sheets[sh]->cells[c]);
                            value = wb.sheets[sh]->cells[c].value;
                            double numericValue = SNAN;
                            unified = value;
                            if(IsNumber(value)){
                                remove_char(wb.sheets[sh]->cells[c].value,',');
                                numericValue = strtod(wb.sheets[sh]->cells[c].value,0);
                                value = ";;"+std::to_string(numericValue)  + ";";
                            }else{
                                value = ";\"" +value + "\";;";
                            }
                            out.append("\"");
                            out.append(full_file_name);
                            out.append( "\";\"");
                            out.append(fingerprint);
                            out.append( "\";\"");
                            out.append(timestamp);
                            out.append( "\";\"");
                            out.append(id_file);
                            out.append("\";\"");
                            out.append(process_uuid);
                            out.append("\";\"");
                            out.append(full_file_name);
                            out.append("\";\"");
                            out.append(sheetName);
                            out.append("\";");
                            out.append(std::to_string(c));
                            out.push_back(';');
                            out.append(std::to_string(wb.sheets[sh]->cells[c].row+1));
                            out.push_back(';');
                            out.append(std::to_string(wb.sheets[sh]->cells[c].col+1));
                            out.append(value);
                            out.append("\"");
                            out.append(unified);
                            out.push_back('\"');
                            out.append(vp);
                            //parquetColumns.full_file_name.push_back(full_file_name);
                            parquetColumns.fingerprint.push_back(fingerprint);
                            //parquetColumns.timestamp.push_back(timestamp);
                            //parquetColumns.id_file.push_back(id_file);
                            //parquetColumns.process_uuid.push_back(process_uuid);
                            parquetColumns.sheetName.push_back(sheetName);
                            parquetColumns.cells.push_back(c);
                            parquetColumns.rows.push_back(wb.sheets[sh]->cells[c].row+1);
                            parquetColumns.columns.push_back(wb.sheets[sh]->cells[c].col+1);
                            parquetColumns.key_value.push_back(wb.sheets[sh]->cells[c].value);
                            parquetColumns.numeric_value.push_back(numericValue);
                            parquetColumns.unified.push_back(unified);
                        }
                    }
                }
                free_xlsx_workbook(&wb);
                return out;
            }
        }else if(ext ==".csv" || ext ==".CSV"){
            csv_workbook wb;
            int status = read_csv_workbook(&wb,fileName.c_str(),csv_sep,'\n');
            if(status!=0){
                free_csv_workbook(&wb);
                throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"csv read error\"";
            }else{
                std::string out;
                out.reserve(1048576);
                //out.append("\"partition_field\";\"custom_primary_key\";\"etl_load_date\";\"id_file\";\"process_uuid\";\"full_file_name\";\"sheet_name\";\"cell_number\";\"idx_row\";\"idx_column\";\"key_value\";\"numeric_value\"\n");
                std::string value;
                value.reserve(1024);
                std::string unified;
                unified.reserve(1024);
                std::string fingerprint;
                fingerprint.reserve(1024);
                std::string sheetName = "sheet1";
                for(size_t c =0;c< wb.nc;c++){
                    fingerprint = std::string(path.filename().c_str()) + "|" + sheetName + "|"+ std::to_string(c);
                    auto val =util::Fingerprint128 (fingerprint);
                    auto f1 = val.first;
                    auto f2 = val.second;
                    fingerprint = std::to_string(f1) + std::to_string(f2);
                    trim_cell(&wb.cells[c]);
                    value = wb.cells[c].value;
                    unified = value;
                    double numericValue = SNAN;
                    if(IsNumber(value)){
                        remove_char(wb.cells[c].value,',');
                        numericValue = strtod(wb.cells[c].value,0);
                        value = ";;"+std::to_string(numericValue) + ";" ;
                    }else{
                        value = ";\"" +value + "\";;";
                    }
                    out.append("\"");
                    out.append(full_file_name);
                    out.append( "\";\"");
                    out.append(fingerprint);
                    out.append( "\";\"");
                    out.append(timestamp);
                    out.append( "\";\"");
                    out.append(id_file);
                    out.append("\";\"");
                    out.append(process_uuid);
                    out.append("\";\"");
                    out.append(full_file_name);
                    out.append("\";\"");
                    out.append(sheetName);
                    out.append("\";");
                    out.append(std::to_string(c));
                    out.push_back(';');
                    out.append(std::to_string(wb.cells[c].row+1));
                    out.push_back(';');
                    out.append(std::to_string(wb.cells[c].col+1));
                    out.append(value);
                    out.append("\"");
                    out.append(unified);
                    out.push_back('\"');
                    out.append(vp);
                    //parquetColumns.full_file_name.push_back(full_file_name);
                    parquetColumns.fingerprint.push_back(fingerprint);
                    //parquetColumns.timestamp.push_back(timestamp);
                    //parquetColumns.id_file.push_back(id_file);
                    //parquetColumns.process_uuid.push_back(process_uuid);
                    parquetColumns.sheetName.push_back(sheetName);
                    parquetColumns.cells.push_back(c);
                    parquetColumns.rows.push_back(wb.cells[c].row+1);
                    parquetColumns.columns.push_back(wb.cells[c].col+1);
                    parquetColumns.key_value.push_back(wb.cells[c].value);
                    parquetColumns.numeric_value.push_back(numericValue);
                    parquetColumns.unified.push_back(unified);
                }
                free_csv_workbook(&wb);
                return out;
            }
        }else if(ext ==".xls" || ext ==".XLS" ){
            xls::xls_error_t error = xls::LIBXLS_OK;
            xls::xlsWorkBook *wb = xls_open_file(fileName.c_str(), "UTF-8", &error);
            if (wb == NULL) {
                xls::xls_close_WB(wb);
                throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str())+ "\";\"failure to read xls workbook\"";
            }
            std::string out;
            out.reserve(1048576);
            //out.append("\"partition_field\";\"custom_primary_key\";\"etl_load_date\";\"id_file\";\"process_uuid\";\"full_file_name\";\"sheet_name\";\"cell_number\";\"idx_row\";\"idx_column\";\"key_value\";\"numeric_value\"\n");
            std::string value;
            value.reserve(1024);
            std::string fingerprint;
            fingerprint.reserve(1024);
            for (int sh=0; sh<wb->sheets.count; sh++){
                xls::xlsWorkSheet *work_sheet = xls::xls_getWorkSheet(wb, sh);
                std::string sheetName = "sheet"+std::to_string(sh+1);
                if(!work_sheet){
                    xls::xls_close_WB(wb);
                    throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str())+ "\";\"failure to read xls worksheet\"";
                }
                error = xls::xls_parseWorkSheet(work_sheet);
                for (int j=0; j<work_sheet->rows.lastrow; j++){
                    xls::xlsRow *row = xls::xls_row(work_sheet, j);
                    if(!row){
                        xls::xls_close_WB(wb);
                        throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str())+ "\";\"failure to read xls row\"";
                    }
                    int c =0;
                    for (int k=0; k<=work_sheet->rows.lastcol; k++){
                        xls::xlsCell *cell = &row->cells.cell[k];
                        fingerprint = std::string(path.filename().c_str()) + "|" + sheetName + "|"+ std::to_string(k);
                        auto val =util::Fingerprint128 (fingerprint);
                        auto f1 = val.first;
                        auto f2 = val.second;
                        fingerprint = std::to_string(f1) + std::to_string(f2);
                        if(!cell) 
                        {
                            xls::xls_close_WB(wb);
                            throw  "\""+timestamp +"\";\""+ std::string(path.filename().c_str())+ "\";\"failure to read xls cell\"";
                        }
                        if (cell->id == XLS_RECORD_NUMBER){
                            value = std::to_string(cell->d);
                            out.append("\"");
                            out.append(full_file_name);
                            out.append( "\";\"");
                            out.append(fingerprint);
                            out.append( "\";\"");
                            out.append(timestamp);
                            out.append( "\";\"");
                            out.append(id_file);
                            out.append("\";\"");
                            out.append(process_uuid);
                            out.append("\";\"");
                            out.append(full_file_name);
                            out.append("\";\"");
                            out.append(sheetName);
                            out.append("\";");
                            out.append(std::to_string(c));
                            out.push_back(';');
                            out.append(std::to_string(cell->row+1));
                            out.push_back(';');
                            out.append(std::to_string(cell->col+1));
                            out.append(";");
                            out.append(";;");
                            out.append(value);
                            out.append(";\"");
                            out.append(value);
                            out.push_back('\"');
                            out.append(vp);
                        }else if(cell->str!=0 && strcmp(cell->str,"error")!=0){
                            value = std::string(cell->str);
                            EscapeQuotes(value);
                            if(!value.empty()){
                                if(IsNumber(value)){
                                    remove_char(cell->str,',');
                                    double numericValue = strtod(cell->str,0);
                                    value = ";;"+std::to_string(numericValue);
                                }else{
                                    value = ";\"" +value + "\"";
                                }
                                out.append("\"");
                                out.append(full_file_name);
                                out.append( "\";\"");
                                out.append(fingerprint);
                                out.append( "\";\"");
                                out.append(timestamp);
                                out.append( "\";\"");
                                out.append(id_file);
                                out.append("\";\"");
                                out.append(process_uuid);
                                out.append("\";\"");
                                out.append(full_file_name);
                                out.append("\";\"");
                                out.append(sheetName);
                                out.append("\";");
                                out.append(std::to_string(c));
                                out.push_back(';');
                                out.append(std::to_string(cell->row+1));
                                out.push_back(';');
                                out.append(std::to_string(cell->col+1));
                                out.append(value);
                                out.append(";");
                                out.append(value);
                                out.append(vp);
                            }
                        }
                        c++;
                    }
                }
            }
            xls::xls_close_WB(wb);
            return out;
        }
        throw "\""+timestamp +"\";\"" + std::string(path.filename().c_str())+ "\";\"file format not supported\"";
    }

};

#endif