#include <iostream>
#include <vector>
#include <ctime>
#include "Extractor.h"


namespace fs = boost::filesystem; 


struct template_field{
  std::string name;
  int i;
  std::string (*f) (const std::string &str);

  bool operator==(const std::string & str) const{
    return name==str;
  }

};

int AppendFromTemplate(std::vector<template_field> &formats, std::string &str, char** row,int rn, int nc)
{

  int kvp = -1;
  std::string prekv;
  prekv.reserve(1024);
  std::string poskv;
  poskv.reserve(1024);
  
  std::string *strp = &prekv;
  for(int i=0; i< formats.size();i++){
    if(!(formats[i]=="keyvalue")){
      
      if(formats[i]=="cell"){
        if(row[formats[i].i]){
          strp->append("\"");
          strp->append(row[formats[i].i]);
          strp->append("\";");
        }else{
          strp->append("\"");
          strp->append(row[formats[i].i]);
          strp->append("\";");
        }
      }else if(formats[i]=="formula"){
         if(row[formats[i].i]){
          strp->append("\"");
          strp->append(formats[i].f(row[formats[i].i]));
          strp->append("\";");
        }else{
          strp->append("\"");
          strp->append(formats[i].f(""));
          strp->append("\";");
        }
      }else{
        strp->append("\"");
        strp->append(formats[i].name);
        strp->append("\";");
      }
    }else{
      strp = &poskv;
      kvp = i;
    }
  }
  if(!poskv.empty()){
    poskv[poskv.size()-1]='\n';
  }else{
    poskv="\n";
  }

  if(kvp!=-1){
    std::string out;
    out.reserve(1024);
    for(int i=0;i<nc;i++){
      if(row[i]){
        out.append(std::to_string(rn));
        out.append(";");
        out.append(std::to_string(i));
        out.append(";\"");
        out.append(row[i]);
        out.append("\";");
        str.append(prekv);
        str.append(out);
        str.append(poskv);
       out.clear();
      }
    }

  }else{
    str.append(prekv);
    str.append(poskv);
  }
  return 0;
}

int main(int argc, char* argv[]){


  /*std::set<std::string> validOptions = {"input",
                                        "output",
                                        "outputpath",
                                        "logpath",
                                        "logfile",
                                        "timestamp",
                                        "iptfield",
                                        "iptlb",
                                        "optfield",
                                        "optlb",
                                        "ext",
                                        "fields"
                                        };

  std::string input="";
  std::string output="output.csv";
  std::string outputpath="";
  std::string logpath="";
  std::string logfile="errors.log"; 
  std::string timestamp=""; 
  std::string iptfield=";"; 
  std::string iptlb="\n"; 
  std::string optfield=";"; 
  std::string optlb="\n"; 
  std::string ext = ".csv";
  std::vector<std::string> fields ={};

  
  std::map<std::string,std::string> options;

  bool isDir=false;
  bool outputPerFile = false;

  try{
    options = hanger::GetOptions(argc,argv,validOptions);
  }catch(std::string &err){
    std::cout << err <<std::endl;
    return -1;
  }

  if(hanger::CheckParameter("input",input,options)){
    if(boost::filesystem::is_directory(input)){
      isDir=true;
    }else{
      if(!boost::filesystem::exists(input)){
        std::cout << "Input file " << input <<" does not exists"  <<std::endl;
        return -1;
      }
    }
  }else{
    std::cout << "No input provided" <<std::endl;
    return -1;
  }

  hanger::CheckParameter("ext",ext,options);

  if(hanger::CheckParameter("outputpath",outputpath,options)){
    if(outputpath[outputpath.size()-1]=='/'){
      outputpath.resize(outputpath.size()-1);
    }
    if(!boost::filesystem::exists(outputpath)){
      boost::filesystem::create_directory(outputpath);
    }

  }
  if(!hanger::CheckParameter("output",output,options)){
    outputPerFile = true;
  }
  

  if(!outputPerFile){
    if(!outputpath.empty()){
      output = outputpath + "/" + output + ext;
    }
  }

  if(hanger::CheckParameter("logpath",logpath,options)){
    if(outputpath[logpath.size()-1]=='/'){
      outputpath.resize(logpath.size()-1);
    }
    if(!boost::filesystem::exists(logpath)){
      boost::filesystem::create_directory(logpath);
    }
  }

  hanger::CheckParameter("logfile",logfile,options);
  if(!logpath.empty()) logfile = logpath + '/' + logfile;

  std::ofstream errorLog(logfile);

  if(!hanger::CheckParameter("timestamp",timestamp,options)){
    time_t time = std::time(0);
    timestamp = std::asctime(std::localtime(&time));
    timestamp.resize(timestamp.size()-1);
  }



  hanger::CheckParameter("iptfield",logfile,options);
  hanger::CheckParameter("iptlb",logfile,options);
  hanger::CheckParameter("optfield",logfile,options);
  hanger::CheckParameter("optlb",logfile,options);

  
  


  if(isDir && outputPerFile){
    
  }else if(isDir && ! outputPerFile)
  {

  }else if(!isDir && outputPerFile)
  {

  }else
  {
    
  }*/

  xlsx_workbook wb;
  read_xlsx_workbook(&wb,"/home/erik/Documents/data/tree-test/built-trees.xlsx");

  auto m = max_col(*wb.sheets[0]);
  std::string out;
  char** test = (char**)calloc(m,sizeof(char*));
  int row = wb.sheets[0]->cells[0].row;
  cell * c =&wb.sheets[0]->cells[0];
  int col =0;
  template_field f1 = {"cell",0};
  template_field f2 = {"formula",1,[](const std::string &str){return std::string("out_of_order");}};
  template_field f3 = {"keyvalue"};
  std::vector<template_field> vec = {f1,f2,f3};
  while(c <=(&wb.sheets[0]->cells[wb.sheets[0]->nc-1])){
    if(c->row!=row){
      AppendFromTemplate(vec,out,test,row,col);
      row = c->row;
      memset(test,0,m*sizeof(char*));
    }else{
      test[c->col] = c->value;
      col = c->col;
      c++;
    }
    
  }
  std::cout << out <<std::endl;

  return 0;
}

