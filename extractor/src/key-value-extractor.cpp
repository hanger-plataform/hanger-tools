/**
 * @file key-value-extractor.cpp
 * @author Erik  (eriksef2@gmail.com)
 * @brief Converts xlsx, xls and csv files to key-value format
 * @version 0.1
 * @date 2021-11-19
 * @copyright Copyright (c) 2021
 * 
 **/

#include "Extractor.h"


extern "C"{
    #include<xls.h>
}


namespace fs = boost::filesystem; 
using namespace hanger;

char csv_sep = ',';

std::string timestamp = "";

int main(int argc, char* argv[]){

    if(argc<5)return __LINE__;
    
    std::ofstream output;
    std::string outputPath;
    std::string extensionName;
    if(argc >= 6){
        if(!fs::is_directory(argv[5])){
            output.open(argv[5]);
            std::cout << argv[5] <<std::endl;
        }else{
            outputPath = std::string(argv[5])+ "/";
        }
    }
    if(argc>=7){
        extensionName = argv[6];
    }else{
        extensionName=".csv";
    }

    std::ofstream errorLog;

    errorLog.open(argv[2]);
    if(!errorLog)return __LINE__;

    csv_sep = argv[4][0];
    timestamp = argv[3];

    auto ext = boost::filesystem::extension(argv[1]);
    if( fs::is_directory(argv[1])){
        //Dumps key-values read from directory to stdout
        if(!output.is_open() && argc < 6){
            std::vector<std::string> fileList;
            fs::directory_iterator it(argv[1]),endIt;
            while(it!=endIt){
                fileList.push_back((*it).path().c_str());
                it++;
            }
            int i;
        #pragma omp parallel shared(fileList) shared(errorLog)
            {
                std::string result;
                result.reserve(1048576);
                
            #pragma omp  for 
                for(int i=0;i<fileList.size();i++){
                    result.clear();
                    try{
                        result = ExtractKeyValue(fileList[i],timestamp,csv_sep);
                        if(!result.empty()) std::cout << result << std::flush;
                    }catch(std::string & err){
                            std::cout << "Error while reading from " + fileList[i] <<std::endl;
                        #pragma omp critical
                            {
                                errorLog << err << std::endl;
                            }
                            
                    }catch(...){
                        std::cout << "Error while reading from " + fileList[i] <<std::endl;
                        #pragma omp critical
                            {
                                auto path = boost::filesystem::path(fileList[i]);
                                errorLog << "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"unknown error\"";
                            }
                    }
                }
            }
        }
        //Dumps key-values read from directory to output
        else if(output.is_open()){
            std::vector<std::string> fileList;
            fs::directory_iterator it(argv[1]),endIt;
            while(it!=endIt){
                fileList.push_back((*it).path().c_str());
                it++;
            }
            int i;
            #pragma omp parallel shared(fileList) shared(output) shared(errorLog)
                {
                    std::string result;
                    result.reserve(1048576);
                #pragma omp  for 
                    for(int i=0;i<fileList.size();i++){
                        result.clear();
                        try{
                            std::cout <<"Reading from " << fileList[i] << std::endl;
                            result = ExtractKeyValue(fileList[i],timestamp,csv_sep);
                            if(!result.empty()){
                                #pragma omp critical
                                {
                                    output << result << std::flush;
                                    
                                }
                                
                            }
                        }catch(std::string & err){
                            std::cout << "Error while reading from " + fileList[i] <<std::endl;
                        #pragma omp critical
                            {
                                errorLog << err << std::endl;
                            }
                            
                        }catch(...){
                            std::cout << "Error while reading from " + fileList[i] <<std::endl;
                            #pragma omp critical
                            {
                                auto path = boost::filesystem::path(fileList[i]);
                                errorLog << "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"unknown error\"";
                            }
                        }
                    }
                }
        }
        //Dumps key-values read from directory to individual files saved in outputPath
        else{
            std::vector<std::string> fileList;
            fs::directory_iterator it(argv[1]),endIt;
            while(it!=endIt){
                fileList.push_back((*it).path().c_str());
                it++;
            }
            int i;
            #pragma omp parallel shared(fileList) shared(errorLog) shared(outputPath)
                {
                    std::string result;
                    result.reserve(1048576);
                #pragma omp  for 
                    for(int i=0;i<fileList.size();i++){
                        auto path = boost::filesystem::path(fileList[i]);
                        std::string savePath;
                        if(boost::filesystem::extension(path.filename().c_str())==".csv"){
                            savePath = outputPath+ std::string(path.filename().c_str());
                        }else{
                            savePath = outputPath+ std::string(path.filename().c_str())+ extensionName;
                        }
                        
                        result.clear();
                        try{
                            std::cout <<"Reading from " << fileList[i] << " Saving to " << savePath <<std::endl;
                            result = ExtractKeyValue(fileList[i],timestamp,csv_sep);
                            std::ofstream locOutput(savePath);
                            if(!result.empty()){
                                locOutput << result << std::flush;
                            }
                            locOutput.close();
                        }catch(std::string & err){
                            std::cout << "Error while reading from " + fileList[i] <<std::endl;
                        #pragma omp critical
                            {
                                errorLog << err << std::endl;
                            }
                            
                        }catch(...){
                            std::cout << "Error while reading from " + fileList[i] <<std::endl;
                            #pragma omp critical
                            {
                                auto path = boost::filesystem::path(fileList[i]);
                                errorLog << "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"unknown error\"";
                            }
                        }
                    }
                }
        }
    }
    //Dumps key-values read from a single file to stdout
    else{
        std::string result;
        result.reserve(1048576);
        try{
            result = ExtractKeyValue(argv[1],timestamp,csv_sep);
                if(!result.empty()) std::cout << result << std::flush;
            }catch(std::string & err){
                std::cout << "Error while reading from " + std::string(argv[1]) <<std::endl;
            #pragma omp critical
                    {
                        errorLog << err << std::endl;
                    }
                
            }catch(...){
                std::cout << "Error while reading from " + std::string(argv[1]) <<std::endl;
                #pragma omp critical
                    {
                        auto path = boost::filesystem::path(std::string(argv[1]));
                        errorLog << "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"unknown error\"";
                    }
            }
    }

    errorLog.close();
    output.close();
    return 0;
}


