/**
 * @file key-value-extractor-str-num.cpp
 * @author Erik  (eriksef2@gmail.com)
 * @brief Converts xlsx, xls and csv files to key-value format
 * @version 0.1
 * @date 2021-11-19
 * @copyright Copyright (c) 2021
 * 
 **/


#include "Extractor.h"

extern "C"{
    #include<xls.h>
}


namespace fs = boost::filesystem; 
using namespace hanger;

//Global Variables
char csv_sep = ',';

std::string vp;
std::string process_uuid = "";
std::string timestamp;
std::string outputPath;
std::string parquetPath;
std::string extensionName;

std::vector< std::pair<std::string,std::string> > vpp;

std::ofstream errorLog;
std::ofstream output;

bool onlyCsv;
bool onlyParquet;

inline void ReadInputFromDirectoryOutputStd(const char* dir);
inline void ReadInputFromDirectoryOutputFile(const char* dir);
inline void ReadInputFromDirectoryOutputFiles(const char* dir);
inline void ReadInputFromFileOutputStd(const char* file);

//inline void pqcsv_ReadInputFromDirectoryOutputStd(const char* dir);
//inline void pqcsv_ReadInputFromDirectoryOutputFile(const char* dir);
inline void pqcsv_ReadInputFromDirectoryOutputFiles(const char* dir);
//inline void pqcsv_ReadInputFromFileOutputStd(const char* file);

int main(int argc, char* argv[]){
    //Requires input (file/directory) an error log, a process_uuid, a csv separator, an output (file/directory) a file extension and variable param
    if(argc<5){
        return __LINE__;
    }

    //Creates a time stamp based on the current time
    
    

    timestamp = GenerateTimestamp();


    if(argc >= 6){
        std::vector<std::string> outputVector;
        boost::split(outputVector,argv[5],boost::is_any_of(","));
        if(outputVector.size()==1)
        {
            onlyCsv = true;
            if(!fs::is_directory(argv[5])){
                output.open(argv[5]);
                std::cout << argv[5] <<std::endl;
            }else{
                outputPath = std::string(argv[5])+ "/";
            }
        }else
        {
            if(outputVector[0]==""){
                onlyParquet = true;
                if(!fs::is_directory(outputVector[1])){
                    std::cout << "Only per file mode is currently supported for parquet" <<std::endl;
                    return -9;
                }else{
                    parquetPath = outputVector[1]+ "/";
                }
            }else
            {
                onlyParquet = onlyCsv =false;
                if(!fs::is_directory(outputVector[0])){
                     std::cout << "Only per file mode is currently supported for parquet" <<std::endl;
                }else
                {
                    outputPath = outputVector[0]+ "/";
                    parquetPath = outputVector[1]+ "/";
                }
            }
        }
    }
    
    
    if(argc>=7){
        extensionName = argv[6];
    }else{
        extensionName=".csv";
    }


    if(argc>=8){
        for(int i=7;i< argc;i++){
            vp.append(";\"");
            vp.append(argv[i]);
            vp.append("\"");
        }
        vp.append("\n");
    }else{
        vp = "\n";
    }

    

    errorLog.open(argv[2]);
    if(!errorLog)return __LINE__;

    csv_sep = argv[4][0];
    process_uuid = argv[3];

    auto ext = boost::filesystem::extension(argv[1]);
    if(onlyCsv)
    {
        if( fs::is_directory(argv[1])){
            //Dumps key-values read from directory to stdout
            if(!output.is_open() && argc < 6)
            {
                ReadInputFromDirectoryOutputStd(argv[1]);
            }
            //Dumps key-values read from directory to output
            else if(output.is_open())
            {
                ReadInputFromDirectoryOutputFile(argv[1]);
            }
            //Dumps key-values read from directory to individual files saved in outputPath
            else
            {
                ReadInputFromDirectoryOutputFiles(argv[1]);
            }
        }
        //Dumps key-values read from a single file to stdout
        else
        {
            ReadInputFromFileOutputStd(argv[1]);
        }
    }

    errorLog.close();
    output.close();
    return 0;
}



void ReadInputFromDirectoryOutputStd(const char* dir)
{
    std::vector<std::string> fileList;
    fs::directory_iterator it(dir),endIt;
    while(it!=endIt){
        fileList.push_back((*it).path().c_str());
        it++;
    }
    int i;
    #pragma omp parallel shared(fileList) shared(errorLog)
        {
            std::string result;
            result.reserve(1048576);
            
        #pragma omp  for 
            for(int i=0;i<fileList.size();i++){
                result.clear();
                try{
                    result = ExtractKeyValueNumeric(fileList[i],timestamp,process_uuid,csv_sep,vp);
                    if(!result.empty()) std::cout << result << std::flush;
                }catch(std::string & err){
                        std::cout << "Error while reading from " + fileList[i] <<std::endl;
                    #pragma omp critical
                        {
                            errorLog << err << std::endl;
                        }
                        
                }catch(...){
                    std::cout << "Error while reading from " + fileList[i] <<std::endl;
                    #pragma omp critical
                        {
                            auto path = boost::filesystem::path(fileList[i]);
                            errorLog << "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"unknown error\"";
                        }
                }
            }
    }
}
void ReadInputFromDirectoryOutputFile(const char* dir)
{
    std::vector<std::string> fileList;
    fs::directory_iterator it(dir),endIt;
    while(it!=endIt){
        fileList.push_back((*it).path().c_str());
        it++;
    }
    int i;
    #pragma omp parallel shared(fileList) shared(output) shared(errorLog)
        {
            std::string result;
            result.reserve(1048576);

            #pragma omp  for 
            for(int i=0;i<fileList.size();i++){
                result.clear();
                try{
                    std::cout <<"Reading from " << fileList[i] << std::endl;
                    result = ExtractKeyValueNumeric(fileList[i],timestamp,process_uuid,csv_sep,vp);
                    if(!result.empty()){
                        #pragma omp critical
                        {
                            output << result << std::flush;
                            
                        }
                        
                    }
                }catch(std::string & err){
                    std::cout << "Error while reading from " + fileList[i] <<std::endl;
                #pragma omp critical
                    {
                        errorLog << err << std::endl;
                    }
                    
                }catch(...){
                    std::cout << "Error while reading from " + fileList[i] <<std::endl;
                    #pragma omp critical
                    {
                        auto path = boost::filesystem::path(fileList[i]);
                        errorLog << "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"unknown error\"";
                    }
                }
            }
        }
}
void ReadInputFromDirectoryOutputFiles(const char* dir)
{
    std::vector<std::string> fileList;
    fs::directory_iterator it(dir),endIt;
    while(it!=endIt){
        fileList.push_back((*it).path().c_str());
        it++;
    }
    int i;
    #pragma omp parallel shared(fileList) shared(errorLog) shared(outputPath)
        {
            std::string result;
            result.reserve(1048576);

            #pragma omp  for 
            for(int i=0;i<fileList.size();i++){
                auto path = boost::filesystem::path(fileList[i]);
                std::string savePath;
                if(boost::filesystem::extension(path.filename().c_str())==".csv"){
                    savePath = outputPath+ std::string(path.filename().c_str());
                }else{
                    savePath = outputPath+ std::string(path.filename().c_str())+ extensionName;
                }
                
                result.clear();
                std::ofstream locOutput;
                try{
                    std::cout <<"Reading from " << fileList[i] << " Saving to " << savePath <<std::endl;
                    result = ExtractKeyValueNumeric(fileList[i],timestamp,process_uuid,csv_sep,vp);
                    
                    if(!result.empty()){
                        locOutput.open(savePath);
                        locOutput << result << std::flush;
                        locOutput.close();
                    }
                }catch(std::string & err){
                    std::cout << "Error while reading from " + fileList[i] <<std::endl;
                #pragma omp critical
                    {
                        errorLog << err << std::endl;
                    }
                    
                }catch(...){
                    std::cout << "Error while reading from " + fileList[i] <<std::endl;
                    #pragma omp critical
                    {
                        auto path = boost::filesystem::path(fileList[i]);
                        errorLog << "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"unknown error\"";
                    }
                }
            }
        }
}
void ReadInputFromFileOutputStd(const char* file)
{
    std::string result;
    result.reserve(1048576);
    try{
        result = ExtractKeyValueNumeric(file,timestamp,process_uuid,csv_sep,vp);
            if(!result.empty()) std::cout << result << std::flush;
        }catch(std::string & err){
            std::cout << "Error while reading from " + std::string(file) <<std::endl;
        #pragma omp critical
                {
                    errorLog << err << std::endl;
                }
            
        }catch(...){
            std::cout << "Error while reading from " + std::string(file) <<std::endl;
            #pragma omp critical
                {
                    auto path = boost::filesystem::path(std::string(file));
                    errorLog << "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"unknown error\"";
                }
        }
}

void pqcsv_ReadInputFromDirectoryOutputFiles(const char* dir)
{
    std::vector<std::string> fileList;
    fs::directory_iterator it(dir),endIt;
    while(it!=endIt){
        fileList.push_back((*it).path().c_str());
        it++;
    }
    int i;
    #pragma omp parallel shared(fileList) shared(errorLog) shared(outputPath)
        {
            std::string result;
            result.reserve(1048576);
            hanger::ParquetColumns parquetColumns;
            #pragma omp  for 
            for(int i=0;i<fileList.size();i++){
                auto path = boost::filesystem::path(fileList[i]);
                std::string savePath;
                parquetColumns.Clear();
                if(boost::filesystem::extension(path.filename().c_str())==".csv"){
                    savePath = outputPath+ std::string(path.filename().c_str());
                }else{
                    savePath = outputPath+ std::string(path.filename().c_str())+ extensionName;
                }
                
                result.clear();
                std::ofstream locOutput;
                try{
                    std::cout <<"Reading from " << fileList[i] << " Saving to " << savePath <<std::endl;
                    result = ExtractKeyValueNumericCSVParquet(fileList[i],timestamp,process_uuid,csv_sep,vp,parquetColumns);
                    
                    if(!result.empty()){
                        locOutput.open(savePath);
                        locOutput << result << std::flush;
                        locOutput.close();
                    }
                }catch(std::string & err){
                    std::cout << "Error while reading from " + fileList[i] <<std::endl;
                #pragma omp critical
                    {
                        errorLog << err << std::endl;
                    }
                    
                }catch(...){
                    std::cout << "Error while reading from " + fileList[i] <<std::endl;
                    #pragma omp critical
                    {
                        auto path = boost::filesystem::path(fileList[i]);
                        errorLog << "\""+timestamp +"\";\""+ std::string(path.filename().c_str()) + "\";\"unknown error\"";
                    }
                }
            }
        }
}



