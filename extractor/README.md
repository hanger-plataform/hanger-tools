# Key Value Extractor

## Dependencies

Currently only linux is supported

0. g++ (install your distro build-essential)
1. libzip - install isntructions from https://libzip.org/
2. libxls - install from https://github.com/libxls/libxls
3. Boost - install from https://www.boost.org/
4. xlsx-parser from https://gitlab.com/hanger-plataform/hanger-tools/-/tree/dev-extractor/xlsx-parser

## Compiling

navigate this directory and type make in the terminal. The binary will be place in ../bin

## Use isntructions

The program takes the following arguments:

0. $(BIN_DIRECTORY)/key-value-extractor
1. directory where files for extration are located
2. The name of the error log file 
3. A time stamp
4. The separator for csv files
5. [optional] output directory or file (if directory it must already exist)
6. [optional] If the output is a directory the extension of the output files

It should loook like:
../bin/key-value-extractor sample-data "$(date)_error_log.log" "$(date)" \; output-dir ".csv"

# Other uses
