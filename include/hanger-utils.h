#ifndef HANGER_UTILS_H
#define HANGER_UTILS_H

#include <string>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

namespace hanger{
  
	/**
     * @brief Escape quotes in a string object
     * 
     * @param str 
     */
    void EscapeQuotes(std::string &str);
	
    void EscapeQuotes(std::string &str)
    {
        int pos = 0;
        int n = str.size();
        while(pos < n){
        int c =0;
            while(str[pos]=='\"'){
                pos++;
                c^=1;
                if(pos==n)break;
            }
            if(c){
                if(pos < n){
                    str.insert(pos,1,'\"');
                }
                else if(pos==n){
                    str.push_back('\"');
                    return;
                }
            }
            n = str.size();
            pos++;
        }
    }
	
	 /**
     * @brief Lists files in a directory with extension ext
     * 
     * @param dir 
     * @param ext Extension of files.
     * @return std::vector<boost::filesystem::path> 
     */
    std::vector<boost::filesystem::path> GetFileList(const std::string &dir, const std::string &ext="");

    std::vector<boost::filesystem::path> GetFileList(const std::string &dir, const std::string &ext){
        std::vector<boost::filesystem::path> fileList;
        if(!boost::filesystem::is_directory(dir)){
        return fileList;
        }
        boost::filesystem::directory_iterator itt(dir),endDir;
        if(ext.empty()){
            while(itt!=endDir){
                fileList.push_back((*itt).path());
                itt++;
            }
            
        }else{
            if(boost::filesystem::extension((*itt).path().filename().c_str()) ==ext){
                fileList.push_back((*itt).path());
                itt++;
            }
            
        }
        
        return fileList;
    }
	
	 /**
     * @brief Get the File List object wich names match the regex reg
     * 
     * @param dir 
     * @param reg 
     * @return std::vector<boost::filesystem::path> 
    **/
    std::vector<boost::filesystem::path> GetFileList(const std::string &dir, const boost::regex &reg);

    std::vector<boost::filesystem::path> GetFileList(const std::string &dir, const boost::regex &reg){
        std::vector<boost::filesystem::path> fileList;
        if(!boost::filesystem::is_directory(dir)){
        return fileList;
        }
		boost::filesystem::directory_iterator itt(dir),endDir;
        while(itt!=endDir){
            if(boost::regex_match((*itt).path().string(),reg)){
                fileList.push_back((*itt).path());
            }
            itt++;
        }
        return fileList;
    }
	

    bool IsNumber(std::string & str);

    bool IsNumber(std::string & str)
    {
        char comm = ',';
        char per = '.';
        char plmn=1;
        int cnt=0;
        bool firstComma = true;
        for(auto c : str){
            if((c<48 || c > 57) && (c!=43 && c!=44 && c!= 45 && c!=46)){
                return false;
                
            }else if(c=='+'|| c=='-'){
                if(plmn){
                    plmn=0;
                }else{
                    return false;
                }
            }
            else if(c==','){
                if((comm==0 || per==0) || (!firstComma && cnt!=3)){
                    return false;
                }else{
                    if(firstComma && cnt > 3){
                        return false;
                    }
                    firstComma = false;
                    comm^=',';
                    cnt =0;
                }
            }
            else if(c == '.'){
                if(per==0){
                    return false;
                }
            }
            else{
                comm=',';
                plmn=0;
                cnt ++;
            }
        }
        return true;
    }

    std::string NumberToTimestamp(int x){
        if(x<10)return "0" + std::to_string(x);
        return std::to_string(x);
    }

    inline std::string GenerateTimestamp()
    {
        time_t t;
        tm *timeinfo;
        time(&t);
        timeinfo = localtime(&t);
        return std::to_string(timeinfo->tm_year+1900) + "-" + hanger::NumberToTimestamp(timeinfo->tm_mon+1)+
                 "-" + hanger::NumberToTimestamp(timeinfo->tm_mday) + " " + hanger::NumberToTimestamp(timeinfo->tm_hour)
                 + ":" + hanger::NumberToTimestamp(timeinfo->tm_min) + ":" + hanger::NumberToTimestamp(timeinfo->tm_sec);
    }
};

#endif