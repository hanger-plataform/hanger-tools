#ifndef HANGER_OPTIONS_H
#define HANGER_OPTIONS_H

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <time.h>
#include <chrono>

#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <utility>


namespace hanger{
  
  std::vector<std::string> GetVectorArguments(std::string &args);
  std::map<std::string,std::string> GetOptions(int argc, char* argv[], std::set<std::string> &validOptions);
  bool CheckParameter(const std::string &option, std::string &value,std::map<std::string,std::string> & options);


  std::vector<std::string> GetVectorArguments(std::string &args){
    if(args.empty()) return{};
    if(args[0]=='['){
      if(args[args.size()-1]==']'){
        args = args.substr(1,args.size()-2);
      }else{
        args = args.substr(1);
      }
    }else if(args[args.size()-1]==']'){
      args.resize(args.size()-1);
    }
    std::vector<std::string> result;
    boost::split(result,args,[](char c){return c ==',';});
    return result;
  }

  std::map<std::string,std::string> GetOptions(int argc, char* argv[], std::set<std::string> &validOptions){
    std::map<std::string,std::string> options;
    for(int i=1;i< argc;i++){
      std::string arg = argv[i];
      int pos = arg.find("=");
      if(pos ==std::string::npos){
        if(validOptions.find(arg)!=validOptions.end()){
          if(options.find(arg)==options.end()){
            options[arg] = "";
          }else{
            throw "Option " + arg +" already used";
          }
        }else{
          throw "Invalid option given: " + arg;
        }
      }else{
        std::string value = arg.substr(pos+1);
        arg = arg.substr(0,pos);
        if(validOptions.find(arg)!=validOptions.end()){
        if(options.find(arg)==options.end()){
          options[arg] = value;
        }else{
          throw "Option " + arg +" already used";
        }
        }else{
          throw "Invalid option given: " + arg;
        }
      }
    }
    return options;
  }

  bool CheckParameter(const std::string &option, std::string &value,std::map<std::string,std::string> & options){
    if(options.find(option)!=options.end()){
      value = options[option];
      return true;
    }else{
      return false;
    }
  }

};

#endif