#include "xlsx_op.h"
#include "sheet_info.h"

char NA[] = "NA";

char* get_next_cell(cell *tgt,char* data, char** stringTable) {
    char* tag = strstr(data, "<c r");
    if (!tag) return 0;
    char* name=0;
    char *r = 0,*t=0,*s=0,*str =0;

    int pos = 3;

    while (tag && tag[pos] != '>'){
        pos++;
        if(tag[pos]=='/'){
            pos = 3;
            tag = strstr(tag+pos, "<c r");
            t=0;
        }else if(tag[pos]=='t'&&tag[pos+1]=='='){
            t = tag + pos+3;
        }
    }

    if (!tag) return 0;

    r = tag + 6;
    int pr = 2;
    while (r[pr] != '\"')pr++;

    char* vasd= r+pr;
    sprintf(tgt->name, "%.*s", pr, r);

    get_array_pos(tgt->name, &tgt->col, &tgt->row);

    char* v = strstr(tag, "<v>") + 3;
    if (!t) {
        tgt->value = v;
        tgt->strPos = 0xFFFFFFFFFFFFFFFF;
        tag = v;
        while (*tag != '<')tag++;
        *tag = 0;
        tag++;
    }
    else {
        if(!strncmp(t,"n\"",2)){
            char* v = strstr(tag, "<v>") + 3;
            tgt->value = v;
            tgt->strPos = 0xFFFFFFFFFFFFFFFF;
            tag = v;
            while (*tag != '<')tag++;
            *tag = 0;
            tag++;
        }else if(!strncmp(t,"s\"",2)){
            if (stringTable) {
                char* v = strstr(tag, "<v>") + 3;
                int val = strtol(v,0,10);
                tgt->value = stringTable[val];
                tgt->strPos = val;
                tag = v;
                while (*tag != '<')tag++;
                *tag = 0;
                tag++;
            }
            else {
                tgt->value = NA;
                tgt->strPos = 0xFFFFFFFFFFFFFFFF;
            }
        }else if(!strncmp(t,"str\"",2)){
            tgt->value = 0;
            char* v = strstr(tag, "<v>") + 3;
            tgt->value = v;
            tgt->strPos = 0xFFFFFFFFFFFFFFFF;
            tag = v;
            while (*tag != '<')tag++;
            *tag = 0;
            tag++;
        }else{
            tgt->value = NA;
            tgt->strPos = 0xFFFFFFFFFFFFFFFF;
        }
        
    }
    
    return tag+1;
}


sheet * read_xlsx_sheet(zip_t* xlsx_file,char* sheetName, char** stringTable, zip_stat_t *info, int *xlsx_error){

    int status;
    status = zip_stat(xlsx_file, sheetName,0,info);
    
    if(status){
        *xlsx_error = XLSX_ERROR_OPENING_SHEET;
        return 0;
    }
    
    zip_file_t* input = zip_fopen(xlsx_file,sheetName,ZIP_FL_UNCHANGED);

    if(input){
        char* buffer;
        sheet* dst = (sheet *) malloc(sizeof (sheet));

        if(!dst){
            *xlsx_error = XLSX_MEMORY_ALLOCATION_ERROR;
            zip_fclose(input);
            return 0;
        }

        buffer = (char*)malloc(info->size+1);
        buffer[info->size]=0;
        zip_fread(input,buffer,info->size);

        status = check_formulas(buffer);
        if(status){
            zip_fclose(input);
            free(buffer);
            free(dst);
            *xlsx_error = XLSX_UNCALCULATED_FORMULA_ERROR;
            return 0;
        }
        cell* temp =  get_table_data(buffer,stringTable,&dst->nc);
        if(temp==0){
            *xlsx_error = XLSX_MEMORY_ALLOCATION_ERROR;
            free(buffer);
            free(dst);
            free(temp);
            zip_fclose(input);
            return 0;
        }
        dst->cells=temp;
        dst->name = sheetName+14;
        zip_fclose(input);
        dst->data = buffer;
        return dst;

    }else{
        *xlsx_error = XLSX_CANNOT_OPEN_SHEET_ERROR;
        return 0;
    }
    
}



int read_xlsx_workbook(xlsx_workbook* wb,const char* name){

    int status;
    int xlsx_error;
    zip_stat_t info;
    zip_file_t* input;

    zip_t* xlsx_file = zip_open(name,ZIP_RDONLY,&status);

    if(!xlsx_file){
        memset(wb,0,sizeof(xlsx_workbook));
        return XLSX_OPEN_ZIP_ERROR;
    }
    
    memset(wb,0,sizeof(xlsx_workbook));

    zip_stat_init(&info);
    status = zip_stat(xlsx_file,"[Content_Types].xml",0,&info);
    int nsc =0;
    if(!status){
        char* contents = (char*)malloc(info.size + 1);
        contents[info.size] = 0;

        input = zip_fopen(xlsx_file, "[Content_Types].xml", ZIP_FL_UNCHANGED);
        zip_fread(input, contents, info.size);
        zip_fclose(input);

        char* worksheet_name = strstr(contents, "xl/worksheets/sheet");
        wb->contents = contents;
        while (worksheet_name) {
            nsc++;
            worksheet_name = strstr(worksheet_name+1, "xl/worksheets/sheet");
        }
        if (nsc == 0) {
            zip_discard(xlsx_file);
            free_xlsx_workbook(wb);
            return XLSX_EMPTY_WORKBOOK_ERROR;
        }
        wb->sheetNames = (char**)malloc(nsc * sizeof(char*));
        size_t pos = 0;
        worksheet_name = strstr(contents, "xl/worksheets/sheet");
        while (worksheet_name) {
            wb->sheetNames[pos] = worksheet_name;
            while (*worksheet_name != '\"')worksheet_name++;
            *worksheet_name = 0;
            pos++;
            worksheet_name = strstr(worksheet_name + 1, "xl/worksheets/sheet");
        }  
    }else{
        return XLSX_FAILURE_TO_READ_CONTENTS_ERROR;
    }
    
    

    status = zip_stat(xlsx_file,"xl/sharedStrings.xml",0,&info);
    if(!status){
        wb->strTableData = (char*)malloc(info.size+1);
        if(!wb->strTableData){
            zip_discard(xlsx_file);
            free_xlsx_workbook(wb);
            return XLSX_FAILURE_TO_ALLOCATE_STRTABLE_ERROR;
        }
        
        input = zip_fopen(xlsx_file,"xl/sharedStrings.xml",ZIP_FL_UNCHANGED);
        
        if(!input){
            free_xlsx_workbook(wb);
            zip_discard(xlsx_file);
            zip_fclose(input);
            return XLSX_FAILURE_TO_OPEN_STRTABLE_FILE_ERROR;
        }
        
        wb->strTableData[info.size]=0;

        
        if(!zip_fread(input,wb->strTableData, info.size)){
            zip_fclose(input);
            zip_discard(xlsx_file);
            free_xlsx_workbook(wb);
            return XLSX_FAILURE_TO_READ_STRTABLE_FILE_ERROR;
        }
        
        wb->stringTable=get_string_table(wb->strTableData,&wb->nstr);
        zip_fclose(input);
    }

    int ns =0;
    wb->sheets = (sheet**)calloc(nsc,sizeof(sheet*));
    sheet * dst = read_xlsx_sheet(xlsx_file,wb->sheetNames[ns],wb->stringTable, &info,&xlsx_error);
    /*if(dst==0){
        zip_discard(xlsx_file);
        free_xlsx_workbook(wb);
        return xlsx_error;
    }*/
    wb->sheets[0] = dst;
    if(dst)wb->sheets[0]->nstr = wb->nstr;
    ns = 1;
    while(dst && ns < nsc){
        dst = read_xlsx_sheet(xlsx_file, wb->sheetNames[ns],wb->stringTable, &info,&xlsx_error);
        /*if(dst==0){
            wb->ns = ns;
            free_xlsx_workbook(wb);
            zip_discard(xlsx_file);
            return xlsx_error;
        }*/
        wb->sheets[ns] = dst;
        if(dst)wb->sheets[ns]->nstr = wb->nstr;
        ns++;
    }

    wb->ns = ns;
    zip_discard(xlsx_file);


  
    int n = strlen(name);
    wb->name = (char*)malloc(n+1);
    memcpy(wb->name,name,n+1);
    wb->status =XLSX_SUCCESS;
    return 0;
    
}

void free_xlsx_workbook(xlsx_workbook* wb){
    
    
    for(int i=0;i< wb->ns;i++){
        if(wb->sheets[i]){
            if(wb->sheets[i]->data){
                free(wb->sheets[i]->data);
                free(wb->sheets[i]->cells);
            }
        }
    }
    
    if(wb->sheets) free(wb->sheets);
    if(wb->sheetNames)free(wb->sheetNames);
    if(wb->stringTable) free(wb->stringTable);
    if(wb->name)free(wb->name);
    if(wb->contents)free(wb->contents);
    if(wb->strTableData)free(wb->strTableData);
    memset(wb,0,sizeof(xlsx_workbook));
}


cell* get_table_data(char* buffer,char** stringTable, size_t *nCells) {
    
    int n = count_cells(buffer);
    if(n==0)*nCells=0;
    if (n > 0) {
        cell* tableData = (cell*)calloc(n, sizeof(cell));
        if (!tableData){
            return 0;
        }
        size_t pos = 0;
        buffer = get_next_cell(&tableData[0], buffer, stringTable);
        while (buffer) {
            pos++;
            buffer = get_next_cell(&tableData[pos], buffer, stringTable);
        }
        *nCells = pos;
        return tableData;
    }
    return 0;
}

int count_cells(char* buffer) {
    int result = 0;
    while ((buffer = strstr(buffer+1, "<c r"))){

        while(*buffer!='>'){
            buffer++;
            if(*buffer=='/')break;
        }
        if(*buffer!='/'){
            result++;
        }
    }    
    return result;
}

size_t count_string_table(char* buffer){
    
    char* type =  strstr(buffer, "<x:si");
    size_t count =0;
    if(!type){
        char* next = strstr(buffer, "</si>");
        while(next){
            count++;
            next = strstr(next+1, "</si>");
        }
    }else{
        char* next = strstr(buffer, "</x:si>");
        while(next){
            count++;
            next = strstr(next+1, "</x:si>");
        }

    }

    return count;
}


char** get_string_table(char* buffer, size_t* nStr) {
    
    if(!buffer) return 0;
    
    char* unq = strstr(buffer, "uniqueCount");
    size_t n;
    if(unq) 
    {   
        unq += 13;
        n= strtol(unq,0,10);
    }else{
        n = count_string_table(buffer);
    }
    if(!n) return 0;

    *nStr = n;
    char* type =  strstr(buffer, "<x:si");
    if(!type){
        if (n > 0) {
            char** table = (char**)calloc((n+1) , sizeof(char*));
            table[n]=0;

            char* begin_tag = strstr(buffer, "<si");
            if(begin_tag==0)return 0;
            char* end_tag ; 
            begin_tag = strstr(begin_tag, "<t");
            if(begin_tag==0)return 0;
            if(begin_tag[2]!='/'&& begin_tag[3]!='/'){
                while(*begin_tag!='>')begin_tag++;
                begin_tag++;
                end_tag = strstr(begin_tag, "</t>");

                if(end_tag){
                    *end_tag =0;
                }else{
                    free(table);
                    return 0;
                }
                table[0] =begin_tag;
                //remove_escapees(begin_tag,end_tag);
            }else{
                end_tag = begin_tag;
                table[0] =NA;
            }
            
            size_t i;
            for (i = 1; i < n; i++){
                begin_tag = strstr(end_tag+1, "<si");
                if(begin_tag==0){
                    table[i] =NA;
                    continue;
                }
                begin_tag = strstr(begin_tag, "<t");
                if(begin_tag==0){
                    table[i] =NA;
                    continue;
                }
            
                if(begin_tag[2]!='/'&& begin_tag[3]!='/'){
                    if(!begin_tag){
                        table[i] =0;
                        break;
                    }
                    while(*begin_tag!='>')begin_tag++;
                    begin_tag++;
                    end_tag = strstr(begin_tag, "</t>");
                    if(end_tag){
                        *end_tag =0;
                    }else{
                        free(table);
                        return 0;
                    }
                    table[i] =begin_tag;
                    remove_escapees(begin_tag,end_tag);
                }else{
                    end_tag = begin_tag;
                    table[i] =NA;
                }
            }
            table[i]=0;
            return table;
        }
        return 0;
    }else{
         if (n > 0) {
            char** table = (char**)calloc((n+1) , sizeof(char*));
            table[n]=0;

            char* begin_tag = strstr(buffer, "<x:si");
            if(begin_tag==0)return 0;
            char* end_tag ; 
            begin_tag = strstr(begin_tag, "<x:t");
            if(begin_tag==0)return 0;
            if(begin_tag[2]!='/'&& begin_tag[3]!='/'){
                while(*begin_tag!='>')begin_tag++;
                begin_tag++;
                end_tag = strstr(begin_tag, "</x:t>");

                if(end_tag){
                    *end_tag =0;
                }else{
                    free(table);
                    return 0;
                }
                table[0] =begin_tag;
                remove_escapees(begin_tag,end_tag);
            }else{
                end_tag = begin_tag;
                table[0] =NA;
            }
            
            size_t i;
            for (i = 1; i < n; i++){
                begin_tag = strstr(end_tag+1, "<x:si");
                if(begin_tag==0){
                    table[i] =NA;
                    continue;
                }
                begin_tag = strstr(begin_tag, "<x:t");
                if(begin_tag==0){
                    table[i] =NA;
                    continue;
                }
                if(begin_tag[2]!='/'&& begin_tag[3]!='/'){
                    if(!begin_tag){
                        table[i] =0;
                        break;
                    }
                    while(*begin_tag!='>')begin_tag++;
                    begin_tag++;
                    end_tag = strstr(begin_tag, "</x:t>");
                    if(end_tag){
                        *end_tag =0;
                    }else{
                        free(table);
                        return 0;
                    }
                    table[i] =begin_tag;
                    remove_escapees(begin_tag,end_tag);
                }else{
                    end_tag = begin_tag;
                    table[i] =NA;
                }
            }
            table[i]=0;
            return table;
        }
        return 0;
    }
    
}


int check_formulas(char* sheet){
    char *f = strstr(sheet,"<f>");
    while(f){
        char* ce = strstr(f,"</c>");
        char* v = strstr(f,"<v>");
        if(ce < v){
            return XLSX_UNCALCULATED_FORMULA_ERROR ;
        }
        f = strstr(f+1,"<f>");
    }
    return 0;
}

const char str_XLSX_OPEN_ZIP_ERROR[]  = "Failure to open the workbook";
const char str_XLSX_FAILURE_TO_READ_CONTENTS_ERROR[] = "Failure to read contents from workbook";
const char str_XLSX_FAILURE_TO_ALLOCATE_STRTABLE_ERROR[] = "Failure to allocare string table";
const char str_XLSX_FAILURE_TO_OPEN_STRTABLE_FILE_ERROR[] = "Failure to open string table file";
const char str_XLSX_FAILURE_TO_READ_STRTABLE_FILE_ERROR[] = "Failure to read string table file";
const char str_XLSX_EMPTY_WORKBOOK_ERROR[] = "Workboook is empty";
const char str_UNKOWN_ERROR[] = "Unkown error";
const char str_XLSX_UNCALCULATED_FORMULA_ERROR[] = "Workboook contains uncalculated formulas";
const char str_XLSX_MEMORY_ALLOCATION_ERROR[] = "Error while allocating memory";
const char str_XLSX_CANNOT_OPEN_SHEET_ERROR[] = "Error while openinf xlsx sheet";
const char str_XLSX_SUCCESS[] = "SUCCESS";

const char* xlsx_get_error_info(int error){
    switch (error)
    {
    case XLSX_OPEN_ZIP_ERROR:
        return str_XLSX_OPEN_ZIP_ERROR;
        break;
    case XLSX_FAILURE_TO_READ_CONTENTS_ERROR:
        return str_XLSX_FAILURE_TO_READ_CONTENTS_ERROR;
        break;
    case XLSX_FAILURE_TO_ALLOCATE_STRTABLE_ERROR:
        return str_XLSX_FAILURE_TO_ALLOCATE_STRTABLE_ERROR;
        break;
    case XLSX_FAILURE_TO_OPEN_STRTABLE_FILE_ERROR:
        return str_XLSX_FAILURE_TO_OPEN_STRTABLE_FILE_ERROR;
    case XLSX_FAILURE_TO_READ_STRTABLE_FILE_ERROR:
        return str_XLSX_FAILURE_TO_READ_STRTABLE_FILE_ERROR;
        break;
    case XLSX_EMPTY_WORKBOOK_ERROR:
        return str_XLSX_EMPTY_WORKBOOK_ERROR;
        break;
    case XLSX_UNCALCULATED_FORMULA_ERROR:
        return str_XLSX_UNCALCULATED_FORMULA_ERROR;
        break;
    case XLSX_MEMORY_ALLOCATION_ERROR:
        return str_XLSX_MEMORY_ALLOCATION_ERROR;
        break;
    case XLSX_CANNOT_OPEN_SHEET_ERROR:
        return str_XLSX_CANNOT_OPEN_SHEET_ERROR;
        break;
    case XLSX_SUCCESS:
        return str_XLSX_SUCCESS;
        break;
    default:
        return str_UNKOWN_ERROR;
        break;
    }
}