#include "csv_op.h"

#include <stdio.h>

#include "string_op.h"



int read_csv_workbook(csv_workbook* wb,const char* name, char sep,char lb){
   
    FILE* input = fopen(name,"r");
    if(!input){
        memset(wb,0,sizeof(csv_workbook));
        return CSV_FAILED_TO_OPEN_FILE_ERROR;
    }
    //
    fseek(input,0,SEEK_END);
    size_t size = ftell(input);
    fseek(input,0,SEEK_SET);

    char* buffer = (char*)malloc(size+1);
    if (!buffer) {
        memset(wb, 0, sizeof(csv_workbook));
        return CSV_FAILED_TO_ALLOCATE_BUFFER_MEMORY_ERROR;
    }
    wb->dataBuffer=buffer;
    wb->dataBufferSize = size;
    buffer[size]=0;

    if(fread(buffer,1,size,input)==0){
        free(buffer);
        fclose(input);
        memset(wb,0,sizeof(csv_workbook));
        return CSV_FAILED_TO_READ_FILE_ERROR;
    }
    fclose(input);

    //Read number of cells
    int nc;
    if(buffer[size-1]==lb)
    {
        nc=0;
    }else{
        nc=1;
    }

    int is_cell = 0;
    while(*buffer){
        if(*buffer == sep){
            if(is_cell)nc++;
            *buffer = 0;
            is_cell=0;
            buffer++;
        }else if(*buffer==lb){
            //marks the line breaks
            *buffer = 1;
            if(is_cell)nc++;
            is_cell=0;
            buffer++;
        }
        else{
            if (!is_cell && *buffer == '\"') {
                buffer++;
                //Tracks escaped quotes
                char is_quote=1;
                while ((*buffer != sep && *buffer!= lb && *buffer!= 0 ) || is_quote){
                    //Removes non printable characters
                    if(*buffer != '\n' &&*buffer>0 && *buffer <32){
                        *buffer = ' ';
                    }else if(*buffer =='\"'){
                        is_quote ^= 1;
                    }
                    buffer++;
                }
            }else{
                buffer++;
            }
            is_cell=1;
        }
    }

    cell* temp = (cell*)malloc(nc*sizeof(cell));
    if(temp){
        wb->cells = temp;
    }else{
        wb->cells =0;
        free_csv_workbook(wb);
        return CSV_FAILED_TO_ALLOCATE_CELLS_ERROR;
    }
   

    char* end = buffer;
    buffer -= size;

    int row =0;
    int col =0;
    wb->nc = nc;
    for(size_t i=0;i<nc;){
        if(buffer==end){
            return 0;
        }
        while(!*buffer){
            buffer++;
            col++;
            if(buffer==end){
                return 0;
            }
        }
        if(*buffer==1){
            col =0;
            *buffer=0;
            buffer++;
            if(buffer==end){
                return 0;
            }
            row++;
        }else{
            wb->cells[i].col=col;
            wb->cells[i].row=row;
            if(*buffer == '\"') buffer++;
            wb->cells[i].value=buffer;
            wb->cells[i].name[0]='c';
            wb->cells[i].name[1]='s';
            wb->cells[i].name[2]='v';
            wb->cells[i].name[3]=0;
            i++;
            while(*buffer){
                buffer++;
                if(*buffer==1){
                    *buffer =0;
                    if(*(buffer-1) == '\"'){
                        *(buffer-1)=0;
                    }
                    buffer++;
                    col =0;
                    row++;
                    break;
                }
            }
            if(*(buffer-1) == '\"')
            {
                *(buffer-1)=0;
            }
        }
    }
    
    return 0;
}

int read_buffered_csv_workbook(csv_workbook* wb,char* buffer, size_t size, char sep,char lb, char fld){
   
   
    wb->dataBuffer=0;
    wb->dataBufferSize = size;
    //Read number of cells
    int nc;
    if(buffer[size-1]==lb)
    {
        nc=0;
    }else{
        nc=1;
    }
    int is_cell = 0;
    while(*buffer){
        if(*buffer == sep){
            if(is_cell)nc++;
            *buffer = 0;
            is_cell=0;
            buffer++;
        }else if(*buffer==lb){
            //marks the line breaks
            *buffer = 1;
            if(is_cell)nc++;
            is_cell=0;
            buffer++;
        }
        else{
            if (!is_cell && *buffer == fld) {
                buffer++;
                //Tracks escaped quotes
                char is_quote=1;
                while ((*buffer != sep && *buffer!= lb && *buffer!= 0 ) || is_quote){
                    //Removes non printable characters
                    if(*buffer != '\n' &&*buffer>0 && *buffer <32){
                        *buffer = ' ';
                    }else if(*buffer ==fld){
                        is_quote ^= 1;
                    }
                    buffer++;
                }
            }else{
                buffer++;
            }
            is_cell=1;
        }
    }

    cell* temp = (cell*)malloc(nc*sizeof(cell));
    if(temp){
        wb->cells = temp;
    }else{
        wb->cells =0;
        return CSV_FAILED_TO_ALLOCATE_CELLS_ERROR;
    }

    char* end = buffer;
    buffer -= size;

    int row =0;
    int col =0;
    wb->nc = nc;
    for(size_t i=0;i<nc;){
        if(buffer==end){
            return 0;
        }
        while(!*buffer){
            buffer++;
            col++;
            if(buffer==end){
                return 0;
            }
        }
        if(*buffer==1){
            col =0;
            *buffer=0;
            buffer++;
            if(buffer==end){
                return 0;
            }
            row++;
        }else{
            wb->cells[i].col=col;
            wb->cells[i].row=row;
            if(*buffer == fld) buffer++;
            wb->cells[i].value=buffer;
            wb->cells[i].name[0]='c';
            wb->cells[i].name[1]='s';
            wb->cells[i].name[2]='v';
            wb->cells[i].name[3]=0;
            i++;
            while(*buffer){
                buffer++;
                if(*buffer==1){
                    *buffer =0;
                    if(*(buffer-1) == fld){
                        *(buffer-1)=0;
                    }
                    buffer++;
                    col =0;
                    row++;
                    break;
                }
            }
            if(*(buffer-1) == fld)
            {
                *(buffer-1)=0;
            }
        }
    }
    
    return 0;
}


int free_csv_workbook(csv_workbook* wb){
    if(wb->cells){
        free(wb->cells);
    }
    if(wb->dataBuffer){
        free(wb->dataBuffer);
    }

    memset(wb,0,sizeof(csv_workbook));
}


const char str_CSV_SUCCESS[] = "CSV parsing success";
const char str_CSV_FAILED_TO_OPEN_FILE_ERROR[]  = "Failed to open the file";
const char str_CSV_FAILED_TO_ALLOCATE_BUFFER_MEMORY_ERROR[] = "Failed to allocate the buffer";
const char str_CSV_FAILED_TO_READ_FILE_ERROR[] = "Failed to read the file";
const char str_CSV_FAILED_TO_ALLOCATE_CELLS_ERROR[] = "Failed to allocate the cells";
const char str_CSV_UNKOWN_ERROR[] = "Unkown Error";

const char* csv_get_error_info(int error)
{
    switch (error)
    {
    case CSV_FAILED_TO_OPEN_FILE_ERROR:
        return str_CSV_FAILED_TO_OPEN_FILE_ERROR;
        break;
    case CSV_FAILED_TO_ALLOCATE_BUFFER_MEMORY_ERROR:
        return str_CSV_FAILED_TO_ALLOCATE_BUFFER_MEMORY_ERROR;
        break;
    case CSV_FAILED_TO_READ_FILE_ERROR:
        return str_CSV_FAILED_TO_READ_FILE_ERROR;
        break;
    case CSV_FAILED_TO_ALLOCATE_CELLS_ERROR:
        return str_CSV_FAILED_TO_ALLOCATE_CELLS_ERROR;
    case CSV_SUCCESS:
        return str_CSV_SUCCESS;
        break;
    default:
        return str_CSV_UNKOWN_ERROR;
        break;
    }
}