#include "string_op.h"
#include <stdlib.h>
#include<stdio.h>




int name_from_position(char* name,unsigned int i, unsigned int j) {
    char temp[10];
    if(j<26){
        sprintf(name,"%c%d",j+65,i+1);
        return 0;
    }else{
        int pos=9;
        temp[9] = j%26 + 65;
        j/=26;
        while(j){
            pos--;
            if(j==26){
                temp[pos] = 90;
                break;
            }else if(j%26 ==0){
                temp[pos] = 90;
                j/=26;
                j--;
            }else{
                temp[pos] = j%26 + 64;
                j/=26;
            }
            
        }

        int k;
        for(k=0;pos<10;k++,pos++){
            name[k]= temp[pos];
        }
        sprintf(name+k,"%d",i+1);
    }
    return 0;
}

int get_array_pos(char* name, int* col, int* row) {
    if (!name) {
        return __LINE__;
    }
    char* bg = name;
    while((*name & 0b11011111)>=65 && (*name & 0b11011111) <= 90){
        name++;
    }
    
    char* nm = name -1;
    int tc =((*nm&0b11011111)-65);
    int pwr = 26;
    while(nm!=bg){
        nm--;
        tc += pwr*((*nm&0b11011111) - 64);
        pwr*=26;
        
    }

    *col = tc;
    *row = strtol(name,0,10) - 1;
    return 0;
}


void trim(char* str){
    if(!str){
        return;
    }
    char* ptr1 = str;
    while(*ptr1==' '){
        ptr1++;
    }
    if(ptr1!=str){
        while(*ptr1){
            *str = *ptr1;
            str++;
            ptr1++;
        }
        *str=0;
        str--;
        while(*str==' '){
            *str=0;
            str--;
        }
    }
}

void remove_tag(char* src,char* tag,char* endtag, size_t tgl){
    if (!src) {
        return;
    }
    while(*src==' ')src++;
    char* tg = strstr(src,tag);
    if(tg!=src)return;

    char* endtg = strstr(src, endtag);
    if(!endtg){ 
        return;
    };
    *endtg = 0;
    while(*src){
        *(src-tgl)=*src;
        src++;
    }
    *(src-tgl)=*src;
    return;
}

//const char* escapees[] = {"&amp;","&lt;","&gt;","&quot;","&apos;"};
size_t remove_escapees(char* beg, char* end){
    char* escape;
    size_t nr=0;
    while(escape=strstr(beg,"&lt;")){
        memmove(escape,escape+4,end- escape-2);
        *escape='<';
        escape++;
        if(escape==end){
            end -=4;
            nr+=4;
            break;
        }
        end -=4;
        nr+=4;
    }
    while(escape=strstr(beg,"&gt;")){
        memmove(escape,escape+4,end- escape-2);
        *escape='>';
        escape++;
        if(escape==end){
            end -=4;
            nr+=4;
            break;
        }
        end -=4;
        nr+=4;
    }
    while(escape=strstr(beg,"&quot;")){
        memmove(escape,escape+6,end- escape-3);
        *escape='\"';
        escape[1] = '\"';
        escape++;
        if(escape==end){
            end -=6;
            nr+=6;
            break;
        }
        end -=6;
        nr+=6;
    }
    while(escape=strstr(beg,"&apos;")){
        memmove(escape,escape+6,end- escape-4);
        *escape='\'';
        escape++;
        if(escape==end){
            end -=6;
            nr+=6;
            break;
        }
        end -=6;
        nr+=6;
    }
    while(escape=strstr(beg,"&amp;")){
        memmove(escape,escape+4,end - escape-3);
        *escape='&';
        escape++;
        if(escape==end){
            end -=5;
        nr+=5;
            break;
        }
        end -=5;
        nr+=5;
    }
    return nr;
}

void remove_first_comma(char * str){
    int found =0;
    size_t n = strlen(str);
    char* end = str + n;
    while(*str){
        if(*str==','){
            found = 1;
            break;
        }
        str++;
    }

    if(found){
        memmove(str,str+1,end - str -1);
    }
    return;
}


void remove_char(char * str, char c)
{
    size_t n = strlen(str);
    char* end = str + n;
    while(*str){
        if(*str==c){
            memmove(str,str+1,end - str);
            end--;
        }
        str++;
    }

    return;
}