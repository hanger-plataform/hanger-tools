#include "sheet_op.h"

#include <stdlib.h>
#include <stdio.h>


cell* get_cell(sheet* sh, int i, int j) {
    if (!sh)return 0;
    int a = 0, b = (sh->nc) / 2, c = sh->nc - 1;
    while (1) {

        if (sh->cells[a].row == i && sh->cells[a].col == j) {
            return &sh->cells[a];
        }
        else if (sh->cells[a].row > i) {
            return 0;
        }
        else if (sh->cells[a].row == i && sh->cells[a].col > j) {
            return 0;
        }

        cell v = sh->cells[c];
        if (sh->cells[c].row == i && sh->cells[c].col == j) {
            return &sh->cells[c];
        }
        else if (sh->cells[c].row < i) {
            return 0;
        }
        else if (sh->cells[c].row == i && sh->cells[c].col < j) {
            return 0;
        }


        if (sh->cells[b].row == i && sh->cells[b].col == j) {
            return &sh->cells[b];
        }
        else if (sh->cells[b].row > i) {
            c = b - 1;
            a++;
            if (c <= a)return 0;
            b = (a + b) / 2;
        }
        else if (sh->cells[b].row == i) {
            if (sh->cells[b].col > j) {
                c = b - 1;
                a++;
                if (c <= a)return 0;
                b = (a + b) / 2;
            }
            else {
                a = b + 1;
                c = c - 1;
                if (a >= c)return 0;
                b = (a + c) / 2;
            }
        }
        else {
            a = b + 1;
            c = c - 1;
            if (a >= c)return 0;
            b = (a + c) / 2;
        }
    }
}

cell* get_named_cell(sheet* sh, char* name) {
    int i, j;
    int status = get_array_pos(name, &j, &i);
    if (!status) {
        return get_cell(sh, i, j);
    }
    return 0;
}

void cell_to_json(char *out, cell* c) {

    if (!c)return;

    char template1s[] = "{\"name\":\"%s\", \"value\":\"%s\"}";
    char template1f[] = "{\"name\":\"%s\", \"value\":%f}";

    if (!strcmp(c->value, "0")) {
        sprintf(out, "{\"name\":\"%s\", \"value\":0}", c->name);
    }
    else {
        double val = strtod(c->value, 0);
        if (val == 0) {
            sprintf(out, template1s, c->name, c->value);
        }
        else {
            sprintf(out, template1f, c->name, val);
        }
    }
    
    return;
}


size_t cell_from_name(const char* name) {
    size_t x = 0x0;
    int* lx = (int*)&x;
    int* ux = (int*)&x + 1;
    int pos = 0;
    int temp = 0;
    while ((name[pos] & 0b1101111) >= 65 && (name[pos] & 0b1101111) <= 90) {
        temp = 27 * temp + (name[pos] & 0b1101111) - 65;
        pos++;
    }
    *lx = temp;
    *ux = strtol(name + pos, 0, 10);
    return x;
}

void trim_cell(cell *c){
    char* beg = c->value;

    while(*beg == ' '){
        beg++;
        if(*beg == 0) {
            return;
        }
    }
    char*  end= beg;
    while(*end)end++;
    end--;
    while(*end==' ')end--;
    end++;
    *end =0;
    c->value = beg;
    return;
}

int max_col(sheet sh){

    int col = -1;
    for(size_t i=0; i< sh.nc;i++){
        if(sh.cells[i].col > col){
            col = sh.cells[i].col;
        }
    }
    return col;
}
