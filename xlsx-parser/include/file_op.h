#ifndef FILE_OP_H
#define FILE_OP_H

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

size_t read_file_buffered(char** buffer, const char* name);

#ifdef __cplusplus
}
#endif

#endif