#ifndef SHEET_OP_H
#define SHEET_OP_H

#include <stdlib.h>
#include <string.h>
#include "string_op.h"

#ifdef __cplusplus
extern "C" {
#endif



typedef struct cell {
    char name[12];
    char* value;
    int row;
    int col;
    size_t strPos;
}cell;

typedef struct cellf32 {
    char name[12];
    float value;
    int row;
    int col;
    size_t strPos;
}cellf32;

typedef struct cellf64 {
    char name[12];
    double value;
    int row;
    int col;
    size_t strPos;
}cellf64;

typedef struct sheet {
    cell* cells;
    char** stringTable;
    size_t nstr;
    size_t nc;
    char* name;
    char* data;
    char status;
}sheet;


cell* get_cell(sheet* sh, int i, int j);

cell* get_named_cell(sheet* sh, char* name);

void cell_to_json(char* out, cell* c);

size_t cell_from_name(const char* name);

void trim_cell(cell *c);

int max_col(sheet sh);

#ifdef __cplusplus
}
#endif


#endif
