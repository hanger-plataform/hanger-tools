#ifndef XLSX_OP_H
#define XLSX_OP_H

#include <stdlib.h>
#include <zip.h>

#include "string_op.h"
#include "sheet_op.h"

#define LOWER(x) x & 0x00000000FFFFFFFF
#define UPPER(x) x >> 32
#define MAX_COLUMN 16384
#define MAX_ROW 1048576

#define MAX_SHEETS 40

#ifdef __cplusplus
extern "C" {
#endif

#define XLSX_OPEN_ZIP_ERROR 100000
#define XLSX_FAILURE_TO_READ_CONTENTS_ERROR 100001
#define XLSX_FAILURE_TO_ALLOCATE_STRTABLE_ERROR 100002
#define XLSX_FAILURE_TO_OPEN_STRTABLE_FILE_ERROR 100003
#define XLSX_FAILURE_TO_READ_STRTABLE_FILE_ERROR 100004
#define XLSX_EMPTY_WORKBOOK_ERROR 100005
#define XLSX_UNCALCULATED_FORMULA_ERROR 100006
#define XLSX_MEMORY_ALLOCATION_ERROR 100007
#define XLSX_CANNOT_OPEN_SHEET_ERROR 100008
#define XLSX_ERROR_OPENING_SHEET 100009
#define XLSX_SUCCESS 0


typedef struct xlsx_workbook{
    sheet** sheets;
    char** sheetNames;
    char** stringTable;
    size_t nstr;
    int ns;
    char* name;
    char* strTableData;
    char* contents;
    int status;
} xlsx_workbook;



void sheet_to_json(const char* name, sheet *sheet);

int read_xlsx_workbook(xlsx_workbook* wb,const char* name);

sheet * read_xlsx_sheet(zip_t* xlsx_file,char* sheetName, char** stringTable, zip_stat_t *info, int *xlsx_error);

int check_formulas(char* sheet);

void free_xlsx_workbook(xlsx_workbook* wb);

void print_sheet(xlsx_workbook *wb,size_t ids);

char* get_next_cell(cell *tgt,char* buffer, char** stringTable);

size_t cell_from_name(const char* name);

cell* get_table_data(char* buffer,char** stringTable, size_t *nCells);

cell* get_cell(sheet* sh, int i, int j);

cell* get_named_cell(sheet* sh, char* name);

int count_cells(char* buffer);

char** get_string_table(char* buffer, size_t* nStr);

const char* xlsx_get_error_info(int error);

#ifdef __cplusplus
}
#endif

#endif