#ifndef CSV_OP_H
#define CSV_OP_H

#include "sheet_op.h"

#define CSV_SUCCESS 0
#define CSV_FAILED_TO_OPEN_FILE_ERROR 100000
#define CSV_FAILED_TO_ALLOCATE_BUFFER_MEMORY_ERROR 100001
#define CSV_FAILED_TO_READ_FILE_ERROR 100002
#define CSV_FAILED_TO_ALLOCATE_CELLS_ERROR 100003


#ifdef __cplusplus
extern "C" {
#endif

typedef struct csv_workbook{
    cell* cells;
    size_t nc;
    const char* name;
    char* dataBuffer;
    size_t dataBufferSize;
}csv_workbook;

int read_csv_workbook(csv_workbook* wb,const char* name, char sep,char lb);

int read_buffered_csv_workbook(csv_workbook* wb,char* buffer, size_t size, char sep,char lb, char fld);

int free_csv_workbook(csv_workbook* wb);

const char* csv_get_error_info(int error);

#ifdef __cplusplus
}
#endif

#endif