#ifndef STRING_OP_H
#define STRING_OP_H

#include <string.h>



#ifdef __cplusplus
extern "C" {
#endif


void remove_tag(char* src,char* tag,char* endtag, size_t tgl);

void trim(char* str);

int get_array_pos(char* name, int* col, int* row);

int name_from_position(char* name,unsigned int i, unsigned int j) ;

size_t remove_escapees(char* beg, char* end);

void remove_first_comma(char * str);

void remove_char(char * str, char c);

#ifdef __cplusplus
}
#endif

#endif