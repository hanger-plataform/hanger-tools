# Instructions

1. Make sure you have g++ installed
2. Install boost https://www.boost.org/
3. Compile inserter.cpp with the following command
    **g++ inserter.cpp -o ../bin/inserter -lboost_filesystem **

4. To ruin the progam use ${HANGER_TOOLS_DIRECTORY}/bin/inserter file_name.csv separator table_name output.sql [optional:commit size]

# Dependencies packages
# Centos and REH based distros


# Development Tools
sudo yum groupinstall "Development Tools" -y

#Others
sudo yum install gcc libstdc++ libstdc++-devel libstdc++-static -y

