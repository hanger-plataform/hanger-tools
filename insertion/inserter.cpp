#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
namespace fs = boost::filesystem;

//g++ -g  -static-libstdc++  inserter.cpp -o inserter -l:libboost_filesystem.a 

int main(int argc, char* argv[]){

    if(argc<5) return __LINE__;
    std::string fileName = argv[1];
    if(fs::extension(fileName)!=".csv"){
        return __LINE__;
    }

    std::string sep = argv[2];
    if(sep[0]=='\"'){
        sep=sep.substr(1,sep.size()-1);
    }
    

    std::string tableName = argv[3];
    if(tableName[0]=='\"'){
        tableName=tableName.substr(1,tableName.size()-1);
    }
    

    std::string outputName = argv[4];
    if(outputName[0]=='\"'){
        outputName=outputName.substr(1,outputName.size()-1);
    }

    size_t commitSize =0;
    if(argc>=6){
        commitSize = strtol(argv[5],0,10);
        if(commitSize==0){
            std::cout <<"Invalid commit size "<<std::endl;
        }
    }

    bool full = true;
    if(argc >=7  && commitSize!=0 && !strcmp(argv[6],"partial")){
        full = false;
    }
    
    std::string nl = "NULL";
    if(argc>=8)nl = argv[7];
    

    if(commitSize==0){
        
        std::ofstream output(outputName);
        std::ifstream input(fileName);
        if(!output || !input){
            if(!output)std::cout << "Could not create " << outputName <<std::endl;
            if(!input)std::cout << "Could not open " << fileName <<std::endl;
            return __LINE__;
        }

        std::string line;
        std::getline(input,line);
        if(line[line.size()=='\r'])line.resize(line.size()-1);

        if(line.empty()){
            std::cout << "Could not read header information" <<std::endl;
        }

        std::vector<std::string> values;
        boost::split(values,line,boost::is_any_of(sep));
        int nFields = values.size();
        std::string outString;
        outString.reserve(1048576);

        outString = "INSERT INTO " + tableName + " (";

        for(auto &v : values){
            boost::replace_all(v,"\""," ");
            outString+= v +",";
        }
        outString[outString.size()-1]=')';

        output << outString  + " VALUES";
        outString.clear();
        
        while(getline(input,line)){
            if(line[line.size()=='\r'])line.resize(line.size()-1);
            int c =0;
            if(!outString.empty()){
                outString = ",";
            }
            
            if(sep.size())boost::split(values,line,boost::is_any_of(sep));
            
            outString += '(';
                for(auto &v : values){
                    if(v.size()>0){
                        if(v[0]=='\"'){
                            v[0]='\'';
                            v[v.size()-1]='\'';
                        }
                        if(v=="''''")v = nl;
                        outString+= v +",";
                    }else{
                        outString+= nl +',';
                    }
                    c++;
                }
                outString[outString.size()-1]=')';
                if(c==nFields){
                    output << outString <<std::flush;
                }
                
            }

            output << ';';
            input.close();
            output.close();
    }else if(full){
        
        std::ofstream output(outputName);
        std::ifstream input(fileName);

        if(!output || !input){
            if(!output)std::cout << "Could not create " << outputName <<std::endl;
            if(!input)std::cout << "Could not open " << fileName <<std::endl;
            return __LINE__;
        }

        std::string line;
        std::getline(input,line);
        if(line[line.size()=='\r'])line.resize(line.size()-1);
        if(line.empty()){
            std::cout << "Could not read header information" <<std::endl;
        }

        std::vector<std::string> values;
        boost::split(values,line,boost::is_any_of(sep));
        int nFields = values.size();

        std::string outString, header;
        outString.reserve(1048576);
        header.reserve(1024);
        header = "INSERT INTO " + tableName + " (";

        for(auto &v : values){
            boost::replace_all(v,"\""," ");
            header+= v +",";
        }
        header[header.size()-1]=')';
        header += " VALUES";
        output << header;

        size_t commitCount =0;
        
        while(getline(input,line)){
            if(line[line.size()=='\r'])line.resize(line.size()-1);
            if(commitCount>=commitSize){
                output << ';' << header;
                commitCount =0;
            }
            
            int c =0;
            if(!outString.empty()){
                if(commitCount){
                    outString = ",";
                }else{
                    outString.clear();
                }
            }
            
            if(sep.size())boost::split(values,line,boost::is_any_of(sep));

            outString += '(';
                for(auto &v : values){
                    if(v.size()>0){
                        if(v[0]=='\"'){
                            v[0]='\'';
                            v[v.size()-1]='\'';
                        }
                        if(v=="''''")v = nl;
                        outString+= v +",";
                    }else{
                        outString+= nl +',';
                    }
                    c++;
                }
                outString[outString.size()-1]=')';
                if(c==nFields){
                    output << outString;
                }
                commitCount++;
                
        }

        output << ';';

        input.close();
        output.close();

    }

   
    return 0;
}